package com.egar.auction.client;

import com.egar.auction.client.view.MainView;
import com.egar.auction.client.view.Utilities;
import com.egar.auction.server.controller.exceptions.*;
import com.egar.auction.server.domain.User;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

/**
 * Client class. This class provide user interface for interact with server
 *
 * @author Lisitsyn R.
 */
public class Client {
    private Socket socket;
    private User currentUser;

    /**
     * Constructs new Client class without parameters.
     */
    public Client() {
        ResourceBundle props = ResourceBundle.getBundle("client");
        int port = Integer.valueOf(props.getString("egar.se.client.port"));
        String address = props.getString("egar.se.client.host");
        System.out.println("Client: - Opening socket...");
        try {
            socket = new Socket(InetAddress.getByName(address), port);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Returns Object representing com.egar.auction.server response for a specified query Object.
     * Response object can contain any  exception that can be thrown by controller's methods
     *
     * @param o server query Object.
     * @return server response Object.
     * @throws PermissionNotGrantedException
     * @throws CategoryNotFoundException
     * @throws UserWithSuchIdNotFoundException
     * @throws LotWithSuchIdNotFoundException
     * @throws WrongServerQueryStructureException
     * @throws IncorrectQueryKeyException
     * @throws LotIsNotSoldException
     */
    public Object sendServerRequest(Object o) throws PermissionNotGrantedException, CategoryNotFoundException, UserWithSuchIdNotFoundException, LotWithSuchIdNotFoundException, WrongServerQueryStructureException, IncorrectQueryKeyException, LotIsNotSoldException {
        try {
            ObjectOutputStream out = new ObjectOutputStream((socket.getOutputStream()));
            ObjectInputStream in = new ObjectInputStream((socket.getInputStream()));
            out.writeObject(o);
            out.flush();
            Object result = in.readObject();
            if (result != null) {
                try {
                    throw (PermissionNotGrantedException) result;
                } catch (ClassCastException e) {
                    try {
                        throw (CategoryNotFoundException) result;
                    } catch (ClassCastException e1) {
                        try {
                            throw (UserWithSuchIdNotFoundException) result;
                        } catch (ClassCastException e2) {
                            try {
                                throw (LotWithSuchIdNotFoundException) result;
                            } catch (ClassCastException e3) {
                                try {
                                    throw (WrongServerQueryStructureException) result;
                                } catch (ClassCastException e4) {
                                    try {
                                        throw (IncorrectQueryKeyException) result;
                                    } catch (ClassCastException e5) {
                                        try {
                                            throw (LotIsNotSoldException) result;
                                        } catch (ClassCastException e6) {
                                            return result;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            } else
                return null;
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Entry method for Client class.
     */
    public void run() {
        Utilities utilities = new Utilities();
        MainView mainView = new MainView(this, utilities);
        mainView.executeView();
        sendStopServerThreadRequest();
    }

    private void sendStopServerThreadRequest() {
        List<Object> request = new ArrayList<>();
        request.add("stopServerThread");
        try {
            sendServerRequest(request);
        } catch (AuctionException e) {
            System.out.println("Unexpected AuctionException on sendStopServerThreadRequest.");
        }
    }

    /**
     * Returns currentUser
     *
     * @return User
     */
    public User getCurrentUser() {
        return currentUser;
    }

    /**
     * Sets currentUser
     */
    public void setCurrentUser(User currentUser) {
        this.currentUser = currentUser;
    }
}
