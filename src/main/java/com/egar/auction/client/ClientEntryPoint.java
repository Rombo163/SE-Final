package com.egar.auction.client;

/**
 * Outer class for client starting. It contains main static method.
 */
public class ClientEntryPoint {
    /**
     * Static method for starting client.
     *
     * @param args command prompt parameters.
     */
    public static void main(String args[]) {
        System.out.println("                 Usernames/Passwords:");
        System.out.println("for users: Vasya,Petya,Kolyan,Sanya,Fedya.");
        System.out.println("for Admin: Admin.");
        Client client = new Client();
        client.run();
    }
}
