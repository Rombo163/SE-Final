package com.egar.auction.client.view;

import com.egar.auction.client.Client;
import com.egar.auction.client.view.bid.BidView;
import com.egar.auction.server.controller.exceptions.AuctionException;
import com.egar.auction.server.controller.exceptions.LotIsNotSoldException;
import com.egar.auction.server.controller.exceptions.LotWithSuchIdNotFoundException;
import com.egar.auction.server.domain.Lot;

import java.util.ArrayList;
import java.util.List;

/**
 * RegUserView class, one of the classes that representing View part of the MVC pattern
 *
 * @author Lisitsyn R.
 */
public class RegUserView {

    private Client client;
    private Utilities utilities;
    private BidView bidView;
    private MainView mainView;

    /**
     * Constructs a RegUserView.
     *
     * @param client    Client.
     * @param mainView  mainView.
     * @param utilities utility class.
     */
    public RegUserView(MainView mainView, Client client, Utilities utilities) {
        this.client = client;
        this.utilities = utilities;
        this.mainView = mainView;
        this.bidView = new BidView(client, utilities);
    }

    /**
     * This is a main method RegUserView class.
     */
    public void executeView() {
        boolean keepWorking = true;

        while (keepWorking) {
            int choice;
            displayMenu();
            try {
                choice = Integer.parseInt(utilities.getInputString());
                switch (choice) {
                    case 0:
                        keepWorking = false;
                        break;
                    case 1:
                        mainView.lotView.executeView();
                        break;
                    case 2:
                        mainView.showAllCategories();
                        break;
                    case 3:
                        bidView.executeView();
                        break;
                    case 4:
                        showDeliveryAndCostInfo();
                        printFormula();
                        break;
                    default:
                        System.out.println("Incorrect input, try again.");
                }
            } catch (NumberFormatException e) {
                System.out.println("Incorrect input, try again.");
            }
        }
    }

    private void displayMenu() {
        System.out.println("-----------------Registered User Menu-----------------");
        System.out.println("Select:");
        System.out.println("    1:  -Show lots menu.");
        System.out.println("    2:  -Show categories.");
        System.out.println("    3:  -Show bids menu.");
        System.out.println("    4:  -Show delivery and cost info.");
        System.out.println("    0:  -For exit.\n");
    }

    private void showDeliveryAndCostInfo() {
        List<Object> request = new ArrayList<>();
        List<Lot> response;
        long limit = Utilities.getDisplayLines();
        long offset = 0;
        boolean userDidNotBuyAnything = true;

        int totalPrice = 0;
        double sumOfDeliveryForLotsWithChosenDeliveryService = 0;
        double sumOfDeliveryForLotsWithDeliveryServiceIsNull = 0;

        try {
            do {
                request.clear();
                request.add("getLotsBySold");
                request.add(client.getCurrentUser().getId());
                request.add(limit);
                request.add(offset);
                response = (ArrayList<Lot>) client.sendServerRequest(request);
                if (response.size() == 0) {
                    if (userDidNotBuyAnything)
                        System.out.println("--------------------------------------------------------------------------------------------------");
                    System.out.println("                                   You did not buy anything.");
                    break;
                } else {
                    userDidNotBuyAnything = false;
                    System.out.println("\n\n---Lot#----Cost(price*pcs)----------DeliveryService-------------CostOfDelivery------TotalForLot---");
                    for (Lot lot : response) {
                        if (lot.getDeliveryService() != null) {
                            List<Object> innerRequest = new ArrayList<>();
                            innerRequest.add("getDeliveryCostForLot");
                            innerRequest.add(lot.getId());
                            double deliveryCost = (double) client.sendServerRequest(innerRequest);
                            System.out.printf("    %-4d%8d(%d*%d)              %-13s%24.2f%19.2f\n",
                                    lot.getId(), lot.getPrice() * lot.getQuantity(), lot.getPrice(), lot.getQuantity(),
                                    lot.getDeliveryService().getTitle(), deliveryCost,
                                    (lot.getPrice() * lot.getQuantity() + deliveryCost));
                            sumOfDeliveryForLotsWithChosenDeliveryService += deliveryCost;
                        } else {
                            List<Object> innerRequest = new ArrayList<>();
                            innerRequest.add("getDeliveryCostForLot");
                            innerRequest.add(lot.getId());
                            double deliveryCost = (double) client.sendServerRequest(innerRequest);
                            System.out.printf("    %-4d%8d(%d*%d)%23s          (%.2f)%14.2f(%.2f)\n",
                                    lot.getId(), lot.getPrice() * lot.getQuantity(), lot.getPrice(),
                                    lot.getQuantity(), "         not chosen(average value)",
                                    deliveryCost,
                                    (float) (lot.getPrice() * lot.getQuantity()),
                                    (lot.getPrice() * lot.getQuantity() + deliveryCost));
                            sumOfDeliveryForLotsWithDeliveryServiceIsNull += deliveryCost;
                        }
                        totalPrice += (lot.getPrice() * lot.getQuantity());
                    }
                    System.out.println("--------------------------------------------------------------------------------------------------");
                }
                offset += limit;
            }
            while (response.size() == limit && (utilities.getInputString().equals("") || utilities.getInputString() != null));
            System.out.println("--------------------------------------------------------------------------------------------------");
            System.out.printf("   Total    %d                                             %9.2f(%.2f)%10.2f(%.2f)\n\n",
                    totalPrice, sumOfDeliveryForLotsWithChosenDeliveryService,
                    sumOfDeliveryForLotsWithChosenDeliveryService + sumOfDeliveryForLotsWithDeliveryServiceIsNull,
                    sumOfDeliveryForLotsWithChosenDeliveryService + totalPrice,
                    sumOfDeliveryForLotsWithChosenDeliveryService + sumOfDeliveryForLotsWithDeliveryServiceIsNull + totalPrice);
        } catch (AuctionException e) {
            e.printStackTrace();
        } catch (ClassCastException e) {
            System.out.println("Incorrect server response structure.");
        }
    }

    private void printFormula() {
        List<Lot> response;
        List<Object> request = new ArrayList<>();
        long limit = Utilities.getDisplayLines();
        long offset = 0;
        boolean userDidNotBuyAnything = true;
        StringBuilder stringBuilder = new StringBuilder();
        double sumCost = 0;
        double sumOfDeliveryForLotsWithChosenDeliveryService = 0;
        double sumOfDeliveryForLotsWithDeliveryServiceIsNull = 0;
        try {
            do {
                request.clear();
                request.add("getLotsBySold");
                request.add(client.getCurrentUser().getId());
                request.add(limit);
                request.add(offset);
                response = (List<Lot>) client.sendServerRequest(request);
                if (response.size() == 0) {
                    if (userDidNotBuyAnything)
                        System.out.println("\nCost of all of your Lots is 0.\n");
                    break;
                } else {
                    if (!userDidNotBuyAnything)
                        stringBuilder.append("+ ");
                    else
                        stringBuilder.append("TotalCost = ");
                    userDidNotBuyAnything = false;
                    for (Lot lot : response) {
                        if (lot.getDeliveryService() != null) {
                            List<Object> innerRequest = new ArrayList<>();
                            innerRequest.add("getDeliveryCostForLot");
                            innerRequest.add(lot.getId());
                            Double lotDeliveryCost = (Double) client.sendServerRequest(innerRequest);

                            stringBuilder.append(String.format("((%d * %d) + %.2f) ",
                                    lot.getQuantity(), lot.getPrice(), lotDeliveryCost));
                            if (response.get(response.size() - 1) != lot)
                                stringBuilder.append("+ ");
                            sumCost += (lot.getQuantity() * lot.getPrice());
                            sumOfDeliveryForLotsWithChosenDeliveryService += lotDeliveryCost;
                        } else {
                            List<Object> innerRequest = new ArrayList<>();
                            innerRequest.add("getDeliveryCostForLot");
                            innerRequest.add(lot.getId());
                            Double lotDeliveryCost = (Double) client.sendServerRequest(innerRequest);

                            stringBuilder.append(String.format("((%d * %d) + 0(%.2f)) ",
                                    lot.getQuantity(), lot.getPrice(), lotDeliveryCost));
                            if (response.get(response.size() - 1) != lot) stringBuilder.append("+ ");
                            sumCost += (lot.getQuantity() * lot.getPrice());
                            sumOfDeliveryForLotsWithDeliveryServiceIsNull += lotDeliveryCost;
                        }
                    }
                }
            }
            while (response.size() == limit && (utilities.getInputString().equals("") || utilities.getInputString() != null));
            stringBuilder.append(String.format("= %.2f(%.2f).", sumCost + sumOfDeliveryForLotsWithChosenDeliveryService,
                    sumCost + sumOfDeliveryForLotsWithChosenDeliveryService + sumOfDeliveryForLotsWithDeliveryServiceIsNull));
            System.out.println(stringBuilder.toString());
        } catch (ClassCastException e) {
            System.out.println("Incorrect server response structure.");
        } catch (LotIsNotSoldException | LotWithSuchIdNotFoundException e) {
            e.printStackTrace();
        } catch (AuctionException e) {
            System.out.println("Unexpected exception in server response." + e);
        }
    }
}
