package com.egar.auction.client.view.lot;

import com.egar.auction.client.Client;
import com.egar.auction.client.view.Utilities;

/**
 * LotView class, one of the classes that representing View part of the MVC pattern
 *
 * @author Lisitsyn R.
 */
public class LotView {

    private Utilities utilities;
    private Client client;
    private FindLotView findLotView;
    private ShowLotsView showLotsView;

    /**
     * Constructs new Lot view for guest with specified client and utilities class.
     *
     * @param client client for server query invocation.
     * @param utilities class that contains utility methods and fields.
     */
    public LotView(Client client, Utilities utilities) {
        this.client = client;
        this.utilities = utilities;
        this.findLotView = new FindLotView(client,utilities);
        this.showLotsView = new ShowLotsView(client, utilities);
    }

    /**
     * This is a main method for LotView class.
     */
    public void executeView() {
        boolean keepWorking = true;

        while(keepWorking) {
            int choice;
            displayMenu();
            try {
                choice = Integer.parseInt(utilities.getInputString());
                switch (choice) {
                    case 0:
                        keepWorking = false;
                        break;
                    case 1:
                        showLotsView.executeView();
                        break;
                    case 2:
                        findLotView.executeView();
                        break;
                    default:
                        System.out.println("Incorrect input, try again.");
                }
            } catch (NumberFormatException e) {
                System.out.println("Incorrect input, try again.");
            }
        }
    }

    private void displayMenu() {
        System.out.println("-----------------Lot Menu-----------------");
        System.out.println("Select:");
        System.out.println("    1: - Show lots.");
        System.out.println("    2: - Find lot.");
        System.out.println("    0: - For return.\n");
    }


}
