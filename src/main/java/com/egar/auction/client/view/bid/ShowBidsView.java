package com.egar.auction.client.view.bid;

import com.egar.auction.client.Client;
import com.egar.auction.client.view.Utilities;
import com.egar.auction.server.OrderKeysForBid;
import com.egar.auction.server.controller.exceptions.AuctionException;
import com.egar.auction.server.domain.Bid;

import java.util.ArrayList;
import java.util.List;

/**
 * ShowBidsView class, one of the classes that representing View part of the MVC pattern
 *
 * @author Lisitsyn R.
 */
public class ShowBidsView {

    private Utilities utilities;
    private Client client;

    /**
     * Constructs new sorted bid view for guest with specified client and utilities class.
     *
     * @param client    client for server query invocation.
     * @param utilities class that contains utility methods and fields.
     */
    public ShowBidsView(Client client, Utilities utilities) {
        this.client = client;
        this.utilities = utilities;
    }

    /**
     * This is a main method for BidView class.
     */
    public void executeView() {
        boolean keepWorking = true;

        while (keepWorking) {
            int choice;
            displayMenu();
            try {
                choice = Integer.parseInt(utilities.getInputString());
                switch (choice) {
                    case 0:
                        keepWorking = false;
                        break;
                    case 1:
                        showBidsSortedBy(OrderKeysForBid.ID);
                        break;
                    case 2:
                        showBidsSortedBy(OrderKeysForBid.LOT);
                        break;
                    default:
                        System.out.println("Incorrect input, try again.");
                }
            } catch (NumberFormatException e) {
                System.out.println("Incorrect input, try again.");
            }
        }
    }

    private void displayMenu() {
        System.out.println("-----------------Show bids menu-----------------");
        System.out.println("Show bids sorted by:");
        System.out.println("    1: - ID.");
        System.out.println("    2: - Lot.");
        System.out.println("    0: - For return.\n");
    }

    private void showBidsSortedBy(OrderKeysForBid key) {
        List<Object> request = new ArrayList<>();
        List<Bid> response;
        long limit = Utilities.getDisplayLines();
        long offset = 0L;
        try {
            do {
                request.clear();
                request.add("getBidsByOwnerSortedByKey");
                request.add(key);
                request.add(limit);
                request.add(offset);
                response = (ArrayList<Bid>) client.sendServerRequest(request);
                if (response.size() == 0)
                    break;
                System.out.println("---id--------OwnerID-------Owner-------------LotID---");
                for (Bid bid : response)
                    utilities.printBid(bid);
                if (response.size() == limit)
                    System.out.println("-----------------------press-Enter-------------------");
                else
                    System.out.println("-----------------------------------------------------");
                offset += limit;
            }
            while (response.size() == limit && (utilities.getInputString().equals("") || utilities.getInputString() != null));
        } catch (ClassCastException e) {
            System.out.println("Wrong server response structure.");
        } catch (AuctionException e) {
            System.out.println("Unexpected exception in server response: " + e);
        }
    }

}
