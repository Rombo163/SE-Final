package com.egar.auction.client.view.lot;

import com.egar.auction.client.Client;
import com.egar.auction.client.view.Utilities;
import com.egar.auction.server.controller.exceptions.AuctionException;
import com.egar.auction.server.controller.exceptions.LotWithSuchIdNotFoundException;
import com.egar.auction.server.controller.exceptions.CategoryNotFoundException;
import com.egar.auction.server.controller.exceptions.UserWithSuchIdNotFoundException;
import com.egar.auction.server.domain.Lot;

import java.util.ArrayList;
import java.util.List;

/**
 * FindLotView class, one of the classes that representing View part of the MVC pattern
 *
 * @author Lisitsyn R.
 */
public class FindLotView {
    private Utilities utilities;
    private Client client;

    /**
     * Constructs new find lot view for guest with specified client and utilities class.
     *
     * @param client    client for server query invocation.
     * @param utilities class that contains utility methods and fields.
     */
    public FindLotView(Client client, Utilities utilities) {
        this.client = client;
        this.utilities = utilities;
    }

    /**
     * This is a main method for FindLotView class.
     */
    public void executeView() {
        boolean keepWorking = true;

        while (keepWorking) {
            int choice;
            displayMenu();
            try {
                choice = Integer.parseInt(utilities.getInputString());
                switch (choice) {
                    case 0:
                        keepWorking = false;
                        break;
                    case 1:
                        findLotById();
                        break;
                    case 2:
                        findLotsByDescription();
                        break;
                    case 3:
                        findLotsByOwner();
                        break;
                    case 4:
                        findLotsByCategory();
                        break;
                    default:
                        System.out.println("Incorrect input, try again.");
                }
            } catch (NumberFormatException e) {
                System.out.println("Incorrect input, try again.");
            }
        }
    }

    private void displayMenu() {
        System.out.println("-----------------Find Lot Menu-----------------");
        System.out.println("Find lot by:");
        System.out.println("    1: - ID.");
        System.out.println("    2: - Descriptions.");
        System.out.println("    3: - Owner.");
        System.out.println("    4: - Category.");
        System.out.println("    0:  -For return.\n");
    }

    private void findLotsByDescription() {
        System.out.println("   Type keyword: \n\t");
        String str = utilities.getInputString();
        List<Object> request = new ArrayList<>();
        List<Lot> response;
        long limit = Utilities.getDisplayLines();
        long offset = 0L;
        boolean serverDidNotFindAnything = true;
        try {
            do {
                request.clear();
                request.add("getLotsByDescription");
                request.add(str);
                request.add(limit);
                request.add(offset);
                response = (ArrayList<Lot>) client.sendServerRequest(request);
                if (response.size() == 0) {
                    if (serverDidNotFindAnything)
                        System.out.println("No matches.");
                    break;
                }
                serverDidNotFindAnything = false;
                System.out.println("---id---------------Category------------Description-------OwnerID--------Owner---------");
                for (Lot lot : response)
                    utilities.printLot(lot);
                if (response.size() == limit)
                    System.out.println("--------------------------------------press-Enter--------------------------------------");
                else
                    System.out.println("---------------------------------------------------------------------------------------");
                offset += limit;
            }
            while (response.size() == limit && (utilities.getInputString().equals("") || utilities.getInputString() != null));
        } catch (ClassCastException e) {
            System.out.println("Wrong server response structure.");
        } catch (AuctionException e) {
            System.out.println("Unexpected exception in server response: " + e);
        }
    }

    private void findLotsByCategory() {
        System.out.println("   Type categoryName: \n\t");
        String str = utilities.getInputString();
        List<Object> request = new ArrayList<>();
        List<Lot> response;
        long limit = Utilities.getDisplayLines();
        long offset = 0L;
        try {
            do {
                request.clear();
                request.add("getAllLotsInCategory");
                request.add(str);
                request.add(limit);
                request.add(offset);
                response = (ArrayList<Lot>) client.sendServerRequest(request);
                if (response.size() == 0)
                    break;
                System.out.println("---id---------------Category------------Description-------OwnerID--------Owner---------");
                for (Lot lot : response)
                    utilities.printLot(lot);
                if (response.size() == limit)
                    System.out.println("--------------------------------------press-Enter--------------------------------------");
                else
                    System.out.println("---------------------------------------------------------------------------------------");
                offset += limit;
            }
            while (response.size() == limit && (utilities.getInputString().equals("") || utilities.getInputString() != null));
        } catch (ClassCastException e) {
            System.out.println("Wrong server response structure.");
        } catch (CategoryNotFoundException e) {
            System.out.println("Category not found.");
        } catch (AuctionException e) {
            System.out.println("Unexpected exception in server response: " + e);
        }
    }

    private void findLotById() {
        System.out.println("   Type lot id: \n\t");
        long id = Integer.valueOf(utilities.getInputString());
        List<Object> request = new ArrayList<>();
        Object response;
        try {
            request.add("getLotById");
            request.add(id);
            response = client.sendServerRequest(request);
            Lot lot = (Lot) response;
            if (response == null)
                //System.out.printf("-------------------------------Lot with id = %d not found------------------------------\n", id);
                System.out.println("Wrong server response structure.");
            else {
                System.out.println("---id---------------Category------------Description-------OwnerID--------Owner---------");
                utilities.printLot(lot);
            }
            System.out.println("---------------------------------------------------------------------------------------");
        } catch (ClassCastException e) {
            System.out.println("Wrong server response structure.");
        } catch (LotWithSuchIdNotFoundException e) {
            System.out.printf("Lot with id = %d not found\n", id);
        } catch (AuctionException e) {
            System.out.println("Unexpected exception in server response: " + e);
        }
    }

    private void findLotsByOwner() {
        System.out.println("    Type user id and press Enter:\n\t");
        long userId = -1;
        while (userId <= 0) {
            try {
                userId = Long.valueOf(utilities.getInputString());
                if (userId <= 0) {
                    System.out.println("Incorrect user id, try again: ");
                }
            } catch (NumberFormatException e) {
                System.out.println("Incorrect user id, try again: ");
            }
        }

        List<Object> request = new ArrayList<>();
        List<Lot> response;
        long limit = Utilities.getDisplayLines();
        long offset = 0L;
        try {
            do {
                request.clear();
                request.add("getLotsByOwner");
                request.add(userId);
                request.add(limit);
                request.add(offset);
                response = (ArrayList<Lot>) client.sendServerRequest(request);
                if (response.size() == 0)
                    break;
                System.out.println("---id---------------Category------------Description-------OwnerID--------Owner---------");
                for (Lot lot : response)
                    utilities.printLot(lot);
                if (response.size() == limit)
                    System.out.println("--------------------------------------press-Enter--------------------------------------");
                else
                    System.out.println("---------------------------------------------------------------------------------------");
                offset += limit;
            }
            while (response.size() == limit && (utilities.getInputString().equals("") || utilities.getInputString() != null));
        } catch (ClassCastException e) {
            System.out.println("Wrong server response structure.");
        } catch (UserWithSuchIdNotFoundException e) {
            System.out.printf("User with id = %d not found.\n", userId);
        } catch (AuctionException e) {
            System.out.println("Unexpected exception in server response: " + e);
        }
    }
}
