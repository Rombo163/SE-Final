package com.egar.auction.client.view.bid;

import com.egar.auction.client.Client;
import com.egar.auction.client.view.Utilities;
import com.egar.auction.server.OrderKeysForBid;
import com.egar.auction.server.controller.exceptions.AuctionException;
import com.egar.auction.server.controller.exceptions.LotWithSuchIdNotFoundException;
import com.egar.auction.server.domain.Bid;

import java.util.ArrayList;
import java.util.List;

/**
 * ShowBidsByLotView class, one of the classes that representing View part of the MVC pattern
 *
 * @author Lisitsyn R.
 */
public class ShowBidsByLotView {
    private Utilities utilities;
    private Client client;

    /**
     * Constructs new find bid view for guest with specified client and utilities class.
     *
     * @param client    client for server query invocation.
     * @param utilities class that contains utility methods and fields.
     */
    public ShowBidsByLotView(Client client, Utilities utilities) {
        this.client = client;
        this.utilities = utilities;
    }

    /**
     * This is a main method for ShowBidsByLotView class.
     */
    public void executeView() {
        boolean keepWorking = true;

        while (keepWorking) {
            int choice;
            displayMenu();
            try {
                choice = Integer.parseInt(utilities.getInputString());
                switch (choice) {
                    case 0:
                        keepWorking = false;
                        break;
                    case 1:
                        showBidsSortedBy(OrderKeysForBid.ID);
                        break;
                    case 2:
                        showBidsSortedBy(OrderKeysForBid.OWNER);
                        break;
                    default:
                        System.out.println("Incorrect input, try again.");
                }
            } catch (NumberFormatException e) {
                System.out.println("Incorrect input, try again.");
            }
        }
    }

    private void displayMenu() {
        System.out.println("-----------------Show bids menu-----------------");
        System.out.println("Show bids sorted by:");
        System.out.println("    1: - ID.");
        System.out.println("    2: - Owner.");
        System.out.println("    0: - For return.\n");
    }

    private void showBidsSortedBy(OrderKeysForBid key) {
        System.out.println("    Type lot number:\n");
        long lotId = -1;
        while (lotId == -1) {
            try {
                lotId = Long.valueOf(utilities.getInputString());
            } catch (NumberFormatException e) {
                System.out.println("Incorrect lot number, try again: ");
            }
        }

        List<Object> request = new ArrayList<>();
        List<Bid> response = null;
        long limit = Utilities.getDisplayLines();
        long offset = 0L;
        try {
            do {
                request.clear();
                request.add("getBidsByLotSortedByKey");
                request.add(lotId);
                request.add(key);
                request.add(limit);
                request.add(offset);
                response = (ArrayList<Bid>) client.sendServerRequest(request);
                if (response != null) {
                    if (response.size() == 0)
                        break;
                    System.out.println("---id--------OwnerID-------Owner-------------LotID---");
                    for (Bid bid : response)
                        utilities.printBid(bid);
                    if (response.size() == limit)
                        System.out.println("-----------------------press-Enter-------------------");
                    else
                        System.out.println("-----------------------------------------------------");
                    offset += limit;
                } else {
                    System.out.println("Lot not found.");
                    break;
                }
            }
            while (response.size() == limit && (utilities.getInputString().equals("") || utilities.getInputString() != null));
        } catch (ClassCastException e) {
            System.out.println("Wrong server response structure.");
        }catch (LotWithSuchIdNotFoundException e) {
            System.out.println("Lot not found.");
        }catch (AuctionException e) {
            e.printStackTrace();
        }
    }

}
