package com.egar.auction.client.view;


import com.egar.auction.client.Client;
import com.egar.auction.client.view.lot.LotView;
import com.egar.auction.server.controller.exceptions.AuctionException;
import com.egar.auction.server.controller.exceptions.CategoryNotFoundException;
import com.egar.auction.server.domain.Category;
import com.egar.auction.server.domain.User;

import java.util.ArrayList;
import java.util.List;

/**
 * An implementation of "view" part of the MVC pattern.
 * This implementation allows user to interact with
 * data from storage through console.
 *
 * @author Lisitsyn Roman
 */
public class MainView {

    protected LotView lotView;
    protected Client client;
    protected Utilities utilities;
    protected AdminView adminView;
    protected RegUserView regUserView;


    /**
     * Constructs new MainView class with specified client and utilities class.
     *
     * @param client    com.egar.auction.client for server query invocation.
     * @param utilities class that contains utility methods and fields.
     */
    public MainView(Client client, Utilities utilities) {
        this.client = client;
        this.utilities = utilities;
        this.lotView = new LotView(client, utilities);
        this.adminView = new AdminView(this, client, utilities);
        this.regUserView = new RegUserView(this, client, utilities);
    }

    /**
     * Entry point of the MainView class.
     */
    public void executeView() {
        boolean keepWorking = true;
        while (keepWorking) {
            int choice;
            displayMenu();
            try {
                choice = Integer.parseInt(utilities.getInputString());
                switch (choice) {
                    case 0:
                        System.out.println("Program closed.");
                        keepWorking = false;
                        break;
                    case 1:
                        lotView.executeView();
                        break;
                    case 2:
                        showAllCategories();
                        break;
                    case 3:
                        authorization();
                        deAuthorization();
                        break;
                    default:
                        System.out.println("Incorrect number, try again.");
                }
            } catch (NumberFormatException e) {
                System.out.println("Incorrect number, try again.");
            }
        }
    }

    private void displayMenu() {
        System.out.println("-----------------Main menu-----------------");
        System.out.println("Select:");
        System.out.println("    1:  -Show lots menu.");
        System.out.println("    2:  -Show categories.");
        System.out.println("    3:  -For authorization.");
        System.out.println("    0:  -For exit.\n");
    }

    private void authorization() {
        System.out.println("Type your username.");
        String userName = utilities.getInputString();
        System.out.println("Type your password.");
        String password = utilities.getInputString();
        List<Object> request = new ArrayList<>();
        request.add("authentication");
        request.add(userName);
        request.add(password);
        try {
            Object result = client.sendServerRequest(request);
            try {
                ArrayList<Object> response = (ArrayList<Object>) result;
                if ((Boolean) (response.get(0))) {
                    client.setCurrentUser((User) response.get(1));
                    System.out.println("Hi Mr. " + client.getCurrentUser().getUserName());
                } else if (!(Boolean) (response.get(0)))
                    System.out.println("Authentication failed.");
                else
                    System.out.println("Wrong server response structure, " +
                            "\nServer response on authentication contains Boolean = null.");
            } catch (ClassCastException e) {
                System.out.println("Wrong server response structure, ClassCastException: " + e +
                        ", Message: " + e.getMessage());
            }
            if (client.getCurrentUser() != null) {
                if (client.getCurrentUser().getRole().getRoleName().equals("Admin"))
                    adminView.executeView();
                else if (client.getCurrentUser().getRole().getRoleName().equals("RegUser"))
                    regUserView.executeView();
            }
        } catch (AuctionException e) {
            System.out.println("Unexpected exception in server response: " + e);
        }
    }

    private void deAuthorization() {
        List<Object> request = new ArrayList<>();
        request.add("deAuthentication");
        try {
            client.sendServerRequest(request);
            client.setCurrentUser(null);
        } catch (AuctionException e) {
            System.out.println("Unexpected exception in server response: " + e);
        }

    }

    protected void showAllCategories() {

        List<Object> request = new ArrayList<>();
        List<Category> response;
        long limit = Utilities.getDisplayLines();
        long offset = 0L;
        try {
            do {
                request.clear();
                request.add("getCategories");
                request.add(limit);
                request.add(offset);
                response = (ArrayList<Category>) client.sendServerRequest(request);
                if (response.size() == 0)
                    break;
                System.out.println("---id------------CategoryName---");
                for (Category category : response) {
                    utilities.printCategory(category);
                }
                if (response.size() == limit)
                    System.out.println("-----------press-Enter----------");
                else
                    System.out.println("--------------------------------");
                offset += limit;
            }
            while (response.size() == limit && (utilities.getInputString().equals("") || utilities.getInputString() != null));
        } catch (ClassCastException e) {
            System.out.println("Wrong server response structure.");
        } catch (CategoryNotFoundException e) {
            System.out.println("Category not found.");
        } catch (AuctionException e) {
            System.out.println("Unexpected exception in server response: " + e);
        }
    }
}
