package com.egar.auction.client.view;

import com.egar.auction.server.domain.Bid;
import com.egar.auction.server.domain.Category;
import com.egar.auction.server.domain.Lot;
import com.egar.auction.server.domain.User;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Utility class that contains utility methods for other different classes.
 *
 * @author Lisitsyn R.
 */
public class Utilities {

    private BufferedReader reader;

    /**
     * Construct new Utilities class, without parameters.
     */
    public Utilities() {
        this.reader = new BufferedReader(new InputStreamReader(System.in));
    }

    /**
     * number lines per one com.egar.auction.server query.
     */
    private final static int displayLines = 10;

    /**
     * Reads a line of text.
     *
     * @return A String containing the contents of the line, not including
     * any line-termination characters, or null if the end of the
     * stream has been reached
     * @see BufferedReader#readLine()
     */
    public String getInputString() {
        String line = null;
        while (line == null) {
            try {
                line = reader.readLine();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return line;
    }

    /**
     * Prints specified Category via console.
     *
     * @param cat specified Category.
     */
    public void printCategory(Category cat) {
        System.out.printf("   %-16d%-20s\n",
                cat.getId(), cat.getCategoryName());
    }

    /**
     * Prints specified User via console.
     *
     * @param user specified User.
     */
    public void printUser(User user) {
        System.out.printf("   %-18d%-20s%-20s \n",
                user.getId(), user.getUserName(), user.getRole().getRoleName());
    }

    /**
     * Prints specified Bid via console.
     *
     * @param bid Specified Bid.
     */
    public void printBid(Bid bid) {
        System.out.printf("   %-12d%-12d%-20s%-16d \n",
                bid.getId(), bid.getOwner().getId(), bid.getOwner().getUserName(), bid.getLot().getId());
    }

    /**
     * Prints specified Lot via console.
     *
     * @param lot Lot.
     */
    public void printLot(Lot lot) {
        System.out.printf("   %-17d%-21s%-20s%-12d%-20s\n",
                lot.getId(), lot.getCategory().getCategoryName(), lot.getDescription(), lot.getOwner().getId(), lot.getOwner().getUserName());
    }

    /**
     * Returns displayLines constant.
     */
    public static int getDisplayLines() {
        return displayLines;
    }
}
