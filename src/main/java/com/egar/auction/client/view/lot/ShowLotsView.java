package com.egar.auction.client.view.lot;

import com.egar.auction.client.Client;
import com.egar.auction.client.view.Utilities;
import com.egar.auction.server.OrderKeysForLot;
import com.egar.auction.server.controller.exceptions.AuctionException;
import com.egar.auction.server.controller.exceptions.IncorrectQueryKeyException;
import com.egar.auction.server.domain.Lot;

import java.util.ArrayList;
import java.util.List;

/**
 * ShowLotsView class, one of the classes that representing View part of the MVC pattern
 *
 * @author Lisitsyn R.
 */
public class ShowLotsView {
    private Utilities utilities;
    private Client client;

    /**
     * Constructs new sorted lot view for guest with specified client and utilities class.
     *
     * @param client    client for server query invocation.
     * @param utilities class that contains utility methods and fields.
     */
    public ShowLotsView(Client client, Utilities utilities) {
        this.client = client;
        this.utilities = utilities;
    }

    /**
     * This is a main method for ShowLotsView class.
     */
    public void executeView() {
        boolean keepWorking = true;

        while (keepWorking) {
            int choice;
            displayMenu();
            try {
                choice = Integer.parseInt(utilities.getInputString());
                switch (choice) {
                    case 0:
                        keepWorking = false;
                        break;
                    case 1:
                        showLotsSortedBy(OrderKeysForLot.ID);
                        break;
                    case 2:
                        showLotsSortedBy(OrderKeysForLot.DESCRIPTION);
                        break;
                    case 3:
                        showLotsSortedBy(OrderKeysForLot.CATEGORY);
                        break;
                    case 4:
                        showLotsSortedBy(OrderKeysForLot.OWNER);
                        break;
                    default:
                        System.out.println("Incorrect input, try again.");
                }
            } catch (NumberFormatException e) {
                System.out.println("Incorrect input, try again.");
            }
        }
    }

    private void showLotsSortedBy(OrderKeysForLot key) {
        List<Object> request = new ArrayList<>();
        List<Lot> response;
        long limit = Utilities.getDisplayLines();
        long offset = 0L;
        try {
            do {
                request.clear();
                request.add("getLotsSortedByKey");
                request.add(key);
                request.add(limit);
                request.add(offset);
                response = (ArrayList<Lot>) client.sendServerRequest(request);
                if (response.size() == 0)
                    break;
                System.out.println("---id---------------Category------------Description-------OwnerID--------Owner---------");
                for (Lot lot : response)
                    utilities.printLot(lot);
                if (response.size() == limit)
                    System.out.println("--------------------------------------press-Enter--------------------------------------");
                else
                    System.out.println("---------------------------------------------------------------------------------------");
                offset += limit;
            }
            while (response.size() == limit && (utilities.getInputString().equals("") || utilities.getInputString() != null));
        } catch (ClassCastException e) {
            System.out.println("Wrong server response structure.");
        } catch (IncorrectQueryKeyException e) {
            e.printStackTrace();
        } catch (AuctionException e) {
            System.out.println("Unexpected exception in server response: " + e);
        }
    }

    private void displayMenu() {
        System.out.println("-----------------Show lots menu-----------------");
        System.out.println("Show lots sorted by:");
        System.out.println("    1: - ID.");
        System.out.println("    2: - Descriptions.");
        System.out.println("    3: - Category.");
        System.out.println("    4: - Owner.");
        System.out.println("    0:  -For return.\n");
    }
}
