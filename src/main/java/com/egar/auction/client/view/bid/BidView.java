package com.egar.auction.client.view.bid;

import com.egar.auction.client.Client;
import com.egar.auction.client.view.Utilities;

/**
 * BidView class, one of the classes that representing View part of the MVC pattern
 *
 * @author Lisitsyn R.
 */
public class BidView {
    private Utilities utilities;
    private Client client;
    private ShowBidsByLotView showBidsByLotView;
    private ShowBidsView showBidsView;

    /**
     * Constructs new bid view for guest with specified client and utilities class.
     *
     * @param client    client for server query invocation.
     * @param utilities class that contains utility methods and fields.
     */
    public BidView(Client client, Utilities utilities) {
        this.client = client;
        this.utilities = utilities;
        this.showBidsByLotView = new ShowBidsByLotView(client, utilities);
        this.showBidsView = new ShowBidsView(client, utilities);
    }

    /**
     * This is a main method for BidView class.
     */
    public void executeView() {
        boolean keepWorking = true;

        while (keepWorking) {
            int choice;
            displayMenu();
            try {
                choice = Integer.parseInt(utilities.getInputString());
                switch (choice) {
                    case 0:
                        keepWorking = false;
                        break;
                    case 1:
                        showBidsView.executeView();
                        break;
                    case 2:
                        showBidsByLotView.executeView();
                        break;
                    default:
                        System.out.println("Incorrect input, try again.");
                }
            } catch (NumberFormatException e) {
                System.out.println("Incorrect input, try again.");
            }
        }
    }

    private void displayMenu() {
        System.out.println("-----------------Bid Menu-----------------");
        System.out.println("Select:");
        System.out.println("    1: - Show my bids.");
        System.out.println("    2: - Show bids by lot.");
        System.out.println("    0:  -For return.\n");
    }
}
