package com.egar.auction.client.view;

import com.egar.auction.client.Client;
import com.egar.auction.client.view.user.UsersView;

/**
 * AdminView class, one of the classes that representing View part of the MVC pattern
 *
 * @author Lisitsyn R.
 */
public class AdminView {

    private Client client;
    private Utilities utilities;
    private UsersView usersView;
    private MainView mainView;

    /**
     * Constructs new AdminView class with specified MainView,Client,Utilities.
     *
     * @param mainView  MainView.
     * @param client    Client.
     * @param utilities Utilities.
     */
    public AdminView(MainView mainView, Client client, Utilities utilities) {
        this.client = client;
        this.utilities = utilities;
        this.mainView = mainView;
        this.usersView = new UsersView(client, utilities);
    }

    /**
     * This is a main method for AdminView class.
     */
    public void executeView() {
        boolean keepWorking = true;

        while (keepWorking) {
            int choice;
            displayMenu();
            try {
                choice = Integer.parseInt(utilities.getInputString());
                switch (choice) {
                    case 0:
                        keepWorking = false;
                        break;
                    case 1:
                        mainView.lotView.executeView();
                        break;
                    case 2:
                        mainView.showAllCategories();
                        break;
                    case 3:
                        usersView.executeView();
                        break;
                    default:
                        System.out.println("Incorrect input, try again.");
                }
            } catch (NumberFormatException e) {
                System.out.println("Incorrect input, try again.");
            }
        }
    }

    private void displayMenu() {
        System.out.println("-----------------Admin Menu-----------------");
        System.out.println("Select:");
        System.out.println("    1:  -Show lots menu.");
        System.out.println("    2:  -Show categories.");
        System.out.println("    3:  -Show users menu.");
        System.out.println("    0:  -For exit.\n");
    }

}
