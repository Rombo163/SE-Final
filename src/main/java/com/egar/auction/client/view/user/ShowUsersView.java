package com.egar.auction.client.view.user;

import com.egar.auction.client.Client;
import com.egar.auction.client.view.Utilities;
import com.egar.auction.server.OrderKeysForUser;
import com.egar.auction.server.controller.exceptions.AuctionException;
import com.egar.auction.server.controller.exceptions.IncorrectQueryKeyException;
import com.egar.auction.server.domain.User;

import java.util.ArrayList;
import java.util.List;

/**
 * ShowUsersView class, one of the classes that representing View part of the MVC pattern
 *
 * @author Lisitsyn R.
 */
public class ShowUsersView {
    private Utilities utilities;
    private Client client;

    /**
     * Constructs new find users view for guest with specified client and utilities class.
     *
     * @param client    client for server query invocation.
     * @param utilities class that contains utility methods and fields.
     */
    public ShowUsersView(Client client, Utilities utilities) {
        this.client = client;
        this.utilities = utilities;
    }

    /**
     * This is a main method for ShowUsersView class.
     */
    public void executeView() {
        boolean keepWorking = true;

        while (keepWorking) {
            int choice;
            displayMenu();
            try {
                choice = Integer.parseInt(utilities.getInputString());
                switch (choice) {
                    case 0:
                        keepWorking = false;
                        break;
                    case 1:
                        showUsersSortedBy(OrderKeysForUser.ID);
                        break;
                    case 2:
                        showUsersSortedBy(OrderKeysForUser.NAME);
                        break;
                    case 3:
                        showUsersSortedBy(OrderKeysForUser.ROLE);
                        break;
                    case 4:
                        break;
                    default:
                        System.out.println("Incorrect input, try again.");
                }
            } catch (NumberFormatException e) {
                System.out.println("Incorrect input, try again.");
            }
        }
    }

    private void displayMenu() {
        System.out.println("-----------------Show users menu-----------------");
        System.out.println("Show users sorted by:");
        System.out.println("    1: - ID.");
        System.out.println("    2: - Name.");
        System.out.println("    3: - Role.");
        System.out.println("    0:  -For return.\n");
    }

    private void showUsersSortedBy(OrderKeysForUser key) {
        List<Object> request = new ArrayList<>();
        List<User> response;
        long limit = Utilities.getDisplayLines();
        long offset = 0L;
        boolean usersNotFound = true;
        try {
            do {
                request.clear();
                request.add("getUsersSortedByKey");
                request.add(key);
                request.add(limit);
                request.add(offset);
                response = (ArrayList<User>) client.sendServerRequest(request);
                if (response.size() == 0) {
                    if (usersNotFound)
                        System.out.println("Users not found.");
                    break;
                }
                usersNotFound = false;
                System.out.println("---id---------------UserName------------UserRole---");
                for (User user : response)
                    utilities.printUser(user);
                if (response.size() == limit)
                    System.out.println("--------------------press-Enter-------------------");
                else
                    System.out.println("--------------------------------------------------");
                offset += limit;
            }
            while (response.size() == limit && (utilities.getInputString().equals("") || utilities.getInputString() != null));
        } catch (ClassCastException e) {
            System.out.println("Wrong server response structure.");
        } catch (IncorrectQueryKeyException e) {
            e.printStackTrace();
        } catch (AuctionException e) {
            System.out.println("Unexpected exception in server response: " + e);
        }
    }
}
