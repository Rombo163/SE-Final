package com.egar.auction.client.view.user;

import com.egar.auction.client.Client;
import com.egar.auction.client.view.Utilities;
import com.egar.auction.server.controller.exceptions.AuctionException;
import com.egar.auction.server.controller.exceptions.UserWithSuchIdNotFoundException;
import com.egar.auction.server.domain.User;

import java.util.ArrayList;
import java.util.List;

/**
 * FindUserView class, one of the classes that representing View part of the MVC pattern
 *
 * @author Lisitsyn R.
 */
public class FindUserView {

    private Utilities utilities;
    private Client client;

    /**
     * Constructs new find user view for guest with specified client and utilities class.
     *
     * @param client    client for server query invocation.
     * @param utilities class that contains utility methods and fields.
     */
    public FindUserView(Client client, Utilities utilities) {
        this.client = client;
        this.utilities = utilities;
    }

    /**
     * This is a main method for FindUserView class.
     */
    public void executeView() {
        boolean keepWorking = true;

        while (keepWorking) {
            int choice;
            displayMenu();
            try {
                choice = Integer.parseInt(utilities.getInputString());
                switch (choice) {
                    case 0:
                        keepWorking = false;
                        break;
                    case 1:
                        findUserById();
                        break;
                    case 2:
                        findUserByName();
                        break;
                    default:
                        System.out.println("Incorrect input, try again.");
                }
            } catch (NumberFormatException e) {
                System.out.println("Incorrect input, try again.");
            }
        }
    }

    private void displayMenu() {
        System.out.println("-----------------Find users menu-----------------");
        System.out.println("Find user by:");
        System.out.println("    1: - ID.");
        System.out.println("    2: - username.");
        System.out.println("    0:  -For return.\n");
    }

    private void findUserById() {
        System.out.println("   Type user id: \n\t");
        long id = Integer.valueOf(utilities.getInputString());
        List<Object> request = new ArrayList<>();
        Object response;
        try {
            request.add("getUserById");
            request.add(id);
            response = client.sendServerRequest(request);
            User user = (User) response;
            if (response == null)
                System.out.println("Wrong server response structure.");
                //System.out.printf("-------------User with id = %d not found-----------\n", id);
            else {
                System.out.println("---id---------------UserName------------UserRole---");
                utilities.printUser(user);
            }
            System.out.println("--------------------press-Enter-------------------");
        } catch (ClassCastException e) {
            System.out.println("Wrong server response structure.");
        } catch (UserWithSuchIdNotFoundException e) {
            System.out.printf("User with id = %d not found\n", id);
        } catch (AuctionException e) {
            System.out.println("Unexpected exception in server response: " + e);
        }
    }

    private void findUserByName() {
        System.out.println("   Type user name: \n\t");
        String str = utilities.getInputString();
        List<Object> request = new ArrayList<>();
        List<User> response;
        long limit = Utilities.getDisplayLines();
        long offset = 0L;
        boolean serverDidNotFindAnything = true;
        try {
            do {
                request.clear();
                request.add("getUsersByName");
                request.add(str);
                request.add(limit);
                request.add(offset);
                response = (ArrayList<User>) client.sendServerRequest(request);
                if (response.size() == 0) {
                    if (serverDidNotFindAnything)
                        System.out.println("No matches.");
                    break;
                }
                serverDidNotFindAnything = false;
                System.out.println("---id---------------UserName------------UserRole---");
                for (User user : response)
                    utilities.printUser(user);
                if (response.size() == limit)
                    System.out.println("--------------------press-Enter-------------------");
                else
                    System.out.println("--------------------------------------------------");
                offset += limit;
            }
            while (response.size() == limit && (utilities.getInputString().equals("") || utilities.getInputString() != null));
        } catch (UserWithSuchIdNotFoundException e) {
            System.out.println("Users not found.");
        } catch (ClassCastException e) {
            System.out.println("Wrong server response structure.");
        } catch (AuctionException e) {
            System.out.println("Unexpected exception in server response: " + e);
        }
    }
}
