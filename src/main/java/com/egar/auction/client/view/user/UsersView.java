package com.egar.auction.client.view.user;

import com.egar.auction.client.Client;
import com.egar.auction.client.view.Utilities;
import com.egar.auction.server.controller.exceptions.AuctionException;
import com.egar.auction.server.domain.User;

import java.util.ArrayList;
import java.util.List;

/**
 * UsersView class, one of the classes that representing View part of the MVC pattern
 *
 * @author Lisitsyn R.
 */
public class UsersView {
    private Utilities utilities;
    private Client client;
    private FindUserView findUserView;
    private ShowUsersView showUsersView;

    /**
     * Constructs new UsersView for guest with specified client and utilities class.
     *
     * @param client    client for server query invocation.
     * @param utilities class that contains utility methods and fields.
     */
    public UsersView(Client client, Utilities utilities) {
        this.client = client;
        this.utilities = utilities;
        this.findUserView = new FindUserView(client, utilities);
        this.showUsersView = new ShowUsersView(client, utilities);
    }

    /**
     * This is a main method for UsersView class.
     */
    public void executeView() {
        boolean keepWorking = true;

        while (keepWorking) {
            int choice;
            displayMenu();
            try {
                choice = Integer.parseInt(utilities.getInputString());
                switch (choice) {
                    case 0:
                        keepWorking = false;
                        break;
                    case 1:
                        showUsersView.executeView();
                        break;
                    case 2:
                        findUserView.executeView();
                        break;
                    case 3:
                        showUsersStat();
                        break;
                    default:
                        System.out.println("Incorrect input, try again.");
                }
            } catch (NumberFormatException e) {
                System.out.println("Incorrect input, try again.");
            }
        }
    }

    private void displayMenu() {
        System.out.println("-----------------Users menu-----------------");
        System.out.println("Select:");
        System.out.println("    1: - Show users.");
        System.out.println("    2: - Find user.");
        System.out.println("    3: - Show users stat.");
        System.out.println("    0: - For exit.\n");
    }

    private void showUsersStat() {
        List<Object> request = new ArrayList<>();
        List<User> response;
        long limit = Utilities.getDisplayLines();
        long offset = 0;
        boolean usersNotFound = true;
        try {
            do {
                request.clear();
                request.add("getUsers");
                request.add(limit);
                request.add(offset);
                response = (List<User>) client.sendServerRequest(request);
                if (response.size() != 0) {
                    System.out.println("---User#-----UserName-----UserRole-----Lots------Bids---");
                    usersNotFound = false;
                    for (User user : response) {
                        if (user.getRole().getRoleName().equals("RegUser")) {
                            List<Object> innerRequest = new ArrayList<>();
                            innerRequest.add("getNumberOfLotsForUser");
                            innerRequest.add(user.getId());
                            Long numberOfLotsForUser = (Long) client.sendServerRequest(innerRequest);
                            innerRequest.clear();
                            innerRequest.add("getNumberOfBidsForUser");
                            innerRequest.add(user.getId());
                            Long numberOfBidsForUser = (Long) client.sendServerRequest(innerRequest);
                            System.out.printf("   %-10d%-13s%-14s%-10d%-10d\n",
                                    user.getId(),
                                    user.getUserName(),
                                    user.getRole().getRoleName(),
                                    numberOfLotsForUser,
                                    numberOfBidsForUser);
                        } else {
                            System.out.printf("   %-10d%-13s%-13s\n",
                                    user.getId(),
                                    user.getUserName(),
                                    user.getRole().getRoleName());
                        }
                    }
                } else {
                    if (usersNotFound)
                        System.out.println("Users not found.");
                }
                System.out.println("--------------------------------------------------------");
            }
            while (response.size() == limit && (utilities.getInputString().equals("") || utilities.getInputString() != null));
        } catch (ClassCastException e) {
            System.out.println("Incorrect server response structure.");
        } catch (AuctionException e) {
            System.out.println("Unexpected exception in server response.");
        }


    }
}
