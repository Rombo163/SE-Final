package com.egar.auction.web.servlets;

import com.egar.auction.server.dao.CategoryDao;
import com.egar.auction.server.dao.DAOFactory;
import com.egar.auction.server.dao.PersistException;
import com.egar.auction.server.domain.Category;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Servlet implementation class MainServlet.
 */
@WebServlet(name = "ShowCategories", urlPatterns = {"/auction/content/ShowCategories"})
public class ShowCategoriesServlet extends HttpServlet {
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ShowCategoriesServlet() {
    }

    /**
     * @see HttpServlet#doGet(HttpServletRequest, HttpServletResponse)
     */
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            int pageSize = Integer.valueOf(req.getParameter("pageSize"));
            int currentPage = Integer.valueOf(req.getParameter("currentPage"));
            if (pageSize <= 0 || currentPage <= 0) {
                RequestDispatcher dispatcher = req.getRequestDispatcher("/unknown.jsp");
                dispatcher.forward(req, resp);
            }
            Map<Long, Long> numberOfLots = new HashMap();
            List<Category> categories = DAOFactory.getInstance().getCategoryDAO().getWithLimitOffset(pageSize, (currentPage - 1) * pageSize);
            CategoryDao categoryDao = DAOFactory.getInstance().getCategoryDAO();
            for (Category category : categories) {
                numberOfLots.put(category.getId(), categoryDao.countLotsInCategory(category));
            }
            long pageNumber;
            long categoriesNumber = DAOFactory.getInstance().getCategoryDAO().countCategories();
            if (categoriesNumber / pageSize == 0)
                pageNumber = 1;
            else if (categoriesNumber % pageSize == 0)
                pageNumber = categoriesNumber / pageSize;
            else
                pageNumber = categoriesNumber / pageSize + 1;

            req.setAttribute("categories", categories);
            req.setAttribute("numberOfLots", numberOfLots);
            req.setAttribute("currentPage", currentPage);
            req.setAttribute("pageNumber", pageNumber);
            RequestDispatcher dispatcher = req.getRequestDispatcher("/content/ShowCategories.jsp");
            dispatcher.forward(req, resp);


        } catch (PersistException e) {
            e.printStackTrace();
        } catch (NumberFormatException e) {
            RequestDispatcher dispatcher = req.getRequestDispatcher("/unknown.jsp");
            dispatcher.forward(req, resp);
        }
    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest, HttpServletResponse)
     */
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req, resp);
    }
}
