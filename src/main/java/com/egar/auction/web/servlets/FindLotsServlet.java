package com.egar.auction.web.servlets;

import com.egar.auction.server.dao.DAOFactory;
import com.egar.auction.server.dao.PersistException;
import com.egar.auction.server.domain.Category;
import com.egar.auction.server.domain.Lot;
import com.egar.auction.server.domain.User;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Servlet implementation class MainServlet.
 */
@WebServlet(name = "FindLots", urlPatterns = {"/auction/content/FindLots"})
public class FindLotsServlet extends HttpServlet {
    /**
     * @see HttpServlet#HttpServlet()
     */
    public FindLotsServlet() {
    }

    /**
     * @see HttpServlet#doGet(HttpServletRequest, HttpServletResponse)
     */
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            int pageSize = Integer.valueOf(req.getParameter("pageSize"));
            int currentPage = Integer.valueOf(req.getParameter("currentPage"));
            String keyParameterType = req.getParameter("keyParameterType");
            String keyValue = req.getParameter("keyValue");
            if (pageSize <= 0 || currentPage <= 0 || keyParameterType == null || keyValue == null) {
                RequestDispatcher dispatcher = req.getRequestDispatcher("/unknown.jsp");
                dispatcher.forward(req, resp);
            } else {
                List<Lot> foundedLots = null;
                if (keyParameterType.equals("id")) {
                    Lot lot = DAOFactory.getInstance().getLotDAO().getById(Long.valueOf(keyValue));
                    foundedLots = new ArrayList<>();
                    foundedLots.add(lot);
                } else if (keyParameterType.equals("owner")) {
                    User user = DAOFactory.getInstance().getUserDAO().getById(Long.valueOf(keyValue));
                    foundedLots = DAOFactory.getInstance().getLotDAO().getByOwner(user, pageSize, (currentPage - 1) * pageSize);
                } else if (keyParameterType.equals("description")) {
                    foundedLots = DAOFactory.getInstance().getLotDAO().getByDescription(keyValue, pageSize, (currentPage - 1) * pageSize);
                } else if (keyParameterType.equals("category")) {
                    Category category = DAOFactory.getInstance().getCategoryDAO().getById(Long.valueOf(keyValue));
                    foundedLots = DAOFactory.getInstance().getLotDAO().getByCategory(category, pageSize, (currentPage - 1) * pageSize);
                } else {
                    RequestDispatcher dispatcher = req.getRequestDispatcher("/unknown.jsp");
                    dispatcher.forward(req, resp);
                }

                long pageNumber;
                long categoriesNumber = DAOFactory.getInstance().getUserDAO().countUsers();
                if (categoriesNumber / pageSize == 0)
                    pageNumber = 1;
                else if (categoriesNumber % pageSize == 0)
                    pageNumber = categoriesNumber / pageSize;
                else
                    pageNumber = categoriesNumber / pageSize + 1;

                req.setAttribute("currentPage", currentPage);
                req.setAttribute("pageNumber", pageNumber);
                req.setAttribute("foundedLots", foundedLots);
                RequestDispatcher dispatcher = req.getRequestDispatcher("/content/FindLots.jsp");
                dispatcher.forward(req, resp);
            }
        } catch (NumberFormatException e) {
            RequestDispatcher dispatcher = req.getRequestDispatcher("/unknown.jsp");
            dispatcher.forward(req, resp);
        } catch (PersistException e) {
            e.printStackTrace();
        }
    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest, HttpServletResponse)
     */
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req, resp);
    }
}
