package com.egar.auction.web.servlets;

import com.egar.auction.server.dao.DAOFactory;
import com.egar.auction.server.dao.PersistException;
import com.egar.auction.server.domain.User;
import com.egar.auction.web.utils.Utils;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Servlet implementation class MainServlet.
 */
@WebServlet(name = "Authorization", urlPatterns = {"/auction/authorization"})
public class AuthorizationServlet extends HttpServlet {

    /**
     * @see HttpServlet#HttpServlet()
     */
    public AuthorizationServlet() {
    }
    /**
     * @see HttpServlet#doGet(HttpServletRequest, HttpServletResponse)
     */
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String login = req.getParameter("login");
        String password = req.getParameter("pass");
        if (!Utils.isNullOrEmpty(login) && !Utils.isNullOrEmpty(password)) {
            try {
                User user = DAOFactory.getInstance().getUserDAO().getByCredentials(login, password);
                if (user == null) {
                    req.setAttribute("user", null);
                    RequestDispatcher dispatcher = req.getRequestDispatcher("/unknown.jsp");
                    dispatcher.forward(req, resp);
                    //resp.sendRedirect(req.getContextPath() + "/auction");
                } else {
                    req.getSession().setAttribute("user", user);
                    if (user.getRole().getRoleName().equals("RegUser")) {
                        resp.sendRedirect(req.getContextPath() + "/auction");
                    } else if (user.getRole().getRoleName().equals("Admin")) {
                        resp.sendRedirect(req.getContextPath() + "/auction");
                    }
                }
            } catch (PersistException e) {
                e.printStackTrace();
            }
        } else {
            resp.sendRedirect(req.getContextPath() + "/auction");
        }
    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest, HttpServletResponse)
     */
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req, resp);
    }
}
