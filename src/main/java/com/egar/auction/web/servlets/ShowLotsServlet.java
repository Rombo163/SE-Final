package com.egar.auction.web.servlets;

import com.egar.auction.server.OrderKeysForLot;
import com.egar.auction.server.dao.DAOFactory;
import com.egar.auction.server.dao.PersistException;
import com.egar.auction.server.domain.Lot;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * Servlet implementation class MainServlet.
 */
@WebServlet(name = "ShowLots", urlPatterns = {"/auction/content/ShowLots"})
public class ShowLotsServlet extends HttpServlet {
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ShowLotsServlet() {
    }

    /**
     * @see HttpServlet#doGet(HttpServletRequest, HttpServletResponse)
     */
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            int pageSize = Integer.valueOf(req.getParameter("pageSize"));
            int currentPage = Integer.valueOf(req.getParameter("currentPage"));
            String order = req.getParameter("order");
            if (pageSize <= 0 || currentPage <= 0 || order == null) {
                RequestDispatcher dispatcher = req.getRequestDispatcher("/unknown.jsp");
                dispatcher.forward(req, resp);
            }

            List<Lot> lots;
            if (order.equals("category"))
                lots = DAOFactory.getInstance().getLotDAO().getSortedByKey(OrderKeysForLot.CATEGORY, pageSize, (currentPage - 1) * pageSize);
            else if (order.equals("description"))
                lots = DAOFactory.getInstance().getLotDAO().getSortedByKey(OrderKeysForLot.DESCRIPTION, pageSize, (currentPage - 1) * pageSize);
            else if (order.equals("owner"))
                lots = DAOFactory.getInstance().getLotDAO().getSortedByKey(OrderKeysForLot.OWNER, pageSize, (currentPage - 1) * pageSize);
            else
                lots = DAOFactory.getInstance().getLotDAO().getSortedByKey(OrderKeysForLot.ID, pageSize, (currentPage - 1) * pageSize);

            long pageNumber;
            long categoriesNumber = DAOFactory.getInstance().getLotDAO().countLots();
            if (categoriesNumber / pageSize == 0)
                pageNumber = 1;
            else if (categoriesNumber % pageSize == 0)
                pageNumber = categoriesNumber / pageSize;
            else
                pageNumber = categoriesNumber / pageSize + 1;
            req.setAttribute("order", order);
            req.setAttribute("currentPage", currentPage);
            req.setAttribute("pageNumber", pageNumber);
            req.setAttribute("lots", lots);
            RequestDispatcher dispatcher = req.getRequestDispatcher("/content/ShowLots.jsp");
            dispatcher.forward(req, resp);
        } catch (PersistException e) {
            e.printStackTrace();
        } catch (NumberFormatException e) {
            RequestDispatcher dispatcher = req.getRequestDispatcher("/unknown.jsp");
            dispatcher.forward(req, resp);
        }
    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest, HttpServletResponse)
     */
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req, resp);
    }
}
