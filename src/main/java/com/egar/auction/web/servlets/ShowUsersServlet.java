package com.egar.auction.web.servlets;

import com.egar.auction.server.OrderKeysForUser;
import com.egar.auction.server.dao.DAOFactory;
import com.egar.auction.server.dao.PersistException;
import com.egar.auction.server.dao.UserDao;
import com.egar.auction.server.domain.User;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Servlet implementation class MainServlet.
 */
@WebServlet(name = "ShowUsers", urlPatterns = {"/auction/content/Admin/ShowUsers"})
public class ShowUsersServlet extends HttpServlet {
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ShowUsersServlet() {
    }

    /**
     * @see HttpServlet#doGet(HttpServletRequest, HttpServletResponse)
     */
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {

            int pageSize = Integer.valueOf(req.getParameter("pageSize"));
            int currentPage = Integer.valueOf(req.getParameter("currentPage"));
            String order = req.getParameter("order");
            if (pageSize <= 0 || currentPage <= 0 || order == null) {
                RequestDispatcher dispatcher = req.getRequestDispatcher("/unknown.jsp");
                dispatcher.forward(req, resp);
            }
            List<User> users;
            if (order.equals("name"))
                users = DAOFactory.getInstance().getUserDAO().getUsersSortedByKey(OrderKeysForUser.NAME, pageSize, (currentPage - 1) * pageSize);
            else if (order.equals("role"))
                users = DAOFactory.getInstance().getUserDAO().getUsersSortedByKey(OrderKeysForUser.ROLE, pageSize, (currentPage - 1) * pageSize);
            else
                users = DAOFactory.getInstance().getUserDAO().getUsersSortedByKey(OrderKeysForUser.ID, pageSize, (currentPage - 1) * pageSize);
            Map<Long, Long> bidsNumbersForUsers = new HashMap();
            Map<Long, Long> lotsNumbersForUsers = new HashMap();
            UserDao userDao = DAOFactory.getInstance().getUserDAO();
            for (User user : users) {
                if (user.getRole().getRoleName().equals("RegUser")) {
                    bidsNumbersForUsers.put(user.getId(), userDao.countBidsForUser(user));
                    lotsNumbersForUsers.put(user.getId(), userDao.countLotsForUser(user));
                }
            }
            long pageNumber;
            long categoriesNumber = DAOFactory.getInstance().getUserDAO().countUsers();
            if (categoriesNumber / pageSize == 0)
                pageNumber = 1;
            else if (categoriesNumber % pageSize == 0)
                pageNumber = categoriesNumber / pageSize;
            else
                pageNumber = categoriesNumber / pageSize + 1;
            req.setAttribute("order", order);
            req.setAttribute("currentPage", currentPage);
            req.setAttribute("pageNumber", pageNumber);
            req.setAttribute("users", users);
            req.setAttribute("bidsNumbersForUsers", bidsNumbersForUsers);
            req.setAttribute("lotsNumbersForUsers", lotsNumbersForUsers);
            RequestDispatcher dispatcher = req.getRequestDispatcher("/content/Admin/ShowUsersStat.jsp");
            dispatcher.forward(req, resp);

        } catch (PersistException e) {
            e.printStackTrace();
        } catch (NumberFormatException e) {
            RequestDispatcher dispatcher = req.getRequestDispatcher("/unknown.jsp");
            dispatcher.forward(req, resp);
        }
    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest, HttpServletResponse)
     */
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req, resp);
    }
}

