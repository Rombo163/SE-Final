package com.egar.auction.web.servlets;

import com.egar.auction.server.dao.DAOFactory;
import com.egar.auction.server.dao.PersistException;
import com.egar.auction.server.dao.UserDao;
import com.egar.auction.server.domain.User;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Servlet implementation class MainServlet.
 */
@WebServlet(name = "FindUsers", urlPatterns = {"/auction/content/Admin/FindUsers"})
public class FindUsersServlet extends HttpServlet {
    /**
     * @see HttpServlet#HttpServlet()
     */
    public FindUsersServlet() {
    }

    /**
     * @see HttpServlet#doGet(HttpServletRequest, HttpServletResponse)
     */
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {

            int pageSize = Integer.valueOf(req.getParameter("pageSize"));
            int currentPage = Integer.valueOf(req.getParameter("currentPage"));
            String keyParameterType = req.getParameter("keyParameterType");
            String keyValue = req.getParameter("keyValue");
            if (pageSize <= 0 || currentPage <= 0 || keyParameterType == null || keyValue == null) {
                RequestDispatcher dispatcher = req.getRequestDispatcher("/unknown.jsp");
                dispatcher.forward(req, resp);
            } else {
                List<User> foundedUsers = null;
                if (keyParameterType.equals("id")) {
                    User user = DAOFactory.getInstance().getUserDAO().getById(Long.valueOf(keyValue));
                    foundedUsers = new ArrayList<>();
                    foundedUsers.add(user);
                } else if (keyParameterType.equals("owner")) {
                    foundedUsers = DAOFactory.getInstance().getUserDAO().getByUserName(keyValue, pageSize, (currentPage - 1) * pageSize);
                } else {
                    RequestDispatcher dispatcher = req.getRequestDispatcher("/unknown.jsp");
                    dispatcher.forward(req, resp);
                }
                long pageNumber;
                long categoriesNumber = DAOFactory.getInstance().getUserDAO().countUsers();
                if (categoriesNumber / pageSize == 0)
                    pageNumber = 1;
                else if (categoriesNumber % pageSize == 0)
                    pageNumber = categoriesNumber / pageSize;
                else
                    pageNumber = categoriesNumber / pageSize + 1;

                Map<Long, Long> bidsNumbersForUsers = new HashMap();
                Map<Long, Long> lotsNumbersForUsers = new HashMap();
                UserDao userDao = DAOFactory.getInstance().getUserDAO();
                for (User user : foundedUsers) {
                    if (user.getRole().getRoleName().equals("RegUser")) {
                        bidsNumbersForUsers.put(user.getId(), userDao.countBidsForUser(user));
                        lotsNumbersForUsers.put(user.getId(), userDao.countLotsForUser(user));
                    }
                }
                req.setAttribute("bidsNumbersForUsers", bidsNumbersForUsers);
                req.setAttribute("lotsNumbersForUsers", lotsNumbersForUsers);
                req.setAttribute("currentPage", currentPage);
                req.setAttribute("pageNumber", pageNumber);
                req.setAttribute("foundedUsers", foundedUsers);
                RequestDispatcher dispatcher = req.getRequestDispatcher("/content/Admin/FindUsers.jsp");
                dispatcher.forward(req, resp);
            }

        } catch (NumberFormatException e) {
            RequestDispatcher dispatcher = req.getRequestDispatcher("/unknown.jsp");
            dispatcher.forward(req, resp);
        } catch (PersistException e) {
            e.printStackTrace();
        }
    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest, HttpServletResponse)
     */
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req, resp);
    }
}
