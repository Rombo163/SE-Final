package com.egar.auction.web.utils;

/**
 * Util class
 *
 * @author Lisitsyn.
 */
public class Utils {

    /**
     * Returns true, if param is null or empty.
     *
     * @param param String to check
     * @return isNullOrEmpty result
     */
    public static boolean isNullOrEmpty(String param) {
        return (param == null || param.isEmpty());
    }
}
