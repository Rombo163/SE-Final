package com.egar.auction.server.controller.exceptions;

/**
 * Common ancestor for all exceptions.
 *
 * @author Lisicyn R.
 */
public class AuctionException extends Exception {
    /**
     * @see Exception#Exception()
     */
    public AuctionException() {
    }

    /**
     * @see Exception#Exception(String)
     */
    public AuctionException(String message) {
        super(message);
    }

    /**
     * @see Exception#Exception(String, Throwable)
     */
    public AuctionException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * @see Exception#Exception(Throwable)
     */
    public AuctionException(Throwable cause) {
        super(cause);
    }

    /**
     * @see Exception#Exception(String, Throwable, boolean, boolean)
     */
    public AuctionException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
