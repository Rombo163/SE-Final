package com.egar.auction.server.controller.exceptions;

/**
 * Signals that an User was not found.
 *
 * @author Lisicyn R.
 */
public class UserWithSuchIdNotFoundException extends AuctionException {
    /**
     * @see Exception#Exception()
     */
    public UserWithSuchIdNotFoundException() {
    }

    /**
     * @see Exception#Exception(String)
     */
    public UserWithSuchIdNotFoundException(String message) {
        super(message);
    }

    /**
     * @see Exception#Exception(String, Throwable)
     */
    public UserWithSuchIdNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * @see Exception#Exception(Throwable)
     */
    public UserWithSuchIdNotFoundException(Throwable cause) {
        super(cause);
    }

    /**
     * @see Exception#Exception(String, Throwable, boolean, boolean)
     */
    public UserWithSuchIdNotFoundException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
