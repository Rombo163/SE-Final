package com.egar.auction.server.controller.exceptions;

/**
 * Exception used to indicate that lot is not sold yet.
 *
 * @author Lisitsyn R.
 */
public class LotIsNotSoldException extends AuctionException {
    /**
     * @see Exception#Exception()
     */
    public LotIsNotSoldException() {
    }

    /**
     * @see Exception#Exception(String)
     */
    public LotIsNotSoldException(String message) {
        super(message);
    }

    /**
     * @see Exception#Exception(String, Throwable)
     */
    public LotIsNotSoldException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * @see Exception#Exception(Throwable)
     */
    public LotIsNotSoldException(Throwable cause) {
        super(cause);
    }

    /**
     * @see Exception#Exception(String, Throwable, boolean, boolean)
     */
    public LotIsNotSoldException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
