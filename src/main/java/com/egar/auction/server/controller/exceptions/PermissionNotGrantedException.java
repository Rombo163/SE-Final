package com.egar.auction.server.controller.exceptions;

/**
 * @author Lisicyn R.
 */
public class PermissionNotGrantedException extends AuctionException {
    /**
     * Signals that current user has no rights to execute controller's method.
     *
     * @see Exception#Exception()
     */
    public PermissionNotGrantedException() {
    }

    /**
     * @see Exception#Exception(String)
     */
    public PermissionNotGrantedException(String message) {
        super(message);
    }

    /**
     * @see Exception#Exception(String, Throwable)
     */
    public PermissionNotGrantedException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * @see Exception#Exception(Throwable)
     */
    public PermissionNotGrantedException(Throwable cause) {
        super(cause);
    }

    /**
     * @see Exception#Exception(String, Throwable, boolean, boolean)
     */
    public PermissionNotGrantedException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
