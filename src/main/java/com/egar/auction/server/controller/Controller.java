package com.egar.auction.server.controller;

import com.egar.auction.server.OrderKeysForBid;
import com.egar.auction.server.OrderKeysForLot;
import com.egar.auction.server.OrderKeysForUser;
import com.egar.auction.server.controller.exceptions.*;
import com.egar.auction.server.dao.*;
import com.egar.auction.server.domain.*;

import java.util.List;

import static java.lang.Math.acos;
import static java.lang.Math.cos;
import static java.lang.Math.sin;

/**
 * An implementation of "controller" part of MVC pattern.
 *
 * @author Lisitsyn Roman
 */
public class Controller {

    private DAOFactory factory;
    private final static int R = 6372795;

    /**
     * Constructs new Controller.
     */
    public Controller() {
        this.factory = DAOFactory.getInstance();
    }

    /**
     * Closes controller resources.
     */
    public void closeResources() {
        this.factory.close();
    }

    /**
     * Returns user from persistent data storage by his credentials
     * or null if user with such credentials was not found.
     *
     * @param userName name of the user.
     * @param password password of the user.
     * @return User user by his name and password.
     */
    public User getUserByHisCredentials(String userName, String password) {
        try {
            return DAOFactory.getInstance().getUserDAO().getByCredentials(userName, password);
        } catch (PersistException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Returns list of the lots sorted be one of parameters depending on the key value.
     *
     * @param key    key value for query it can take one of the values id,description,category,owner.
     * @param limit  limit for sql query.
     * @param offset offset for sql query.
     * @return List of the lots.
     * @throws IncorrectQueryKeyException
     */
    public List<Lot> getLotsSortedByKey(OrderKeysForLot key, long limit, long offset) throws IncorrectQueryKeyException {
        try {
            return factory.getLotDAO().getSortedByKey(key, limit, offset);
        } catch (PersistException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Returns all lots that belong to specified user.
     *
     * @param ownerId owner id.
     * @param limit   limit for sql query.
     * @param offset  offset for sql query.
     * @return List of Lots that contains lots owned by specified user.
     * @throws UserWithSuchIdNotFoundException
     */
    public List<Lot> getLotsByOwner(long ownerId, long limit, long offset) throws UserWithSuchIdNotFoundException {
        try {
            User user = factory.getUserDAO().getById(ownerId);
            if (user == null)
                throw new UserWithSuchIdNotFoundException();
            else
                return factory.getLotDAO().getByOwner(user, limit, offset);
        } catch (PersistException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Returns lot with specified id.
     *
     * @param id lot id.
     * @return lot with specified id.
     * @throws LotWithSuchIdNotFoundException if lot with such id was not found.
     */
    public Lot getLotById(Long id) throws LotWithSuchIdNotFoundException {
        try {
            Lot lot = factory.getLotDAO().getById(id);
            if (lot != null)
                return lot;
            else
                throw new LotWithSuchIdNotFoundException("Lot with id = " + id + " not found.");
        } catch (PersistException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Returns a List of lots in specified category.
     *
     * @param categoryName Name of the Category that contains needed lots.
     * @param limit        limit for sql query.
     * @param offset       offset  for sql query.
     * @return List of Lots that contains lots in specified category.
     * @throws CategoryNotFoundException
     */
    public List<Lot> getAllLotsInCategory(String categoryName, long limit, long offset) throws CategoryNotFoundException {
        List<Lot> lots;
        try {
            Category category = factory.getCategoryDAO().getByCategoryName(categoryName);
            if (category == null || category.getId() == null)
                throw new CategoryNotFoundException();
            LotDao lotDao = factory.getLotDAO();
            lots = lotDao.getByCategory(category, limit, offset);
            return lots;
        } catch (PersistException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Returns lots descriptions of which contain key value as part of it.
     *
     * @param key    key string.
     * @param limit  limit for sql query.
     * @param offset offset for sql query.
     * @return list of the lots.
     */
    public List<Lot> getLotsByDescription(String key, long limit, long offset) {
        try {
            return factory.getLotDAO().getByDescription(key, limit, offset);
        } catch (PersistException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Return List of the bids with specified owner and sorted by one of the fields depending of the key value.
     *
     * @param userId specified owner.
     * @param key    key value, it can take one of the values id, lot.
     * @param limit  limit for sql query.
     * @param offset offset for sql query.
     * @return list if the bids.
     */
    public List<Bid> getBidsByOwnerSortedByKey(long userId, OrderKeysForBid key, long limit, long offset) {
        try {
            User user = factory.getUserDAO().getById(userId);
            if (user != null) {
                return factory.getBidDAO().getBidsByOwnerWithOrderByKey(key, user, limit, offset);
            }
        } catch (PersistException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Returns List of the bids with specified lotId and sorted by one of the fields depending of the key value.
     *
     * @param lotId  specified lotId
     * @param key    key value, it can take one of the values id, bid_owner.
     * @param limit  limit for sql query.
     * @param offset offset for sql query.
     * @return Returns List of the bids with specified lotId.
     * @throws LotWithSuchIdNotFoundException if loy with such lotId is not exist.
     * @throws IncorrectQueryKeyException     if query key has incorrect value.
     */
    public List<Bid> getBidsByLotSortedByKey(long lotId, OrderKeysForBid key, long limit, long offset) throws LotWithSuchIdNotFoundException, IncorrectQueryKeyException {
        try {
            Lot lot = factory.getLotDAO().getById(lotId);
            if (lot != null) {
                return factory.getBidDAO().getBidsByLotWithOrderByKey(key, lot, limit, offset);
            }
        } catch (PersistException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Returns a List of Categories that contains categories in storage.
     *
     * @param limit  limit for sql query.
     * @param offset offset  for sql query.
     * @return List of Categories that contains categories in storage.
     */
    public List<Category> getCategories(long limit, long offset) {
        List<Category> categories;
        try {
            CategoryDao categoryDao = factory.getCategoryDAO();
            categories = categoryDao.getWithLimitOffset(limit, offset);
            return categories;
        } catch (PersistException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Returns delivery cost for chosen delivery service of the lot,
     * or average delivery cost if delivery service was not chosen.
     *
     * @param lotId Lot id.
     * @return Delivery cost.
     * @throws LotWithSuchIdNotFoundException if lot with id = lotId not found.
     * @throws LotIsNotSoldException          if lot with id = lotId not sold yet.
     */
    public Double getDeliveryCostForLot(long lotId) throws LotIsNotSoldException, LotWithSuchIdNotFoundException {
        try {
            Lot lot = factory.getLotDAO().getById(lotId);
            if (lot == null)
                throw new LotWithSuchIdNotFoundException("Lot with id = " + lotId + " not found.");
            else if (lot.isLotSold()) {
                if (lot.getDeliveryService() != null) {
                    return lot.getDeliveryService().getDistancePrice() * getDistance(lot.getBuyer(), lot)
                            + lot.getDeliveryService().getVolumePrice() * getVolume(lot)
                            + lot.getDeliveryService().getWeightPrice() * lot.getWeight() * lot.getQuantity();
                } else {
                    return getAverageDistancePrice() * getDistance(lot.getBuyer(), lot)
                            + getAverageVolumePrice() * getVolume(lot)
                            + getAverageWeightPrice() * lot.getWeight() * lot.getQuantity();
                }
            } else
                throw new LotIsNotSoldException("Lot with lot id = " + lotId + " is not sold yet.");
        } catch (PersistException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Returns list of sold lots.
     *
     * @param limit  limit for sql query.
     * @param offset offset for sql query.
     * @return list of sold lots.
     */
    public List<Lot> getLotsBySold(long buyerId, long limit, long offset) {
        try {
            return factory.getLotDAO().getBySold(true, buyerId, limit, offset);
        } catch (PersistException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Returns list of users.
     *
     * @param limit  limit for sql query.
     * @param offset offset for sql query.
     * @return list of users.
     */
    public List<User> getUsers(long limit, long offset) {
        try {
            return factory.getUserDAO().getUsers(limit, offset);
        } catch (PersistException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Returns number of lots for specified user.
     *
     * @param userId registered user id.
     * @return Number of lots for specified user.
     * @throws UserWithSuchIdNotFoundException
     */
    public Long getNumberOfLotsForUser(long userId) throws UserWithSuchIdNotFoundException {
        try {
            User user = factory.getUserDAO().getById(userId);
            if (user != null) {
                return factory.getUserDAO().countLotsForUser(user);
            } else
                throw new UserWithSuchIdNotFoundException("User with id = " + userId + " is not exist.");
        } catch (PersistException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Returns number of bids for specified user.
     *
     * @param userId registered user id.
     * @return Number of bids for specified user.
     * @throws UserWithSuchIdNotFoundException
     */
    public Long getNumberOfBidsForUser(long userId) throws UserWithSuchIdNotFoundException {
        try {
            User user = factory.getUserDAO().getById(userId);
            if (user != null) {
                return factory.getUserDAO().countBidsForUser(user);
            } else
                throw new UserWithSuchIdNotFoundException("User with id = " + userId + " is not exist.");
        } catch (PersistException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Returns User by specified id throws  UserWithSuchIdNotFoundException if user with such id was not found.
     *
     * @param id specified id.
     * @return User by specified id
     * @throws UserWithSuchIdNotFoundException if user with such id was not found.
     */
    public User getUserById(long id) throws UserWithSuchIdNotFoundException {
        try {
            User user = factory.getUserDAO().getById(id);
            if (user != null)
                return user;
            else
                throw new UserWithSuchIdNotFoundException("User with id = " + id + " is not exist.");
        } catch (PersistException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Returns List of the users, names of which contain key string as part of it.
     *
     * @param name   key string.
     * @param limit  limit for sql query.
     * @param offset offset for sql query.
     * @return returns List of the users.
     */
    public List<User> getUsersByName(String name, long limit, long offset) {
        try {
            return factory.getUserDAO().getByUserName(name, limit, offset);
        } catch (PersistException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Return List of the users with  sorted by one of the fields depending of the key value.
     *
     * @param key    string key
     * @param limit  limit for sql query.
     * @param offset offset for sql query.
     * @return Return List of the users.
     */
    public List<User> getUsersSortedByKey(OrderKeysForUser key, long limit, long offset) {
        try {
            return factory.getUserDAO().getUsersSortedByKey(key, limit, offset);
        } catch (PersistException e) {
            e.printStackTrace();
        }
        return null;
    }

    private float getVolume(Lot lot) {
        return ((lot.getHeight() * lot.getWidth() * lot.getDepth()) * lot.getQuantity());
    }

    private double getDistance(User buyer, Lot lot) {
        return R * acos(sin(lot.getOwner().getLatitude()) * sin(buyer.getLatitude()) +
                cos(lot.getOwner().getLatitude()) * cos(buyer.getLatitude()) * cos(lot.getOwner().getLongitude()
                        - buyer.getLongitude()));
    }

    private Double getAverageWeightPrice() {
        try {
            return factory.getDeliveryServiceDAO().getAverageWeightPrice();
        } catch (PersistException e) {
            e.printStackTrace();
        }
        return 0d;
    }

    private Double getAverageVolumePrice() {
        try {
            return factory.getDeliveryServiceDAO().getAverageVolumePrice();
        } catch (PersistException e) {
            e.printStackTrace();
        }
        return 0d;
    }

    private Double getAverageDistancePrice() {
        try {
            return factory.getDeliveryServiceDAO().getAverageDistancePrice();
        } catch (PersistException e) {
            e.printStackTrace();
        }
        return 0d;
    }


}
