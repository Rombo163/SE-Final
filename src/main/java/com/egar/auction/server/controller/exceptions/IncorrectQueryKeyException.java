package com.egar.auction.server.controller.exceptions;

/**
 * Exception used to indicate that query contains incorrect key for search method or any other purpose.
 *
 * @author Lisitsyn R.
 */
public class IncorrectQueryKeyException extends AuctionException {
    /**
     * @see Exception#Exception()
     */
    public IncorrectQueryKeyException() {
    }

    /**
     * @see Exception#Exception(String)
     */
    public IncorrectQueryKeyException(String message) {
        super(message);
    }

    /**
     * @see Exception#Exception(String, Throwable)
     */
    public IncorrectQueryKeyException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * @see Exception#Exception(Throwable)
     */
    public IncorrectQueryKeyException(Throwable cause) {
        super(cause);
    }

    /**
     * @see Exception#Exception(String, Throwable, boolean, boolean)
     */
    public IncorrectQueryKeyException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
