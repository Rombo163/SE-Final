package com.egar.auction.server.controller.exceptions;

/**
 * Exception used to indicate a problem with com.egar.auction.server query structure.
 *
 * @author Lisicyn R.
 */
public class WrongServerQueryStructureException extends AuctionException {
    /**
     * @see Exception#Exception()
     */
    public WrongServerQueryStructureException() {
    }

    /**
     * @see Exception#Exception(String)
     */
    public WrongServerQueryStructureException(String message) {
        super(message);
    }

    /**
     * @see Exception#Exception(String, Throwable)
     */
    public WrongServerQueryStructureException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * @see Exception#Exception(Throwable)
     */
    public WrongServerQueryStructureException(Throwable cause) {
        super(cause);
    }

    /**
     * @see Exception#Exception(String, Throwable, boolean, boolean)
     */
    public WrongServerQueryStructureException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}