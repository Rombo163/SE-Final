package com.egar.auction.server.controller.exceptions;

/**
 * Signals that an Lot was not found.
 *
 * @author Lisicyn R.
 */
public class LotWithSuchIdNotFoundException extends AuctionException {
    /**
     * @see Exception#Exception()
     */
    public LotWithSuchIdNotFoundException() {
    }

    /**
     * @see Exception#Exception(String)
     */
    public LotWithSuchIdNotFoundException(String message) {
        super(message);
    }

    /**
     * @see Exception#Exception(String, Throwable)
     */
    public LotWithSuchIdNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * @see Exception#Exception(Throwable)
     */
    public LotWithSuchIdNotFoundException(Throwable cause) {
        super(cause);
    }

    /**
     * @see Exception#Exception(String, Throwable, boolean, boolean)
     */
    public LotWithSuchIdNotFoundException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
