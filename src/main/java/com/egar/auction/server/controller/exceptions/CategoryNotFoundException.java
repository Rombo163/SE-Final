package com.egar.auction.server.controller.exceptions;

/**
 *  Signals that an Category was not found.
 *
 * @author Lisicyn R.
 */
public class CategoryNotFoundException extends AuctionException {
    /**
     * @see Exception#Exception()
     */
    public CategoryNotFoundException() {
    }

    /**
     * @see Exception#Exception(String)
     */
    public CategoryNotFoundException(String message) {
        super(message);
    }

    /**
     * @see Exception#Exception(String, Throwable)
     */
    public CategoryNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * @see Exception#Exception(Throwable)
     */
    public CategoryNotFoundException(Throwable cause) {
        super(cause);
    }

    /**
     * @see Exception#Exception(String, Throwable, boolean, boolean)
     */
    public CategoryNotFoundException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
