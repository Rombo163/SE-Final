package com.egar.auction.server.domain;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Lot model for auction.
 *
 * @author Lisitsyn Roman
 */
@Entity
@Table(name = "lot")
public class Lot implements Serializable {
    @Id
    private long id;
    @Column(name = "price")
    private int price;
    @ManyToOne
    @JoinColumn(name = "lot_owner")
    private User owner;
    @Column(name = "description")
    private String description;
    @ManyToOne
    @JoinColumn(name = "category")
    private Category category;
    @Column(name = "quantity")
    private int quantity;
    @Column(name = "height")
    private float height;
    @Column(name = "width")
    private float width;
    @Column(name = "depth")
    private float depth;
    @Column(name = "weight")
    private float weight;
    @ManyToOne
    @JoinColumn(name = "delivery")
    private DeliveryService deliveryService;
    @ManyToOne
    @JoinColumn(name = "buyer")
    private User buyer;
    @Column(name = "lotsold")
    private boolean lotSold;

    public Lot() {
    }

    /**
     * Constructs Lot with specified id, price, category, description and owner.
     *
     * @param id          Unique id number for this lot.
     * @param price       Price for this lot.
     * @param category    Category which will be contain this lot.
     * @param description Description for this lot.
     * @param owner       User who created this lot.
     * @param quantity    Quantity of products.
     * @param height      Height of the product.
     * @param width       Width of the product.
     * @param depth       Depth of the product.
     * @param weight      Weight of the product.
     */
    public Lot(long id, int price, Category category, String description, User owner, int quantity, float height, float width, float depth, float weight) {
        this.id = id;
        this.price = price;
        this.category = category;
        this.description = description;
        this.owner = owner;
        this.quantity = quantity;
        this.height = height;
        this.width = width;
        this.depth = depth;
        this.weight = weight;
        this.lotSold = false;
        this.deliveryService = null;
        this.buyer = null;
    }

    /**
     * Returns id of the Lot.
     *
     * @return Returns id of the Lot.
     */
    public Long getId() {
        return id;
    }

    /**
     * Returns Category of the Lot.
     *
     * @return Returns Category of the Lot.
     */
    public Category getCategory() {
        return category;
    }

    /**
     * Returns Owner of the Lot.
     *
     * @return Returns Owner of the Lot.
     */
    public User getOwner() {
        return owner;
    }

    /**
     * Returns Price of the Lot.
     *
     * @return Returns Price of the Lot.
     */
    public int getPrice() {
        return price;
    }

    /**
     * Returns DeliverService of the lot.
     *
     * @return DeliverService of the lot.
     */
    public DeliveryService getDeliveryService() {
        return deliveryService;
    }

    /**
     * Sets DeliverService of the lot.
     *
     * @param deliveryService DeliverService of the lot.
     */
    public void setDeliveryService(DeliveryService deliveryService) {
        this.deliveryService = deliveryService;
    }

    /**
     * Returns Buyer of the lot.
     *
     * @return Buyer of the lot.
     */
    public User getBuyer() {
        return buyer;
    }

    /**
     * Sets Buyer of the lot.
     *
     * @param buyer Buyer of the lot.
     */
    public void setBuyer(User buyer) {
        this.buyer = buyer;
    }

    /**
     * Returns "lotSold" flag of the lot.
     *
     * @return Flag "lotSold"
     */
    public boolean isLotSold() {
        return lotSold;
    }

    /**
     * Sets "lotSold" flag of the lot.
     *
     * @param lotSold Flag "lotSold" f the lot.
     */
    public void setLotSold(boolean lotSold) {
        this.lotSold = lotSold;
    }

    /**
     * Returns weight of the lot.
     *
     * @return Weight of the lot.
     */
    public float getWeight() {
        return weight;
    }

    /**
     * Returns depth of the lot.
     *
     * @return Depth of the lot.
     */
    public float getDepth() {
        return depth;
    }

    /**
     * Returns width of the lot.
     *
     * @return Width of the lot.
     */
    public float getWidth() {
        return width;
    }

    /**
     * Returns height of the lot.
     *
     * @return Height of the lot.
     */
    public float getHeight() {
        return height;
    }

    /**
     * Returns quantity of the lot.
     *
     * @return Quantity of the lot.
     */
    public int getQuantity() {
        return quantity;
    }

    /**
     * @return description of the lot.
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set.
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @param category the category to set.
     */
    public void setCategory(Category category) {
        this.category = category;
    }

    /**
     * @param quantity the quantity to set.
     */
    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    /**
     * @param height the height to set.
     */
    public void setHeight(float height) {
        this.height = height;
    }

    /**
     * @param width the width to set.
     */
    public void setWidth(float width) {
        this.width = width;
    }

    /**
     * @param depth the depth to set.
     */
    public void setDepth(float depth) {
        this.depth = depth;
    }

    /**
     * @param weight the weight to set.
     */
    public void setWeight(float weight) {
        this.weight = weight;
    }

    /**
     * @param id the id to set.
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * @param price the price to set.
     */
    public void setPrice(int price) {
        this.price = price;
    }

    /**
     * @param owner the owner to set.
     */
    public void setOwner(User owner) {
        this.owner = owner;
    }

    /**
     * Returns a string representation of the Lot.
     *
     * @return Returns a string representation of the Lot.
     */
    @Override
    public String toString() {
        return String.format("   %-10d%-14d%-11d%-11s%-14s%-15s",
                id, quantity, price, owner, category.getCategoryName(), description);
    }

}
