package com.egar.auction.server.domain;


import javax.persistence.*;
import java.io.Serializable;

/**
 * Bid model for auction.
 *
 * @author Lisitsyn Roman
 */
@Entity
@Table(name = "bid")
public class Bid implements Serializable {

    @Id
    private long id;
    @ManyToOne
    @JoinColumn(name = "bid_owner")
    private User owner;
    @Column(name = "value")
    private int valueOfBid;
    @ManyToOne
    @JoinColumn(name = "lot")
    private Lot lot;

    /**
     * Constructs new Bid without parameters.
     */
    public Bid() {
    }

    /**
     * Constructs Bid with specified id, owner, lot and valueOfBid.
     *
     * @param id         id
     * @param owner      User who created this bid.
     * @param lot        Lot for that was created this bid.
     * @param valueOfBid Value that User offers for lot.
     */
    public Bid(long id, User owner, Lot lot, int valueOfBid) {
        this.id = id;
        this.owner = owner;
        this.lot = lot;
        this.valueOfBid = valueOfBid;
    }

    /**
     * Returns owner of the bid.
     *
     * @return Returns owner of the bid.
     */
    public User getOwner() {
        return owner;
    }

    /**
     * Returns lot of the bid.
     *
     * @return Returns lot of the bid.
     */
    public Lot getLot() {
        return lot;
    }

    /**
     * Returns bid id.
     */
    public long getId() {
        return id;
    }

    /**
     * Sets id of the bid.
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * Sets owner of the bid.
     */
    public void setOwner(User owner) {
        this.owner = owner;
    }

    /**
     * Returns bid value.
     */
    public int getValueOfBid() {
        return valueOfBid;
    }

    /**
     * Sets value of the bid.
     */
    public void setValueOfBid(int valueOfBid) {
        this.valueOfBid = valueOfBid;
    }

    /**
     * Sets lot of the bid.
     */
    public void setLot(Lot lot) {
        this.lot = lot;
    }
}
