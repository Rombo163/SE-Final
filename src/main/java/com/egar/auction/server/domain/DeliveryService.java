package com.egar.auction.server.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * This class provides a implementation of the Delivery Service entity.
 *
 * @author Lisitsyn Roman
 */
@Entity
@Table(name = "delivery")
public class DeliveryService implements Serializable {

    @Id
    private long id;
    @Column(name = "name")
    private String title;
    @Column(name = "w_price")
    private float weightPrice;
    @Column(name = "v_price")
    private float volumePrice;
    @Column(name = "d_price")
    private float distancePrice;
    @Column(name = "min_weight")
    private double minWeight;
    @Column(name = "max_weight")
    private double maxWeight;
    @Column(name = "min_volume")
    private double minVolume;
    @Column(name = "max_volume")
    private double maxVolume;
    @Column(name = "min_distance")
    private double minDistance;
    @Column(name = "max_distance")
    private double maxDistance;

    /**
     * Constructs new DeliveryService without parametres.
     */
    public DeliveryService() {

    }

    /**
     * Constructs DeliveryService with specified name, weightPrice,volumePrice and distancePrice.
     *
     * @param id            Service id.
     * @param serviceName   Name of the Service.
     * @param weightPrice   The price of one weight unit.
     * @param volumePrice   The price of one volume unit.
     * @param distancePrice The price of one distance unit.
     * @param minWeight     Minimum weight for delivery.
     * @param maxWeight     Maximum weight for delivery.
     * @param minVolume     Minimum volume for delivery.
     * @param maxVolume     Maximum volume for delivery.
     * @param minDistance   Minimum distance for delivery.
     * @param maxDistance   Maximum distance for delivery.
     */
    public DeliveryService(long id, String serviceName, float weightPrice, float volumePrice, float distancePrice, double minWeight, double maxWeight, double minVolume, double maxVolume, double minDistance, double maxDistance) {
        this.id = id;
        this.title = serviceName;
        this.weightPrice = weightPrice;
        this.volumePrice = volumePrice;
        this.distancePrice = distancePrice;
        this.minWeight = minWeight;
        this.maxWeight = maxWeight;
        this.minVolume = minVolume;
        this.maxVolume = maxVolume;
        this.minDistance = minDistance;
        this.maxDistance = maxDistance;
    }

    /**
     * Returns bid id.
     */
    public Long getId() {
        return id;
    }

    /**
     * Sets bid id.
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * Sets bid title.
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * Sets bid weight price.
     */
    public void setWeightPrice(float weightPrice) {
        this.weightPrice = weightPrice;
    }

    /**
     * Sets bid volume price.
     */
    public void setVolumePrice(float volumePrice) {
        this.volumePrice = volumePrice;
    }

    /**
     * Sets bid distance price.
     */
    public void setDistancePrice(float distancePrice) {
        this.distancePrice = distancePrice;
    }

    /**
     * Returns bid min_weight.
     */
    public double getMinWeight() {
        return minWeight;
    }

    /**
     * Sets bid min_weight.
     */
    public void setMinWeight(double minWeight) {
        this.minWeight = minWeight;
    }

    /**
     * Returns bid max_weight.
     */
    public double getMaxWeight() {
        return maxWeight;
    }

    /**
     * Sets bid max_weight.
     */
    public void setMaxWeight(double maxWeight) {
        this.maxWeight = maxWeight;
    }

    /**
     * Returns bid min_volume.
     */
    public double getMinVolume() {
        return minVolume;
    }

    /**
     * Sets bid min_volume.
     */
    public void setMinVolume(double minVolume) {
        this.minVolume = minVolume;
    }

    /**
     * Returns bid max_volume.
     */
    public double getMaxVolume() {
        return maxVolume;
    }

    /**
     * Sets bid max_volume.
     */
    public void setMaxVolume(double maxVolume) {
        this.maxVolume = maxVolume;
    }

    /**
     * Returns bid min_distance.
     */
    public double getMinDistance() {
        return minDistance;
    }

    /**
     * Sets bid min_distance.
     */
    public void setMinDistance(double minDistance) {
        this.minDistance = minDistance;
    }

    /**
     * Returns bid max_distance.
     */
    public double getMaxDistance() {
        return maxDistance;
    }

    /**
     * Sets bid max_distance.
     */
    public void setMaxDistance(double maxDistance) {
        this.maxDistance = maxDistance;
    }

    /**
     * Returns title of the DeliveryService.
     *
     * @return Title of the DeliveryService.
     */
    public String getTitle() {
        return title;
    }

    /**
     * Returns weightPrice of the DeliveryService.
     *
     * @return WeightPrice of the DeliveryService.
     */
    public float getWeightPrice() {
        return weightPrice;
    }

    /**
     * Returns volumePrice of the DeliveryService.
     *
     * @return VolumePrice of the DeliveryService.
     */
    public float getVolumePrice() {
        return volumePrice;
    }

    /**
     * Returns distancePrice of the DeliveryService.
     *
     * @return DistancePrice of the DeliveryService.
     */
    public float getDistancePrice() {
        return distancePrice;
    }
}