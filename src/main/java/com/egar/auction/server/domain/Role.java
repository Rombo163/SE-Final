package com.egar.auction.server.domain;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Role model for auction.
 *
 * @author Lisitsyn R.
 */
@Entity
@Table(name = "role")
public class Role implements Serializable {
    @Id
    //@GeneratedValue(strategy = GenerationType.IDENTITY)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "role_id_seq")
    @SequenceGenerator(name = "role_id_seq",sequenceName = "role_id_seq",allocationSize = 1)
    private Long id;
    @Column(name = "name")
    private String roleName;

    /**
     * Constructs new Role with specified role name.
     *
     * @param roleName role name.
     */
    public Role(String roleName) {
        this.roleName = roleName;
    }

    /**
     * Constructs new Role without parameters.
     */
    public Role() {
    }

    /**
     * Sets role name.
     */
    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    /**
     * Returns role name.
     */
    public String getRoleName() {
        return roleName;
    }

    /**
     * Sets role id.
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * Returns role id.
     */
    public Long getId() {
        return id;
    }

    /**
     * @see Object#equals(Object)
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Role)) return false;

        Role role = (Role) o;

        return roleName.equals(role.roleName);

    }

    /**
     * @see Object#hashCode()
     */
    @Override
    public int hashCode() {
        return roleName.hashCode();
    }
}
