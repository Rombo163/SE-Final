package com.egar.auction.server.domain;


import javax.persistence.*;
import java.io.Serializable;

/**
 * User model for auction.
 *
 * @author Lisitsyn R.
 */
@Entity
@Table(name = "auction_user")
public class User implements Serializable {
    @Id
    private Long id;
    @Column(name = "name")
    private String userName;
    @Column(name = "password")
    private String password;
    @ManyToOne
    @JoinColumn(name = "role")
    private Role role;
    @Column(name = "latitude")
    private double latitude;
    @Column(name = "longitude")
    private double longitude;

    /**
     * Constructs new User without parameters.
     */
    public User() {

    }

    /**
     * Constructs new User with specified userName, password, Role, latitude, longitude.
     */
    public User(String userName, String password, Role role, double latitude, double longitude) {
        this.userName = userName;
        this.password = password;
        this.role = role;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    /**
     * Returns user id.
     */
    public Long getId() {
        return id;
    }

    /**
     * Sets user id.
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * Returns user userName.
     */
    public String getUserName() {
        return userName;
    }

    /**
     * Sets user name.
     */
    public void setUserName(String userName) {
        this.userName = userName;
    }

    /**
     * Returns user password.
     */
    public String getPassword() {
        return password;
    }

    /**
     * Sets user password.
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * Returns user Role.
     */
    public Role getRole() {
        return role;
    }

    /**
     * Sets user Role.
     */
    public void setRole(Role role) {
        this.role = role;
    }

    /**
     * Returns user latitude.
     */
    public double getLatitude() {
        return latitude;
    }

    /**
     * Sets user latitude.
     */
    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    /**
     * Returns user longitude.
     */
    public double getLongitude() {
        return longitude;
    }

    /**
     * Sets user longitude.
     */
    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    /**
     * @see Object#equals(Object)
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof User)) return false;

        User user = (User) o;

        if (id != user.id) return false;
        return userName != null ? userName.equals(user.userName) : user.userName == null;
    }

    /**
     * @see Object#hashCode()
     */
    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (userName != null ? userName.hashCode() : 0);
        return result;
    }
}
