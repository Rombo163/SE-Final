package com.egar.auction.server.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * Category model for auction.
 *
 * @author Lisitsyn Roman
 */
@Entity
@Table(name = "category")
public class Category implements Serializable {
    @Id
    private long id;
    @Column(name = "name")
    private String categoryName;

    /**
     * Constructs new Category without parametres.
     */
    public Category() {
    }

    /**
     * Constructs Category with specified category name.
     *
     * @param categoryName Name of the Category.
     */
    public Category(long id, String categoryName) {
        this.id = id;
        this.categoryName = categoryName;
    }

    /**
     * Returns name of the  Category.
     *
     * @return Name of the Category.
     */
    public String getCategoryName() {
        return categoryName;
    }

    /**
     * Returns category id.
     */
    public Long getId() {
        return id;
    }

    /**
     * Sets category id.
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * Sets Category name.
     */
    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

}
