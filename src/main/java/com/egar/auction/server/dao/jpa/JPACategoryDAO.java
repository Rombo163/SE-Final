package com.egar.auction.server.dao.jpa;

import com.egar.auction.server.dao.CategoryDao;
import com.egar.auction.server.dao.PersistException;
import com.egar.auction.server.domain.Category;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import java.util.List;

/**
 * JPA Category DAO.
 *
 * @author Lisitsyn.
 */
public class JPACategoryDAO implements CategoryDao {

    private EntityManagerFactory emf;

    /**
     * Constructs new JPACategoryDAO with specified EntityManager.
     *
     * @param emf EntityManagerFactory to set.
     */
    public JPACategoryDAO(EntityManagerFactory emf) {
        this.emf = emf;
    }

    /**
     * @see CategoryDao#persist(Category)
     */
    @Override
    public Category persist(Category category) throws PersistException {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        em.persist(category);
        em.getTransaction().commit();
        em.close();
        return category;
    }

    /**
     * @see CategoryDao#update(Category)
     */
    @Override
    public void update(Category category) throws PersistException {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        em.merge(category);
        em.getTransaction().commit();
        em.close();
    }

    /**
     * @see CategoryDao#delete(Category)
     */
    @Override
    public void delete(Category category) throws PersistException {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        Category mergedCategory = em.merge(category);
        em.remove(mergedCategory);
        em.getTransaction().commit();
        em.close();
    }

    /**
     * @see CategoryDao#getById(long)
     */
    @Override
    public Category getById(long pk) throws PersistException {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        Category category = em.find(Category.class, pk);
        em.getTransaction().commit();
        em.close();
        return category;
    }

    /**
     * @see CategoryDao#getByCategoryName(String)
     */
    @Override
    public Category getByCategoryName(String name) throws PersistException {
        EntityManager em = emf.createEntityManager();
        TypedQuery<Category> q = em.createQuery("SELECT category FROM Category category WHERE category.categoryName = :name", Category.class);
        q.setParameter("name", name);
        Category result = q.getSingleResult();
        em.close();
        return result;
    }

    /**
     * @see CategoryDao#getWithLimitOffset(long, long)
     */
    @Override
    public List<Category> getWithLimitOffset(long limit, long offset) throws PersistException {
        EntityManager em = emf.createEntityManager();
        TypedQuery<Category> q = em.createQuery("SELECT category FROM Category category", Category.class);
        q.setFirstResult((int) offset);
        q.setMaxResults((int) limit);
        List<Category> result = q.getResultList();
        em.close();
        return result;
    }

    /**
     *@see CategoryDao#countCategories()
     */
    @Override
    public Long countCategories() {
        EntityManager em = emf.createEntityManager();
        Query q = em.createQuery("SELECT COUNT(c) FROM Category c");
        Long result = (Long) q.getSingleResult();
        em.close();
        return result;
    }

    /**
     *@see CategoryDao#countLotsInCategory(Category)
     */
    @Override
    public Long countLotsInCategory(Category category) {
        EntityManager em = emf.createEntityManager();
        Query q = em.createQuery("SELECT COUNT(l) FROM Lot l WHERE l.category = :category");
        q.setParameter("category", category);
        Long result = (Long) q.getSingleResult();
        em.close();
        return result;
    }
}
