package com.egar.auction.server.dao.jpa;

import com.egar.auction.server.dao.*;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 * JPA DAO Factory.
 *
 * @author Lisitsyn R.
 */
public class JPADAOFactory extends DAOFactory {

    private static final String AUC_PU_NAME = "auction";
    private EntityManagerFactory factory;

    /**
     * Default.
     */
    public JPADAOFactory() {
        factory = Persistence.createEntityManagerFactory(AUC_PU_NAME);
    }

    /**
     * @see DAOFactory#close()
     */
    @Override
    public void close() {
        this.factory.close();
    }

    /**
     * @see UserDao
     */
    @Override
    public UserDao getUserDAO() {
        return new JPAUserDAO(factory);
    }

    /**
     * @see BidDao
     */
    @Override
    public BidDao getBidDAO() {
        return new JPABidDAO(factory);
    }

    /**
     * @see CategoryDao
     */
    @Override
    public CategoryDao getCategoryDAO() {
        return new JPACategoryDAO(factory);
    }

    /**
     * @see DeliveryServiceDao
     */
    @Override
    public DeliveryServiceDao getDeliveryServiceDAO() {
        return new JPADeliveryServiceDAO(factory);
    }

    /**
     * @see LotDao
     */
    @Override
    public LotDao getLotDAO() {
        return new JPALotDAO(factory);
    }

    /**
     * @see RoleDao
     */
    @Override
    public RoleDao getRoleDAO() {
        return new JPARoleDAO(factory);
    }
}
