package com.egar.auction.server.dao.postgres;

import com.egar.auction.server.dao.CategoryDao;
import com.egar.auction.server.dao.PersistException;
import com.egar.auction.server.domain.Category;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Category DAO implementation for Postgres SQL.
 *
 * @author Lisitsyn R.
 */
public class PostgresCategoryDao implements CategoryDao {
    private final DataSource dataSource;

    /**
     * Constructs PostgresCategoryDao with specified DataSource.
     *
     * @param dataSource specified DataSource.
     */
    PostgresCategoryDao(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    /**
     * @see CategoryDao#persist(Category)
     */
    @Override
    public Category persist(Category category) throws PersistException {
        String sql = "INSERT INTO category (name) VALUES (?);";
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setString(1, category.getCategoryName());
            int count = statement.executeUpdate();
            if (count != 1)
                throw new PersistException("On persist modify more then 1 record: " + count);
        } catch (SQLException e) {
            throw new PersistException(e);
        }
        sql = "SELECT CURRVAL(pg_get_serial_sequence('category','id')) AS last_insert_id;";
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement statement = connection.prepareStatement(sql);
            ResultSet resultSet = statement.executeQuery();
            resultSet.next();
            category.setId(resultSet.getLong(1));
            return category;
        } catch (SQLException e) {
            throw new PersistException(e);
        }
    }

    /**
     * @see CategoryDao#update(Category)
     */
    @Override
    public void update(Category category) throws PersistException {
        String sql = "UPDATE category SET name = ? WHERE id = ?;";
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setString(1, category.getCategoryName());
            statement.setLong(2, category.getId());
            int count = statement.executeUpdate();
            if (count != 1)
                throw new PersistException("On update modify more then 1 record: " + count);
        } catch (SQLException e) {
            throw new PersistException(e);
        }

    }

    /**
     * @see CategoryDao#delete(Category)
     */
    @Override
    public void delete(Category category) throws PersistException {
        String sql = "DELETE FROM category WHERE id = ?;";
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setLong(1, category.getId());
            int count = statement.executeUpdate();
            if (count != 1)
                throw new PersistException("On delete modify more then 1 record: " + count);
        } catch (SQLException e) {
            throw new PersistException(e);
        }
    }

    /**
     * @see CategoryDao#getById(long)
     */
    @Override
    public Category getById(long pk) throws PersistException {
        String sql = "SELECT * FROM category WHERE id = ?;";
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setLong(1, pk);
            ResultSet resultSet = statement.executeQuery();
            if (!resultSet.next())
                return null;
            else {
                //resultSet.next();
                Category category = new Category();
                category.setId(pk);
                category.setCategoryName(resultSet.getString("name"));
                return category;
            }

        } catch (SQLException e) {
            throw new PersistException(e);
        }
    }

    /**
     * @see CategoryDao#getByCategoryName(String)
     */
    @Override
    public Category getByCategoryName(String name) throws PersistException {
        String sql = "SELECT * FROM category WHERE name = ?;";
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, name);
            ResultSet rs = stm.executeQuery();
            if (!rs.next())
                return null;
            else {
                //rs.next();
                Category category = new Category();
                category.setId(rs.getLong("id"));
                category.setCategoryName(rs.getString("name"));
                return category;
            }
        } catch (SQLException e) {
            throw new PersistException(e);
        }
    }

    /**
     * @see CategoryDao#getWithLimitOffset(long, long)
     */
    @Override
    public List<Category> getWithLimitOffset(long limit, long offset) throws PersistException {
        String sql = "SELECT * FROM category ORDER BY id LIMIT ? OFFSET ?;";
        List<Category> categories = new ArrayList<>();
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setLong(1, limit);
            statement.setLong(2, offset);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Category category = new Category();
                category.setCategoryName(resultSet.getString("name"));
                category.setId(resultSet.getLong("id"));
                categories.add(category);
            }
        } catch (SQLException e) {
            throw new PersistException(e);
        }
        return categories;
    }

    /**
     * @see CategoryDao#countCategories()
     */
    @Override
    public Long countCategories() {
        String sql = "SELECT COUNT(id)FROM category;";
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stm = connection.prepareStatement(sql);
            ResultSet rs = stm.executeQuery();
            rs.next();
            return rs.getLong("count");
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * @see CategoryDao#countLotsInCategory(Category)
     */
    @Override
    public Long countLotsInCategory(Category category) {
        String sql = "SELECT COUNT(id)FROM lot WHERE category = ?;";
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setLong(1, category.getId());
            ResultSet rs = stm.executeQuery();
            rs.next();
            return rs.getLong("count");
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
}
