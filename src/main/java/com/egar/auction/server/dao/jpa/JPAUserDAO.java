package com.egar.auction.server.dao.jpa;

import com.egar.auction.server.OrderKeysForUser;
import com.egar.auction.server.dao.PersistException;
import com.egar.auction.server.dao.UserDao;
import com.egar.auction.server.domain.User;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import java.util.List;

/**
 * JPA User DAO.
 *
 * @author Lisitsyn.
 */
public class JPAUserDAO implements UserDao {

    private EntityManagerFactory emf;

    /**
     * Constructs new JPAUserDAO with specified EntityManager.
     *
     * @param emf EntityManager to set.
     */
    public JPAUserDAO(EntityManagerFactory emf) {
        this.emf = emf;
    }

    /**
     * @see UserDao#persist(User)
     */
    @Override
    public User persist(User user) throws PersistException {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        em.persist(user);
        em.getTransaction().commit();
        em.close();
        return user;
    }

    /**
     * @see UserDao#getById(long)
     */
    @Override
    public User getById(long pk) throws PersistException {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        User user = em.find(User.class, pk);
        em.getTransaction().commit();
        em.close();
        return user;
    }

    /**
     * @see UserDao#update(User)
     */
    @Override
    public void update(User user) throws PersistException {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        em.merge(user);
        em.getTransaction().commit();
        em.close();
    }

    /**
     * @see UserDao#delete(User)
     */
    @Override
    public void delete(User user) throws PersistException {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        User mergedUser = em.merge(user);
        em.remove(mergedUser);
        em.getTransaction().commit();
        em.close();
    }

    /**
     * @see UserDao#getByUserName(String, long, long)
     */
    @Override
    public List<User> getByUserName(String name, long limit, long offset) throws PersistException {
        EntityManager em = emf.createEntityManager();
        TypedQuery<User> q = em.createQuery("SELECT user FROM User user WHERE user.userName LIKE :name", User.class);
        q.setParameter("name", "%" + name + "%");
        q.setMaxResults((int) limit);
        q.setFirstResult((int) offset);
        List<User> result = q.getResultList();
        em.close();
        return result;
    }

    /**
     * @see UserDao#getUsers(long, long)
     */
    @Override
    public List<User> getUsers(long limit, long offset) throws PersistException {
        EntityManager em = emf.createEntityManager();
        TypedQuery<User> q = em.createQuery("SELECT user FROM User user", User.class);
        q.setFirstResult((int) offset);
        q.setMaxResults((int) limit);
        List<User> result = q.getResultList();
        em.close();
        return result;
    }

    /**
     * @see UserDao#countLotsForUser(User)
     */
    @Override
    public Long countLotsForUser(User user) {
        EntityManager em = emf.createEntityManager();
        Query q = em.createQuery("SELECT COUNT(l) FROM Lot l WHERE l.owner = :user");
        q.setParameter("user", user);
        Long result = (Long) q.getSingleResult();
        em.close();
        return result;
    }

    /**
     * @see UserDao#countBidsForUser(User)
     */
    @Override
    public Long countBidsForUser(User user) {
        EntityManager em = emf.createEntityManager();
        Query q = em.createQuery("SELECT COUNT(b) FROM Bid b WHERE b.owner = :user");
        q.setParameter("user", user);
        Long result = (Long) q.getSingleResult();
        em.close();
        return result;
    }

    /**
     *@see UserDao#countUsers()
     */
    @Override
    public Long countUsers() {
        EntityManager em = emf.createEntityManager();
        Query q = em.createQuery("SELECT COUNT(u) FROM User u");
        Long result = (Long) q.getSingleResult();
        em.close();
        return result;
    }

    /**
     * @see UserDao#getByCredentials(String, String)
     */
    @Override
    public User getByCredentials(String userName, String password) throws PersistException {
        EntityManager em = emf.createEntityManager();
        TypedQuery q = em.createQuery("SELECT user FROM User user WHERE userName = :name AND password = :password", User.class);
        q.setParameter("name", userName);
        q.setParameter("password", password);
        User result = (User) q.getSingleResult();
        em.close();
        return result;
    }

    /**
     * @see UserDao#getUsersSortedByKey(OrderKeysForUser, long, long)
     */
    @Override
    public List<User> getUsersSortedByKey(OrderKeysForUser key, long limit, long offset) throws PersistException {
        EntityManager em = emf.createEntityManager();
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<User> userCriteriaQuery = cb.createQuery(User.class);
        Root<User> userRoot = userCriteriaQuery.from(User.class);
        userCriteriaQuery.select(userRoot);
        switch (key) {
            case ID:
                userCriteriaQuery.orderBy(cb.asc(userRoot.get("id")));
                break;
            case NAME:
                userCriteriaQuery.orderBy(cb.asc(userRoot.get("userName")));
                break;
            case ROLE:
                userCriteriaQuery.orderBy(cb.asc(userRoot.get("role")));
                break;
            default:
                em.close();
                return null;
        }
        List<User> result = em.createQuery(userCriteriaQuery).setFirstResult((int) offset).setMaxResults((int) limit).getResultList();
        em.close();
        return result;
    }

    /**
     * @see UserDao#countUsersWithSpecifiedName(String)
     */
    @Override
    public Long countUsersWithSpecifiedName(String name) {
        EntityManager em = emf.createEntityManager();
        Query q = em.createQuery("SELECT COUNT(u) FROM User u WHERE u.name LIKE := name");
        q.setParameter("name", name);
        Long result = (Long) q.getSingleResult();
        em.close();
        return result;
    }
}
