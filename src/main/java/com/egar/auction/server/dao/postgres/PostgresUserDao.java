package com.egar.auction.server.dao.postgres;

import com.egar.auction.server.OrderKeysForUser;
import com.egar.auction.server.dao.PersistException;
import com.egar.auction.server.dao.UserDao;
import com.egar.auction.server.domain.Role;
import com.egar.auction.server.domain.User;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * User DAO implementation for Postgres SQL.
 *
 * @author Lisitsyn R.
 */
public class PostgresUserDao implements UserDao {
    private final DataSource dataSource;

    /**
     * Constructs new PostgresUserDao with specified DataSource.
     *
     * @param dataSource specified DataSource.
     */
    public PostgresUserDao(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    /**
     * @see UserDao#persist(User)
     */
    @Override
    public User persist(User user) throws PersistException {
        String sql = "INSERT INTO auction_user (name,password,role,latitude,longitude) VALUES (?,?,?,?,?);";
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setString(1, user.getUserName());
            statement.setString(2, user.getPassword());
            statement.setDouble(4, user.getLatitude());
            statement.setDouble(5, user.getLongitude());
            if (user.getRole() == null)
                statement.setLong(3, 0);
            else if (user.getRole().getId() == null)
                throw new PersistException("Can not persist user, role of the user has id = null.");
            else
                statement.setLong(3, user.getRole().getId());
            int count = statement.executeUpdate();
            if (count != 1)
                throw new PersistException("On persist modify more then 1 record:" + count);
        } catch (SQLException e) {
            throw new PersistException(e);
        }
        sql = "SELECT CURRVAL(pg_get_serial_sequence('auction_user','id')) AS last_insert_id;";
        //sql = "SELECT LASTVAL();";
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement statement = connection.prepareStatement(sql);
            ResultSet resultSet = statement.executeQuery();
            resultSet.next();
            user.setId(resultSet.getLong(1));
            return user;
        } catch (SQLException e) {
            throw new PersistException(e);
        }
    }

    /**
     * @see UserDao#getById(long)
     */
    @Override
    public User getById(long pk) throws PersistException {
        User user = new User();
        String sql = "SELECT * \n" +
                "FROM auction_user as au \n" +
                "LEFT JOIN (SELECT id AS role_id, name AS role_name FROM role) AS foo ON au.role = foo.role_id \n" +
                "WHERE au.id = ?;";
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setLong(1, pk);
            ResultSet resultSet = statement.executeQuery();
            if (!resultSet.next())
                return null;
            else {
                parseResultSet(resultSet, user);
                return user;
            }
        } catch (SQLException e) {
            throw new PersistException(e);
        }
    }

    /**
     * @see UserDao#getByUserName(String, long, long)
     */
    @Override
    public List<User> getByUserName(String name, long limit, long offset) throws PersistException {
        List<User> users = new ArrayList<>();
        String sql = "SELECT * \n" +
                "FROM auction_user as au \n" +
                "LEFT JOIN (SELECT id AS role_id, name AS role_name FROM role) AS foo ON au.role = foo.role_id \n" +
                "WHERE au.name LIKE ? ORDER BY id LIMIT ? OFFSET ?;";
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setString(1, "%" + name + "%");
            statement.setLong(2, limit);
            statement.setLong(3, offset);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                User user = new User();
                parseResultSet(resultSet, user);
                users.add(user);
            }
            return users;
        } catch (SQLException e) {
            throw new PersistException(e);
        }
    }

    /**
     * @see UserDao#update(User)
     */
    @Override
    public void update(User user) throws PersistException {
        String sql = "UPDATE auction_user SET name = ?, password = ?, role = ?, latitude = ?, longitude = ? WHERE id = ?; ";
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setString(1, user.getUserName());
            statement.setString(2, user.getPassword());
            statement.setDouble(4, user.getLatitude());
            statement.setDouble(5, user.getLongitude());
            if (user.getRole() == null)
                statement.setLong(3, user.getRole().getId());
            else if (user.getRole().getId() == null)
                throw new PersistException("Can not update user, role of the user has id = null.");
            else
                statement.setLong(3, user.getRole().getId());
            statement.setLong(6, user.getId());
            int count = statement.executeUpdate();
            if (count != 1)
                throw new PersistException("On update modify more then 1 record: " + count);
        } catch (SQLException e) {
            throw new PersistException(e);
        }

    }

    /**
     * @see UserDao#delete(User)
     */
    @Override
    public void delete(User user) throws PersistException {
        String sql = "DELETE FROM auction_user WHERE id =?;";
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setLong(1, user.getId());
            int count = statement.executeUpdate();
            if (count != 1)
                throw new PersistException("On delete modify more then 1 record: " + count);
        } catch (SQLException e) {
            throw new PersistException(e);
        }
    }

    /**
     * @see UserDao#getUsers(long, long)
     */
    @Override
    public List<User> getUsers(long limit, long offset) throws PersistException {
        String sql = "SELECT * \n" +
                "FROM auction_user as au \n" +
                "LEFT JOIN (SELECT id AS role_id, name AS role_name FROM role) AS foo ON au.role = foo.role_id \n" +
                "ORDER BY id \n" +
                "LIMIT ? \n" +
                "OFFSET ?;";
        List<User> users = new ArrayList<>();
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setLong(1, limit);
            statement.setLong(2, offset);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                User user = new User();
                parseResultSet(resultSet, user);
                users.add(user);
            }
        } catch (SQLException e) {
            throw new PersistException(e);
        }
        return users;
    }

    /**
     * @see UserDao#countLotsForUser(User)
     */
    @Override
    public Long countLotsForUser(User user) {
        String sql = "SELECT COUNT(id)FROM(SELECT * FROM lot WHERE lot_owner = ?)AS l;";
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setLong(1, user.getId());
            ResultSet rs = stm.executeQuery();
            rs.next();
            return rs.getLong("count");
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * @see UserDao#countBidsForUser(User)
     */
    @Override
    public Long countBidsForUser(User user) {
        String sql = "SELECT COUNT(id)FROM(SELECT * FROM bid WHERE bid_owner = ?)AS b;";
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setLong(1, user.getId());
            ResultSet rs = stm.executeQuery();
            rs.next();
            return rs.getLong("count");
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * @see UserDao#countUsers()
     */
    @Override
    public Long countUsers() {
        String sql = "SELECT COUNT(id)FROM auction_user;";
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stm = connection.prepareStatement(sql);
            ResultSet rs = stm.executeQuery();
            rs.next();
            return rs.getLong("count");
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * @see UserDao#countUsersWithSpecifiedName(String)
     */
    @Override
    public Long countUsersWithSpecifiedName(String name) {
        String sql = "SELECT COUNT(id)FROM auction_user WHERE name = ?;";
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, name);
            ResultSet rs = stm.executeQuery();
            rs.next();
            return rs.getLong("count");
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * @see UserDao#getByCredentials(String, String)
     */
    @Override
    public User getByCredentials(String userName, String password) throws PersistException {
        String sql = "SELECT * \n" +
                "FROM auction_user as au \n" +
                "LEFT JOIN (SELECT id AS role_id, name AS role_name FROM role) AS foo ON au.role = foo.role_id \n" +
                "WHERE au.name = ? AND au.password = ?;";
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, userName);
            stm.setString(2, password);
            ResultSet rs = stm.executeQuery();
            if (!rs.next())
                return null;
            else {
                User user = new User();
                parseResultSet(rs, user);
                return user;
            }
        } catch (SQLException e) {
            throw new PersistException(e);
        }
    }

    /**
     * @see UserDao#getUsersSortedByKey(OrderKeysForUser, long, long)
     */
    @Override
    public List<User> getUsersSortedByKey(OrderKeysForUser key, long limit, long offset) throws PersistException {
        String sqlOrderByColumnName;
        switch (key) {
            case ID:
                sqlOrderByColumnName = "id";
                break;
            case NAME:
                sqlOrderByColumnName = "name";
                break;
            case ROLE:
                sqlOrderByColumnName = "role";
                break;
            default:
                return null;
        }
        String sql = "SELECT * \n" +
                "FROM auction_user as au \n" +
                "LEFT JOIN (SELECT id AS role_id, name AS role_name FROM role) AS foo ON au.role = foo.role_id \n" +
                "ORDER BY " + sqlOrderByColumnName +
                " LIMIT ? \n" +
                "OFFSET ?;";
        List<User> users = new ArrayList<>();
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setLong(1, limit);
            statement.setLong(2, offset);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                User user = new User();
                parseResultSet(resultSet, user);
                users.add(user);
            }
        } catch (SQLException e) {
            throw new PersistException(e);
        }
        return users;
    }

    private void parseResultSet(ResultSet rs, User user) throws SQLException, PersistException {
        user.setId(rs.getLong("id"));
        user.setUserName(rs.getString("name"));
        user.setPassword(rs.getString("password"));
        user.setLatitude(rs.getDouble("latitude"));
        user.setLongitude(rs.getDouble("longitude"));
        if (rs.getLong("role") == 0)
            user.setRole(null);
        else {
            Role role = new Role();
            user.setRole(role);
            role.setId(rs.getLong("role_id"));
            role.setRoleName(rs.getString("role_name"));
        }
    }
}
