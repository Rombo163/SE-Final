package com.egar.auction.server.dao;


import com.egar.auction.server.domain.Category;

import java.util.List;

/**
 * Category DAO interface for Category entity.
 *
 * @author Lisitsyn R.
 */
public interface CategoryDao {

    /**
     * Persists specified category in persistent data storage.
     *
     * @param category specified category.
     * @return category with set id.
     * @throws PersistException
     */
    Category persist(Category category) throws PersistException;

    /**
     * Updates specified category.
     *
     * @param category specified category.
     * @throws PersistException
     */
    void update(Category category) throws PersistException;

    /**
     * Deletes specified category.
     *
     * @param category specified category.
     * @throws PersistException
     */
    void delete(Category category) throws PersistException;

    /**
     * Returns Category by specified id.
     *
     * @param pk id.
     * @return Category by specified id.
     * @throws PersistException
     */
    Category getById(long pk) throws PersistException;

    /**
     * Returns Category by specified name.
     *
     * @param name name of the category.
     * @return category with specified name.
     * @throws PersistException
     */
    Category getByCategoryName(String name) throws PersistException;

    /**
     * returns List of the categories.
     *
     * @param limit  limit for sql query.
     * @param offset offset for sql query.
     * @return list of the categories.
     * @throws PersistException
     */
    List<Category> getWithLimitOffset(long limit, long offset) throws PersistException;

    /**
     * Returns number of the Categories from persistent data storage.
     *
     * @return number of the Categories from persistent data storage.
     */
    Long countCategories();

    /**
     * Returns number of the lots with specified Category from persistent data storage.
     *
     * @return number of the lots with specified Category from persistent data storage.
     */
    Long countLotsInCategory(Category category);
}
