package com.egar.auction.server.dao;

import com.egar.auction.server.dao.postgres.PostgresDAOFactory;

/**
 * DAO Factory singleton object.
 *
 * @author Lisitsyn.
 */
public abstract class DAOFactory {

    private static DAOFactory instance;

    protected DAOFactory() {
    }

    /**
     * Returns instance of JDBCDAOFactory.
     *
     * @return JDBCDAOFactory.
     */
    public static DAOFactory getInstance() {
        if (instance == null)
            instance = new PostgresDAOFactory();
            //instance = new JPADAOFactory();
        return instance;
    }

    /**
     * Closes factory resources.
     */
    public void close() {

    }

    /**
     * Clone singleton fix.
     *
     * @throws CloneNotSupportedException
     */
    @Override
    protected final Object clone() throws CloneNotSupportedException {
        throw new CloneNotSupportedException();
    }

    /**
     * Returns user DAO.
     *
     * @return {@link UserDao}
     */
    public abstract UserDao getUserDAO();

    /**
     * Returns bid DAO.
     *
     * @return {@link BidDao}
     */
    public abstract BidDao getBidDAO();

    /**
     * Returns category DAO.
     *
     * @return {@link CategoryDao}
     */
    public abstract CategoryDao getCategoryDAO();

    /**
     * Returns delivery service DAO.
     *
     * @return {@link DeliveryServiceDao}
     */
    public abstract DeliveryServiceDao getDeliveryServiceDAO();

    /**
     * Returns lot DAO.
     *
     * @return {@link LotDao}
     */
    public abstract LotDao getLotDAO();

    /**
     * Returns role DAO.
     *
     * @return {@link RoleDao}
     */
    public abstract RoleDao getRoleDAO();
}
