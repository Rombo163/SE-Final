package com.egar.auction.server.dao.postgres;

import com.egar.auction.server.OrderKeysForBid;
import com.egar.auction.server.dao.BidDao;
import com.egar.auction.server.dao.PersistException;
import com.egar.auction.server.domain.*;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Bid DAO implementation for PostgresSQL.
 *
 * @author Lisitsyn R.
 */
public class PostgresBidDao implements BidDao {
    private DataSource dataSource;

    /**
     * Constructs PostgresBidDao with specified DataSource.
     *
     * @param dataSource specified DataSource.
     */
    public PostgresBidDao(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    /**
     * @see BidDao#persist(Bid)
     */
    @Override
    public Bid persist(Bid bid) throws PersistException {
        String sql = "INSERT INTO bid (bid_owner,value,lot) VALUES (?,?,?);";
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement statement = connection.prepareStatement(sql);
            if (bid.getOwner() == null)
                statement.setLong(1, 0);
            else if (bid.getOwner().getId() == null)
                throw new PersistException("Can not persist bid, owner of the bid has id = null.");
            else
                statement.setLong(1, bid.getOwner().getId());
            statement.setInt(2, bid.getValueOfBid());
            if (bid.getLot() == null)
                statement.setLong(3, 0);
            else if (bid.getLot().getId() == null)
                throw new PersistException("Can not persist bid, lot of the bid has id = null.");
            else
                statement.setLong(3, bid.getLot().getId());
            int count = statement.executeUpdate();
            if (count != 1)
                throw new PersistException("On persist modify more then 1 record: " + count);
        } catch (SQLException e) {
            throw new PersistException(e);
        }
        sql = "SELECT CURRVAL(pg_get_serial_sequence('bid','id')) AS last_insert_id;";
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement statement = connection.prepareStatement(sql);
            ResultSet resultSet = statement.executeQuery();
            resultSet.next();
            bid.setId(resultSet.getLong(1));
            return bid;
        } catch (SQLException e) {
            throw new PersistException(e);
        }
    }

    /**
     * @see BidDao#getById(long)
     */
    @Override
    public Bid getById(long pk) throws PersistException {
        String sql =
                //@formatter:off
                "SELECT * \n" +
                        "FROM bid \n" +
                        "LEFT JOIN \n" +
                            "(SELECT id AS u_id,name AS u_name,password as u_password, role as u_role, \n" +
                            "latitude AS u_latitude,longitude AS u_longitude FROM auction_user)AS u \n" +
                            "ON bid.bid_owner = u.u_id \n" +
                        "LEFT JOIN \n" +
                            "(SELECT id AS u_role_id,name AS u_role_name FROM role)AS u_role \n" +
                            "ON u.u_role = u_role.u_role_id \n" +
                        "LEFT JOIN \n" +
                            "(SELECT id AS lot_id,price,category,description,lot_owner,quantity,height,width,depth, \n" +
                            "weight,delivery,buyer,lotsold FROM lot) AS lot \n" +
                            "ON bid.lot = lot.lot_id \n" +
                        "LEFT JOIN \n" +
                            "(SELECT id AS o_id, name AS o_name, password AS o_password, role AS o_role, \n" +
                            "latitude AS o_latitude,longitude AS o_longitude FROM auction_user) AS lot_owner \n" +
                            "ON lot.lot_owner = lot_owner.o_id \n" +
                        "LEFT JOIN \n" +
                            "(SELECT id as o_role_id,name as o_role_name FROM role)AS owner_role \n" +
                            "ON lot_owner.o_role = owner_role.o_role_id \n" +
                        "LEFT JOIN \n" +
                            "(SELECT id AS b_id, name AS b_name, password AS b_password, role AS b_role, \n" +
                            "latitude AS b_latitude,longitude AS b_longitude FROM auction_user) AS lot_buyer \n" +
                            "ON lot.buyer = lot_buyer.b_id \n" +
                        "LEFT JOIN \n" +
                            "(SELECT id as b_role_id,name as b_role_name FROM role)AS buyer_role \n" +
                            "ON lot_buyer.b_role = buyer_role.b_role_id \n" +
                        "LEFT JOIN \n" +
                            "(SELECT id as cat_id,name as cat_name FROM category)AS cat \n" +
                            "ON lot.category = cat.cat_id \n" +
                        "LEFT JOIN \n" +
                        "(SELECT id as d_id,name as d_name,w_price,v_price,d_price,min_weight,max_weight, \n" +
                            "min_volume,max_volume,min_distance,max_distance FROM delivery) as d \n" +
                            "ON lot.delivery = d.d_id \n" +
                        "WHERE bid.id = ?;";
        //@formatter:on
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setLong(1, pk);
            ResultSet resultSet = statement.executeQuery();
            if (!resultSet.next())
                return null;
            else {
                Bid bid = new Bid();
                parseResultSet(resultSet, bid);
                return bid;
            }
        } catch (SQLException e) {
            throw new PersistException(e);
        }
    }

    /**
     * @see BidDao#update(Bid)
     */
    @Override
    public void update(Bid bid) throws PersistException {
        String sql = "UPDATE bid SET bid_owner = ?,value = ?,lot = ? WHERE id = ?;";
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement statement = connection.prepareStatement(sql);
            if (bid.getOwner() == null)
                statement.setLong(1, 0);
            else if (bid.getOwner().getId() == null)
                throw new PersistException("Can not persist bid, owner of the bid has id = null.");
            else
                statement.setLong(1, bid.getOwner().getId());
            statement.setInt(2, bid.getValueOfBid());
            if (bid.getLot() == null)
                statement.setLong(3, 0);
            else if (bid.getLot().getId() == null)
                throw new PersistException("Can not persist bid, lot of the bid has id = null.");
            else
                statement.setLong(3, bid.getLot().getId());
            statement.setLong(4, bid.getId());
            int count = statement.executeUpdate();
            if (count != 1)
                throw new PersistException("On update modify more then 1 record: " + count);
        } catch (SQLException e) {
            throw new PersistException(e);
        }
    }

    /**
     * @see BidDao#delete(Bid)
     */
    @Override
    public void delete(Bid bid) throws PersistException {
        String sql = "DELETE FROM bid WHERE id = ?;";
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setLong(1, bid.getId());
            int count = statement.executeUpdate();
            if (count != 1)
                throw new PersistException("On delete modify more then 1 record: " + count);
        } catch (SQLException e) {
            throw new PersistException(e);
        }

    }

    /**
     * @see BidDao#getBidsByLotWithOrderByKey(OrderKeysForBid, Lot, long, long)
     */
    @Override
    public List<Bid> getBidsByLotWithOrderByKey(OrderKeysForBid key, Lot lot, long limit, long offset) throws PersistException {
        String orderColumnName;
        switch (key) {
            case ID:
                orderColumnName = "id";
                break;
            case OWNER:
                orderColumnName = "bid_owner";
                break;
            case LOT:
                orderColumnName = "lot";
                break;
            default:
                return null;
        }
        List<Bid> bids = new ArrayList<>();
        String sql =
                //@formatter:off
                "SELECT * \n" +
                        "FROM bid \n" +
                        "LEFT JOIN \n" +
                            "(SELECT id AS u_id,name AS u_name,password as u_password, role as u_role, \n" +
                            "latitude AS u_latitude,longitude AS u_longitude FROM auction_user)AS u \n" +
                            "ON bid.bid_owner = u.u_id \n" +
                        "LEFT JOIN \n" +
                            "(SELECT id AS u_role_id,name AS u_role_name FROM role)AS u_role \n" +
                            "ON u.u_role = u_role.u_role_id\n" +
                        "LEFT JOIN \n" +
                            "(SELECT id AS lot_id,price,category,description,lot_owner,quantity,height,width,depth, \n" +
                            "weight,delivery,buyer,lotsold FROM lot) AS lot \n" +
                            "ON bid.lot = lot.lot_id \n" +
                        "LEFT JOIN \n" +
                            "(SELECT id AS o_id, name AS o_name, password AS o_password, role AS o_role, \n" +
                            "latitude AS o_latitude,longitude AS o_longitude FROM auction_user) AS lot_owner \n" +
                            "ON lot.lot_owner = lot_owner.o_id \n" +
                        "LEFT JOIN \n" +
                            "(SELECT id as o_role_id,name as o_role_name FROM role)AS owner_role \n" +
                            "ON lot_owner.o_role = owner_role.o_role_id \n" +
                        "LEFT JOIN \n" +
                            "(SELECT id AS b_id, name AS b_name, password AS b_password, role AS b_role, \n" +
                            "latitude AS b_latitude,longitude AS b_longitude FROM auction_user) AS lot_buyer \n" +
                            "ON lot.buyer = lot_buyer.b_id \n" +
                        "LEFT JOIN \n" +
                            "(SELECT id as b_role_id,name as b_role_name FROM role)AS buyer_role \n" +
                            "ON lot_buyer.b_role = buyer_role.b_role_id \n" +
                        "LEFT JOIN \n" +
                            "(SELECT id as cat_id,name as cat_name FROM category)AS cat \n" +
                            "ON lot.category = cat.cat_id \n" +
                        "LEFT JOIN \n" +
                            "(SELECT id as d_id,name as d_name,w_price,v_price,d_price,min_weight,max_weight, \n" +
                            "min_volume,max_volume,min_distance,max_distance FROM delivery) as d \n" +
                            "ON lot.delivery = d.d_id \n" +
                        "WHERE bid.lot = ? \n" +
                        "ORDER BY "+orderColumnName +" \n" +
                        "LIMIT ? \n" +
                        "OFFSET ?;";
        //@formatter:on
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setLong(1, lot.getId());
            stm.setLong(2, limit);
            stm.setLong(3, offset);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                Bid bid = new Bid();
                parseResultSet(rs, bid);
                bids.add(bid);
            }
            return bids;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * @see BidDao#getBidsByOwnerWithOrderByKey(OrderKeysForBid, User, long, long)
     */
    @Override
    public List<Bid> getBidsByOwnerWithOrderByKey(OrderKeysForBid key, User user, long limit, long offset) throws PersistException {
        String orderColumnName;
        switch (key) {
            case ID:
                orderColumnName = "id";
                break;
            case OWNER:
                orderColumnName = "bid_owner";
                break;
            case LOT:
                orderColumnName = "lot";
                break;
            default:
                return null;
        }
        List<Bid> bids = new ArrayList<>();
        String sql =
                //@formatter:off
                "SELECT * \n" +
                        "FROM bid \n" +
                        "LEFT JOIN \n" +
                            "(SELECT id AS u_id,name AS u_name,password as u_password, role as u_role, \n" +
                            "latitude AS u_latitude,longitude AS u_longitude FROM auction_user)AS u \n" +
                            "ON bid.bid_owner = u.u_id \n" +
                        "LEFT JOIN \n" +
                            "(SELECT id AS u_role_id,name AS u_role_name FROM role)AS u_role \n" +
                            "ON u.u_role = u_role.u_role_id\n" +
                        "LEFT JOIN \n" +
                            "(SELECT id AS lot_id,price,category,description,lot_owner,quantity,height,width,depth, \n" +
                            "weight,delivery,buyer,lotsold FROM lot) AS lot \n" +
                            "ON bid.lot = lot.lot_id \n" +
                        "LEFT JOIN \n" +
                            "(SELECT id AS o_id, name AS o_name, password AS o_password, role AS o_role, \n" +
                            "latitude AS o_latitude,longitude AS o_longitude FROM auction_user) AS lot_owner \n" +
                            "ON lot.lot_owner = lot_owner.o_id \n" +
                        "LEFT JOIN \n" +
                            "(SELECT id as o_role_id,name as o_role_name FROM role)AS owner_role \n" +
                            "ON lot_owner.o_role = owner_role.o_role_id \n" +
                        "LEFT JOIN \n" +
                            "(SELECT id AS b_id, name AS b_name, password AS b_password, role AS b_role, \n" +
                            "latitude AS b_latitude,longitude AS b_longitude FROM auction_user) AS lot_buyer \n" +
                            "ON lot.buyer = lot_buyer.b_id \n" +
                        "LEFT JOIN \n" +
                            "(SELECT id as b_role_id,name as b_role_name FROM role)AS buyer_role \n" +
                            "ON lot_buyer.b_role = buyer_role.b_role_id \n" +
                        "LEFT JOIN \n" +
                            "(SELECT id as cat_id,name as cat_name FROM category)AS cat \n" +
                            "ON lot.category = cat.cat_id \n" +
                        "LEFT JOIN \n" +
                            "(SELECT id as d_id,name as d_name,w_price,v_price,d_price,min_weight,max_weight, \n" +
                            "min_volume,max_volume,min_distance,max_distance FROM delivery) as d \n" +
                            "ON lot.delivery = d.d_id \n" +
                        "WHERE bid.bid_owner = ? \n" +
                        "ORDER BY "+ orderColumnName+" \n" +
                        "LIMIT ? \n" +
                        "OFFSET ?;";
        //@formatter:on
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setLong(1, user.getId());
            stm.setLong(2, limit);
            stm.setLong(3, offset);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                Bid bid = new Bid();
                parseResultSet(rs, bid);
                bids.add(bid);
            }
            return bids;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * @see BidDao#countBidsWithSpecifiedOwner(User)
     */
    @Override
    public Long countBidsWithSpecifiedOwner(User owner) {
        String sql = "SELECT COUNT(id)FROM Bid WHERE bid_owner = ?;";
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setLong(1, owner.getId());
            ResultSet rs = stm.executeQuery();
            rs.next();
            return rs.getLong("count");
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * @see BidDao#countBidsWithSpecifiedLot(Lot)
     */
    @Override
    public Long countBidsWithSpecifiedLot(Lot lot) {
        String sql = "SELECT COUNT(id)FROM Bid WHERE lot = ?;";
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setLong(1, lot.getId());
            ResultSet rs = stm.executeQuery();
            rs.next();
            return rs.getLong("count");
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    private void parseResultSet(ResultSet rs, Bid bid) throws SQLException {
        bid.setId(rs.getLong("id"));
        bid.setValueOfBid(rs.getInt("value"));
        long entityId = rs.getLong("bid_owner");
        if (entityId == 0)
            bid.setOwner(null);
        else {
            User user = new User();
            bid.setOwner(user);
            parseUser(rs, user, "u");
        }
        entityId = rs.getLong("lot");
        if (entityId == 0)
            bid.setLot(null);
        else {
            Lot lot = new Lot();
            bid.setLot(lot);
            lot.setId(rs.getLong("lot_id"));
            lot.setDescription(rs.getString("description"));
            lot.setPrice(rs.getInt("price"));
            lot.setQuantity(rs.getInt("quantity"));
            lot.setWeight(rs.getFloat("weight"));
            lot.setWidth(rs.getFloat("width"));
            lot.setHeight(rs.getFloat("height"));
            lot.setDepth(rs.getFloat("depth"));
            lot.setLotSold(rs.getBoolean("lotsold"));
            entityId = rs.getLong("lot_owner");
            if (entityId == 0)
                lot.setOwner(null);
            else {
                User user = new User();
                lot.setOwner(user);
                parseUser(rs, user, "o");
            }
            entityId = rs.getLong("buyer");
            if (entityId == 0)
                lot.setBuyer(null);
            else {
                User user = new User();
                lot.setBuyer(user);
                parseUser(rs, user, "b");
            }
            entityId = rs.getLong("category");
            if (entityId == 0)
                lot.setCategory(null);
            else {
                Category category = new Category();
                lot.setCategory(category);
                category.setId(rs.getLong("cat_id"));
                category.setCategoryName(rs.getString("cat_name"));
            }
            entityId = rs.getLong("delivery");
            if (entityId == 0)
                lot.setDeliveryService(null);
            else {
                DeliveryService deliveryService = new DeliveryService();
                lot.setDeliveryService(deliveryService);
                deliveryService.setId(rs.getLong("d_id"));
                deliveryService.setTitle(rs.getString("d_name"));
                deliveryService.setDistancePrice(rs.getFloat("d_price"));
                deliveryService.setVolumePrice(rs.getFloat("v_price"));
                deliveryService.setWeightPrice(rs.getFloat("w_price"));
                deliveryService.setMaxDistance(rs.getDouble("max_distance"));
                deliveryService.setMaxVolume(rs.getDouble("max_volume"));
                deliveryService.setMaxWeight(rs.getLong("max_weight"));
                deliveryService.setMinDistance(rs.getLong("min_distance"));
                deliveryService.setMinVolume(rs.getLong("min_volume"));
                deliveryService.setMinWeight(rs.getLong("min_weight"));
            }
        }
    }

    private void parseUser(ResultSet rs, User user, String tableName) throws SQLException {
        user.setId(rs.getLong(tableName + "_id"));
        user.setUserName(rs.getString(tableName + "_name"));
        user.setPassword(rs.getString(tableName + "_password"));
        user.setLatitude(rs.getDouble(tableName + "_latitude"));
        user.setLongitude(rs.getDouble(tableName + "_longitude"));
        long userRoleId = rs.getLong(tableName + "_role");
        if (userRoleId == 0)
            user.setRole(null);
        else {
            Role role = new Role();
            user.setRole(role);
            role.setId(rs.getLong(tableName + "_role_id"));
            role.setRoleName(rs.getString(tableName + "_role_name"));
        }
    }
}
