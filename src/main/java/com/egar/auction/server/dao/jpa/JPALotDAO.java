package com.egar.auction.server.dao.jpa;

import com.egar.auction.server.OrderKeysForLot;
import com.egar.auction.server.dao.LotDao;
import com.egar.auction.server.dao.PersistException;
import com.egar.auction.server.domain.Category;
import com.egar.auction.server.domain.Lot;
import com.egar.auction.server.domain.User;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

/**
 * JPA Lot DAO.
 *
 * @author Lisitsyn.
 */
public class JPALotDAO implements LotDao {

    private EntityManagerFactory emf;

    /**
     * Constructs new JPALotDAO with specified EntityManager.
     *
     * @param emf EntityManagerFactory.
     */
    public JPALotDAO(EntityManagerFactory emf) {
        this.emf = emf;
    }

    /**
     * @see LotDao#persist(Lot)
     */
    @Override
    public Lot persist(Lot lot) throws PersistException {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        em.persist(lot);
        em.getTransaction().commit();
        em.close();
        return lot;
    }

    /**
     * @see LotDao#getById(long)
     */
    @Override
    public Lot getById(long pk) throws PersistException {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        Lot result = em.find(Lot.class, pk);
        em.getTransaction().commit();
        em.close();
        return result;
    }

    /**
     * @see LotDao#update(Lot)
     */
    @Override
    public void update(Lot lot) throws PersistException {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        em.merge(lot);
        em.getTransaction().commit();
        em.close();
    }

    /**
     * @see LotDao#delete(Lot)
     */
    @Override
    public void delete(Lot lot) throws PersistException {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        em.remove(em.merge(lot));
        em.getTransaction().commit();
        em.close();
    }

    /**
     * @see LotDao#getBySold(boolean, long, long, long)
     */
    @Override
    public List<Lot> getBySold(boolean lotSold, long userId, long limit, long offset) throws PersistException {
        EntityManager em = emf.createEntityManager();
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Lot> lotCriteriaQuery = cb.createQuery(Lot.class);
        Root<Lot> lotRoot = lotCriteriaQuery.from(Lot.class);
        lotCriteriaQuery.select(lotRoot);
        lotCriteriaQuery.where(cb.equal(lotRoot.get("lotSold"), lotSold));
        List<Lot> result = em.createQuery(lotCriteriaQuery).setFirstResult((int) offset).setMaxResults((int) limit).getResultList();
        em.close();
        return result;
    }

    /**
     * @see LotDao#getByCategory(Category, long, long)
     */
    @Override
    public List<Lot> getByCategory(Category category, long limit, long offset) throws PersistException {
        EntityManager em = emf.createEntityManager();
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Lot> lotCriteriaQuery = cb.createQuery(Lot.class);
        Root<Lot> lotRoot = lotCriteriaQuery.from(Lot.class);
        lotCriteriaQuery.select(lotRoot);
        lotCriteriaQuery.where(cb.equal(lotRoot.get("category"), category));
        List<Lot> result = em.createQuery(lotCriteriaQuery).setFirstResult((int) offset).setMaxResults((int) limit).getResultList();
        em.close();
        return result;
    }

    /**
     * @see LotDao#getByOwner(User, long, long)
     */
    @Override
    public List<Lot> getByOwner(User user, long limit, long offset) throws PersistException {
        EntityManager em = emf.createEntityManager();
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Lot> lotCriteriaQuery = cb.createQuery(Lot.class);
        Root<Lot> lotRoot = lotCriteriaQuery.from(Lot.class);
        lotCriteriaQuery.select(lotRoot);
        lotCriteriaQuery.where(cb.equal(lotRoot.get("owner"), user));
        List<Lot> result = em.createQuery(lotCriteriaQuery).setFirstResult((int) offset).setMaxResults((int) limit).getResultList();
        em.close();
        return result;
    }

    /**
     * @see LotDao#getByDescription(String, long, long)
     */
    @Override
    public List<Lot> getByDescription(String description, long limit, long offset) throws PersistException {
        EntityManager em = emf.createEntityManager();
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Lot> lotCriteriaQuery = cb.createQuery(Lot.class);
        Root<Lot> lotRoot = lotCriteriaQuery.from(Lot.class);
        lotCriteriaQuery.select(lotRoot);
        lotCriteriaQuery.where(cb.like(lotRoot.get("description"), "%" + description + "%"));
        List<Lot> result = em.createQuery(lotCriteriaQuery).setFirstResult((int) offset).setMaxResults((int) limit).getResultList();
        em.close();
        return result;
    }

    /**
     * @see LotDao#getSortedByKey(OrderKeysForLot, long, long)
     */
    @Override
    public List<Lot> getSortedByKey(OrderKeysForLot key, long limit, long offset) throws PersistException {
        EntityManager em = emf.createEntityManager();
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Lot> lotCriteriaQuery = cb.createQuery(Lot.class);
        Root<Lot> lotRoot = lotCriteriaQuery.from(Lot.class);
        lotCriteriaQuery.select(lotRoot);
        switch (key) {
            case ID:
                lotCriteriaQuery.orderBy(cb.asc(lotRoot.get("id")));
                break;
            case CATEGORY:
                lotCriteriaQuery.orderBy(cb.asc(lotRoot.get("category")));
                break;
            case DESCRIPTION:
                lotCriteriaQuery.orderBy(cb.asc(lotRoot.get("description")));
                break;
            case OWNER:
                lotCriteriaQuery.orderBy(cb.asc(lotRoot.get("owner")));
                break;
            default:
                return null;
        }
        List<Lot> result = em.createQuery(lotCriteriaQuery).setFirstResult((int) offset).setMaxResults((int) limit).getResultList();
        em.close();
        return result;
    }

    /**
     * @see LotDao#countLots()
     */
    @Override
    public Long countLots() {
        EntityManager em = emf.createEntityManager();
        Query q = em.createQuery("SELECT COUNT(l) FROM Lot l");
        Long result = (Long) q.getSingleResult();
        em.close();
        return result;
    }

    /**
     * @see LotDao#countLotsWithSpecifiedOwner(User)
     */
    @Override
    public Long countLotsWithSpecifiedOwner(User owner) {
        EntityManager em = emf.createEntityManager();
        Query q = em.createQuery("SELECT COUNT(l) FROM Lot l WHERE l.owner := owner");
        q.setParameter("owner", owner);
        Long result = (Long) q.getSingleResult();
        em.close();
        return result;
    }

    /**
     * @see LotDao#countLotsWithSpecifiedCategory(Category)
     */
    @Override
    public Long countLotsWithSpecifiedCategory(Category category) {
        EntityManager em = emf.createEntityManager();
        Query q = em.createQuery("SELECT COUNT(l) FROM Lot l WHERE l.category := category");
        q.setParameter("category", category);
        Long result = (Long) q.getSingleResult();
        em.close();
        return result;
    }

    /**
     * @see LotDao#countLotsWithSpecifiedDescription(String)
     */
    @Override
    public Long countLotsWithSpecifiedDescription(String description) {
        EntityManager em = emf.createEntityManager();
        Query q = em.createQuery("SELECT COUNT(l) FROM Lot l WHERE l.description LIKE := description");
        q.setParameter("description", description);
        Long result = (Long) q.getSingleResult();
        em.close();
        return result;
    }
}
