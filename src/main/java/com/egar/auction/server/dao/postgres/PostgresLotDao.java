package com.egar.auction.server.dao.postgres;

import com.egar.auction.server.OrderKeysForLot;
import com.egar.auction.server.dao.LotDao;
import com.egar.auction.server.dao.PersistException;
import com.egar.auction.server.domain.*;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * PostgresLotDao implementation.
 *
 * @author Lisitsyn R.
 */
public class PostgresLotDao implements LotDao {
    private DataSource dataSource;

    /**
     * Constructs new PostgresLotDao with specified dataSource.
     *
     * @param dataSource specified dataSource.
     */
    public PostgresLotDao(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    /**
     * @see LotDao#persist(Lot)
     */
    @Override
    public Lot persist(Lot lot) throws PersistException {
        String sql = "INSERT INTO lot (price, lot_owner, description, category, quantity, height, width, depth, weight, delivery, buyer, lotsold) VALUES (?,?,?,?,?,?,?,?,?,?,?,?);";
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setInt(1, lot.getPrice());
            if (lot.getOwner() == null)
                statement.setLong(2, 0);
            else if (lot.getOwner().getId() == null)
                throw new PersistException("Can not persist lot, owner of the lot has id = null.");
            else
                statement.setLong(2, lot.getOwner().getId());
            statement.setString(3, lot.getDescription());
            if (lot.getCategory() == null)
                statement.setLong(4, 0);
            else if (lot.getCategory().getId() == null)
                throw new PersistException("Can not persist lot, category of the lot has id = null.");
            else
                statement.setLong(4, lot.getCategory().getId());
            statement.setInt(5, lot.getQuantity());
            statement.setFloat(6, lot.getHeight());
            statement.setFloat(7, lot.getWidth());
            statement.setFloat(8, lot.getDepth());
            statement.setFloat(9, lot.getWeight());
            if (lot.getDeliveryService() == null)
                statement.setLong(10, 0);
            else if (lot.getDeliveryService().getId() == null)
                throw new PersistException("Can not update lot, deliveryService of the lot has id = null.");
            else
                statement.setLong(10, lot.getDeliveryService().getId());
            if (lot.getBuyer() == null)
                statement.setLong(11, 0);
            else if (lot.getBuyer().getId() == null)
                throw new PersistException("Can not update lot, buyer of the lot has id = null.");
            else
                statement.setLong(11, lot.getBuyer().getId());
            statement.setBoolean(12, lot.isLotSold());
            int count = statement.executeUpdate();
            if (count != 1)
                throw new PersistException("On persist modify more then 1 record: " + count);
        } catch (SQLException e) {
            throw new PersistException(e);
        }
        sql = "SELECT CURRVAL(pg_get_serial_sequence('lot','id')) AS last_insert_id;";
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement statement = connection.prepareStatement(sql);
            ResultSet resultSet = statement.executeQuery();
            resultSet.next();
            lot.setId(resultSet.getLong(1));
        } catch (SQLException e) {
            throw new PersistException(e);
        }
        return lot;
    }

    /**
     * @see LotDao#delete(Lot)
     */
    @Override
    public Lot getById(long pk) throws PersistException {
        String sql =
                //@formatter:off
                "SELECT * \n" +
                    "FROM lot \n" +
                    "LEFT JOIN \n" +
                        "(SELECT id AS o_id, name AS o_name, password AS o_password, role AS o_role, \n" +
                        "latitude AS o_latitude,longitude AS o_longitude FROM auction_user) AS lot_owner \n" +
                        "ON lot.lot_owner = lot_owner.o_id \n" +
                    "LEFT JOIN \n" +
                        "(SELECT id as o_role_id,name as o_role_name FROM role)AS owner_role \n" +
                        "ON lot_owner.o_role = owner_role.o_role_id \n" +
                    "LEFT JOIN \n" +
                        "(SELECT id AS b_id, name AS b_name, password AS b_password, role AS b_role, \n" +
                        "latitude AS b_latitude,longitude AS b_longitude FROM auction_user) AS lot_buyer \n" +
                        "ON lot.buyer = lot_buyer.b_id \n" +
                    "LEFT JOIN \n" +
                        "(SELECT id as b_role_id,name as b_role_name FROM role)AS buyer_role \n" +
                        "ON lot_buyer.b_role = buyer_role.b_role_id \n" +
                    "LEFT JOIN \n" +
                        "(SELECT id as cat_id,name as cat_name FROM category)AS cat \n" +
                        "ON lot.category = cat.cat_id \n" +
                    "LEFT JOIN \n" +
                        "(SELECT id as d_id,name as d_name,w_price,v_price,d_price,min_weight,max_weight, \n" +
                        "min_volume,max_volume,min_distance,max_distance FROM delivery) as d \n" +
                        "ON lot.delivery = d.d_id \n" +
                    "WHERE lot.id = ?;";
        //@formatter:on
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setLong(1, pk);
            ResultSet rs = statement.executeQuery();
            if (!rs.next())
                return null;
            else {
                //rs.next();
                Lot lot = new Lot();
                parseResultSet(rs, lot);
                return lot;
            }
        } catch (SQLException e) {
            throw new PersistException(e);
        }
    }

    /**
     * @see LotDao#update(Lot)
     */
    @Override
    public void update(Lot lot) throws PersistException {
        String sql = "UPDATE lot SET price = ?, lot_owner = ?, description = ?, category = ?, quantity = ?, height = ?," +
                " width = ?, depth = ?, weight = ?, delivery = ?, buyer = ?, lotsold = ? WHERE id = ?;";
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setInt(1, lot.getPrice());
            if (lot.getOwner() == null)
                statement.setLong(2, 0);
            else if (lot.getOwner().getId() == null)
                throw new PersistException("Can not update lot, owner of the lot has id = null.");
            else
                statement.setLong(2, lot.getOwner().getId());
            statement.setString(3, lot.getDescription());
            if (lot.getCategory() == null)
                statement.setLong(4, 0);
            else if (lot.getCategory().getId() == null)
                throw new PersistException("Can not update lot, category of the lot has id = null.");
            else
                statement.setLong(4, lot.getCategory().getId());
            statement.setInt(5, lot.getQuantity());
            statement.setFloat(6, lot.getHeight());
            statement.setFloat(7, lot.getWidth());
            statement.setFloat(8, lot.getDepth());
            statement.setFloat(9, lot.getWeight());
            if (lot.getDeliveryService() == null)
                statement.setLong(10, 0);
            else if (lot.getDeliveryService().getId() == null)
                throw new PersistException("Can not update lot, deliveryService of the lot has id = null.");
            else
                statement.setLong(10, lot.getDeliveryService().getId());
            if (lot.getBuyer() == null)
                statement.setLong(11, 0);
            else if (lot.getBuyer().getId() == null)
                throw new PersistException("Can not update lot, buyer of the lot has id = null.");
            else
                statement.setLong(11, lot.getBuyer().getId());
            statement.setBoolean(12, lot.isLotSold());
            statement.setLong(13, lot.getId());
            int count = statement.executeUpdate();
            if (count != 1)
                throw new PersistException("On update modify more then 1 record: " + count);
        } catch (SQLException e) {
            throw new PersistException(e);
        }
    }

    /**
     * @see LotDao#delete(Lot)
     */
    @Override
    public void delete(Lot lot) throws PersistException {
        String sql = "DELETE FROM lot WHERE id = ?;";
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setLong(1, lot.getId());
            int count = statement.executeUpdate();
            if (count != 1)
                throw new PersistException("On delete modify more then 1 record: " + count);
        } catch (SQLException e) {
            throw new PersistException(e);
        }
    }

    /**
     * @see LotDao#getBySold(boolean, long, long, long)
     */
    @Override
    public List<Lot> getBySold(boolean lotSold, long userId, long limit, long offset) throws PersistException {
        List<Lot> lots = new ArrayList<>();
        String sql =
                //@formatter:off
                "SELECT * \n" +
                    "FROM lot \n" +
                    "LEFT JOIN \n" +
                        "(SELECT id AS o_id, name AS o_name, password AS o_password, role AS o_role, \n" +
                        "latitude AS o_latitude,longitude AS o_longitude FROM auction_user) AS lot_owner \n" +
                        "ON lot.lot_owner = lot_owner.o_id \n" +
                    "LEFT JOIN \n" +
                        "(SELECT id as o_role_id,name as o_role_name FROM role)AS owner_role \n" +
                        "ON lot_owner.o_role = owner_role.o_role_id \n" +
                    "LEFT JOIN \n" +
                        "(SELECT id AS b_id, name AS b_name, password AS b_password, role AS b_role, \n" +
                        "latitude AS b_latitude,longitude AS b_longitude FROM auction_user) AS lot_buyer \n" +
                        "ON lot.buyer = lot_buyer.b_id \n" +
                    "LEFT JOIN \n" +
                        "(SELECT id as b_role_id,name as b_role_name FROM role)AS buyer_role \n" +
                        "ON lot_buyer.b_role = buyer_role.b_role_id \n" +
                    "LEFT JOIN \n" +
                        "(SELECT id as cat_id,name as cat_name FROM category)AS cat \n" +
                        "ON lot.category = cat.cat_id \n" +
                    "LEFT JOIN \n" +
                        "(SELECT id as d_id,name as d_name,w_price,v_price,d_price,min_weight,max_weight, \n" +
                        "min_volume,max_volume,min_distance,max_distance FROM delivery) as d \n" +
                        "ON lot.delivery = d.d_id \n" +
                    "WHERE lot.buyer = ? AND lot.lotsold = ? \n" +
                    "ORDER BY id \n" +
                    "LIMIT ? \n" +
                    "OFFSET ?;";
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setLong(1,userId);
            statement.setBoolean(2, lotSold);
            statement.setLong(3, limit);
            statement.setLong(4, offset);
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                Lot lot = new Lot();
                parseResultSet(rs, lot);
                lots.add(lot);
            }
            return lots;
        } catch (SQLException e) {
            throw new PersistException(e);
        }
    }

    /**
     *@see LotDao#getByCategory(Category, long, long)
    */
    public List<Lot> getByCategory(Category category, long limit, long offset) throws PersistException {
        List<Lot> lots = new ArrayList<>();
        String sql =
                //@formatter:off
                "SELECT *\n" +
                        "FROM lot \n" +
                        "LEFT JOIN \n" +
                            "(SELECT id AS o_id, name AS o_name, password AS o_password, role AS o_role, \n" +
                            "latitude AS o_latitude,longitude AS o_longitude FROM auction_user) AS lot_owner \n" +
                            "ON lot.lot_owner = lot_owner.o_id \n" +
                        "LEFT JOIN \n" +
                            "(SELECT id as o_role_id,name as o_role_name FROM role)AS owner_role \n" +
                            "ON lot_owner.o_role = owner_role.o_role_id \n" +
                        "LEFT JOIN \n" +
                            "(SELECT id AS b_id, name AS b_name, password AS b_password, role AS b_role, \n" +
                            "latitude AS b_latitude,longitude AS b_longitude FROM auction_user) AS lot_buyer \n" +
                            "ON lot.buyer = lot_buyer.b_id \n" +
                        "LEFT JOIN \n" +
                            "(SELECT id as b_role_id,name as b_role_name FROM role)AS buyer_role \n" +
                            "ON lot_buyer.b_role = buyer_role.b_role_id \n" +
                        "LEFT JOIN \n" +
                            "(SELECT id as cat_id,name as cat_name FROM category)AS cat \n" +
                            "ON lot.category = cat.cat_id \n" +
                        "LEFT JOIN \n" +
                            "(SELECT id as d_id,name as d_name,w_price,v_price,d_price,min_weight,max_weight, \n" +
                            "min_volume,max_volume,min_distance,max_distance FROM delivery) as d \n" +
                            "ON lot.delivery = d.d_id \n" +
                        "WHERE lot.category = ? \n" +
                        "ORDER BY id \n" +
                        "LIMIT ? \n" +
                        "OFFSET ?;";
        //@formatter:on
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setLong(1, category.getId());
            statement.setLong(2, limit);
            statement.setLong(3, offset);
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                Lot lot = new Lot();
                parseResultSet(rs, lot);
                lots.add(lot);
            }
            return lots;
        } catch (SQLException e) {
            throw new PersistException(e);
        }
    }

    /**
     * @see LotDao#getByOwner(User, long, long)
     */
    @Override
    public List<Lot> getByOwner(User user, long limit, long offset) throws PersistException {
        List<Lot> lots = new ArrayList<>();
        String sql =
                //@formatter:off
                "SELECT *" +
                        "FROM lot \n" +
                        "LEFT JOIN \n" +
                            "(SELECT id AS o_id, name AS o_name, password AS o_password, role AS o_role,\n" +
                            "latitude AS o_latitude,longitude AS o_longitude FROM auction_user) AS lot_owner \n" +
                            "ON lot.lot_owner = lot_owner.o_id \n" +
                        "LEFT JOIN \n" +
                            "(SELECT id as o_role_id,name as o_role_name FROM role)AS owner_role \n" +
                            "ON lot_owner.o_role = owner_role.o_role_id \n" +
                        "LEFT JOIN \n" +
                            "(SELECT id AS b_id, name AS b_name, password AS b_password, role AS b_role,\n" +
                            "latitude AS b_latitude,longitude AS b_longitude FROM auction_user) AS lot_buyer \n" +
                            "ON lot.buyer = lot_buyer.b_id \n" +
                        "LEFT JOIN \n" +
                            "(SELECT id as b_role_id,name as b_role_name FROM role)AS buyer_role \n" +
                            "ON lot_buyer.b_role = buyer_role.b_role_id \n" +
                        "LEFT JOIN \n" +
                            "(SELECT id as cat_id,name as cat_name FROM category)AS cat \n" +
                            "ON lot.category = cat.cat_id \n" +
                        "LEFT JOIN \n" +
                            "(SELECT id as d_id,name as d_name,w_price,v_price,d_price,min_weight,max_weight, \n" +
                            "min_volume,max_volume,min_distance,max_distance FROM delivery) as d \n" +
                            "ON lot.delivery = d.d_id \n" +
                        "WHERE lot.lot_owner = ? \n" +
                        "ORDER BY id \n" +
                        "LIMIT ? \n" +
                        "OFFSET ?;";
        // @formatter:on
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setLong(1, user.getId());
            stm.setLong(2, limit);
            stm.setLong(3, offset);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                Lot lot = new Lot();
                parseResultSet(rs, lot);
                lots.add(lot);
            }
            return lots;
        } catch (SQLException e) {
            throw new PersistException(e);
        }
    }

    /**
     * @see LotDao#getByDescription(String, long, long)
     */
    @Override
    public List<Lot> getByDescription(String description, long limit, long offset) throws PersistException {
        List<Lot> lots = new ArrayList<>();
        String sql =
                //@formatter:off
                "SELECT *" +
                        "FROM lot \n" +
                        "LEFT JOIN \n" +
                            "(SELECT id AS o_id, name AS o_name, password AS o_password, role AS o_role,\n" +
                            "latitude AS o_latitude,longitude AS o_longitude FROM auction_user) AS lot_owner \n" +
                            "ON lot.lot_owner = lot_owner.o_id \n" +
                        "LEFT JOIN \n" +
                            "(SELECT id as o_role_id,name as o_role_name FROM role)AS owner_role \n" +
                            "ON lot_owner.o_role = owner_role.o_role_id \n" +
                        "LEFT JOIN \n" +
                            "(SELECT id AS b_id, name AS b_name, password AS b_password, role AS b_role,\n" +
                            "latitude AS b_latitude,longitude AS b_longitude FROM auction_user) AS lot_buyer \n" +
                            "ON lot.buyer = lot_buyer.b_id \n" +
                        "LEFT JOIN \n" +
                            "(SELECT id as b_role_id,name as b_role_name FROM role)AS buyer_role \n" +
                            "ON lot_buyer.b_role = buyer_role.b_role_id \n" +
                        "LEFT JOIN \n" +
                            "(SELECT id as cat_id,name as cat_name FROM category)AS cat \n" +
                            "ON lot.category = cat.cat_id \n" +
                        "LEFT JOIN \n" +
                            "(SELECT id as d_id,name as d_name,w_price,v_price,d_price,min_weight,max_weight, \n" +
                            "min_volume,max_volume,min_distance,max_distance FROM delivery) as d \n" +
                            "ON lot.delivery = d.d_id \n" +
                        "WHERE lot.description LIKE ? \n" +
                        "ORDER BY description \n" +
                        "LIMIT ? \n" +
                        "OFFSET ?;";
        //@formatter:on
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, "%" + description + "%");
            stm.setLong(2, limit);
            stm.setLong(3, offset);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                Lot lot = new Lot();
                parseResultSet(rs, lot);
                lots.add(lot);
            }
            return lots;
        } catch (SQLException e) {
            throw new PersistException(e);
        }
    }

    /**
     * @see LotDao#getSortedByKey(OrderKeysForLot, long, long)
     */
    @Override
    public List<Lot> getSortedByKey(OrderKeysForLot key, long limit, long offset) throws PersistException {
        String orderByColumnName;
        switch (key) {
            case ID:
                orderByColumnName = "id";
                break;
            case CATEGORY:
                orderByColumnName = "cat_name";
                break;
            case DESCRIPTION:
                orderByColumnName = "description";
                break;
            case OWNER:
                orderByColumnName = "o_id";
                break;
            default:
                return null;
        }
        List<Lot> lots = new ArrayList<>();
        String sql =
                //@formatter:off
                "SELECT *" +
                        "FROM lot \n" +
                        "LEFT JOIN \n" +
                            "(SELECT id AS o_id, name AS o_name, password AS o_password, role AS o_role,\n" +
                            "latitude AS o_latitude,longitude AS o_longitude FROM auction_user) AS lot_owner \n" +
                            "ON lot.lot_owner = lot_owner.o_id \n" +
                        "LEFT JOIN \n" +
                            "(SELECT id as o_role_id,name as o_role_name FROM role)AS owner_role \n" +
                            "ON lot_owner.o_role = owner_role.o_role_id \n" +
                        "LEFT JOIN \n" +
                            "(SELECT id AS b_id, name AS b_name, password AS b_password, role AS b_role,\n" +
                            "latitude AS b_latitude,longitude AS b_longitude FROM auction_user) AS lot_buyer \n" +
                            "ON lot.buyer = lot_buyer.b_id \n" +
                        "LEFT JOIN \n" +
                            "(SELECT id as b_role_id,name as b_role_name FROM role)AS buyer_role \n" +
                            "ON lot_buyer.b_role = buyer_role.b_role_id \n" +
                        "LEFT JOIN \n" +
                            "(SELECT id as cat_id,name as cat_name FROM category)AS cat \n" +
                            "ON lot.category = cat.cat_id \n" +
                        "LEFT JOIN \n" +
                            "(SELECT id as d_id,name as d_name,w_price,v_price,d_price,min_weight,max_weight, \n" +
                            "min_volume,max_volume,min_distance,max_distance FROM delivery) as d \n" +
                            "ON lot.delivery = d.d_id \n" +
                        "ORDER BY " +orderByColumnName+" \n" +
                        "LIMIT ? \n" +
                        "OFFSET ?;";
        //@formatter:on
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setLong(1, limit);
            stm.setLong(2, offset);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                Lot lot = new Lot();
                parseResultSet(rs, lot);
                lots.add(lot);
            }
            return lots;
        } catch (SQLException e) {
            throw new PersistException(e);
        }
    }

    /**
     * @see LotDao#countLots()
     */
    @Override
    public Long countLots() {
        String sql = "SELECT COUNT(id)FROM lot;";
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stm = connection.prepareStatement(sql);
            ResultSet rs = stm.executeQuery();
            rs.next();
            return rs.getLong("count");
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * @see LotDao#countLotsWithSpecifiedOwner(User)
     */
    @Override
    public Long countLotsWithSpecifiedOwner(User owner) {
        String sql = "SELECT COUNT(id)FROM lot WHERE lot_owner = ?;";
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setLong(1, owner.getId());
            ResultSet rs = stm.executeQuery();
            rs.next();
            return rs.getLong("count");
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * @see LotDao#countLotsWithSpecifiedCategory(Category)
     */
    @Override
    public Long countLotsWithSpecifiedCategory(Category category) {
        String sql = "SELECT COUNT(id)FROM lot WHERE category = ?;";
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setLong(1, category.getId());
            ResultSet rs = stm.executeQuery();
            rs.next();
            return rs.getLong("count");
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * @see LotDao#countLotsWithSpecifiedDescription(String)
     */
    @Override
    public Long countLotsWithSpecifiedDescription(String description) {
        String sql = "SELECT COUNT(id)FROM lot WHERE description LIKE ?;";
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, "%" + description + "%");
            ResultSet rs = stm.executeQuery();
            rs.next();
            return rs.getLong("count");
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    private void parseResultSet(ResultSet rs, Lot lot) throws SQLException, PersistException {
        lot.setId(rs.getLong("id"));
        lot.setDescription(rs.getString("description"));
        lot.setPrice(rs.getInt("price"));
        lot.setQuantity(rs.getInt("quantity"));
        lot.setWeight(rs.getFloat("weight"));
        lot.setWidth(rs.getFloat("width"));
        lot.setHeight(rs.getFloat("height"));
        lot.setDepth(rs.getFloat("depth"));
        lot.setLotSold(rs.getBoolean("lotsold"));
        long entityId = rs.getLong("lot_owner");
        if (entityId == 0)
            lot.setOwner(null);
        else {
            User user = new User();
            lot.setOwner(user);
            parseUser(rs, user, "o");
        }
        entityId = rs.getLong("buyer");
        if (entityId == 0)
            lot.setBuyer(null);
        else {
            User user = new User();
            lot.setBuyer(user);
            parseUser(rs, user, "b");
        }
        entityId = rs.getLong("category");
        if (entityId == 0)
            lot.setCategory(null);
        else {
            Category category = new Category();
            lot.setCategory(category);
            category.setId(rs.getLong("cat_id"));
            category.setCategoryName(rs.getString("cat_name"));
        }
        entityId = rs.getLong("delivery");
        if (entityId == 0)
            lot.setDeliveryService(null);
        else {
            DeliveryService deliveryService = new DeliveryService();
            lot.setDeliveryService(deliveryService);
            deliveryService.setId(rs.getLong("d_id"));
            deliveryService.setTitle(rs.getString("d_name"));
            deliveryService.setDistancePrice(rs.getFloat("d_price"));
            deliveryService.setVolumePrice(rs.getFloat("v_price"));
            deliveryService.setWeightPrice(rs.getFloat("w_price"));
            deliveryService.setMaxDistance(rs.getDouble("max_distance"));
            deliveryService.setMaxVolume(rs.getDouble("max_volume"));
            deliveryService.setMaxWeight(rs.getLong("max_weight"));
            deliveryService.setMinDistance(rs.getLong("min_distance"));
            deliveryService.setMinVolume(rs.getLong("min_volume"));
            deliveryService.setMinWeight(rs.getLong("min_weight"));
        }
    }

    private void parseUser(ResultSet rs, User user, String tableName) throws SQLException {
        user.setId(rs.getLong(tableName + "_id"));
        user.setUserName(rs.getString(tableName + "_name"));
        user.setPassword(rs.getString(tableName + "_password"));
        user.setLatitude(rs.getDouble(tableName + "_latitude"));
        user.setLongitude(rs.getDouble(tableName + "_longitude"));
        long userRoleId = rs.getLong(tableName + "_role");
        if (userRoleId == 0)
            user.setRole(null);
        else {
            Role role = new Role();
            user.setRole(role);
            role.setId(rs.getLong(tableName + "_role_id"));
            role.setRoleName(rs.getString(tableName + "_role_name"));
        }
    }
}
