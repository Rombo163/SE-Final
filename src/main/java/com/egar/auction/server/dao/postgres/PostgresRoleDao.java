package com.egar.auction.server.dao.postgres;


import com.egar.auction.server.dao.PersistException;
import com.egar.auction.server.dao.RoleDao;
import com.egar.auction.server.domain.Role;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Role DAO implementation for Postgres SQL.
 *
 * @author Lisitsyn R.
 */
public class PostgresRoleDao implements RoleDao {
    private final DataSource dataSource;

    /**
     * Constructs new PostgresRoleDAO with specified DataSource.
     *
     * @param dataSource specified DataSource.
     */
    PostgresRoleDao(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    /**
     * @see RoleDao#persist(Role)
     */
    @Override
    public Role persist(Role role) throws PersistException {
        String sql = "INSERT INTO role (name) VALUES (?);";
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setString(1, role.getRoleName());
            int count = statement.executeUpdate();
            if (count != 1)
                throw new PersistException("On persist modify more then 1 record:" + count);
        } catch (SQLException e) {
            throw new PersistException(e);
        }
        //sql = "SELECT CURRVAL(pg_get_serial_sequence('Role','id')) AS last_insert_id;";
        sql = "SELECT LASTVAL();";
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement statement = connection.prepareStatement(sql);
            ResultSet resultSet = statement.executeQuery();
            resultSet.next();
            role.setId(resultSet.getLong(1));
            return role;
        } catch (SQLException e) {
            throw new PersistException(e);
        }
    }

    /**
     * @see RoleDao#getById(long)
     */
    @Override
    public Role getById(long pk) throws PersistException {
        String sql = "SELECT * FROM role WHERE id = ?;";
        Role role = new Role();
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setLong(1, pk);
            ResultSet rs = stm.executeQuery();
            if (!rs.next())
                return null;
            else {
                //rs.next();
                role.setId(rs.getLong("id"));
                role.setRoleName(rs.getString("name"));
                return role;
            }
        } catch (SQLException e) {
            throw new PersistException();
        }
    }

    /**
     * @see RoleDao#update(Role)
     */
    @Override
    public void update(Role role) throws PersistException {

        String sql = "UPDATE role SET name = ? WHERE id = ?;";
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setString(1, role.getRoleName());
            statement.setLong(2, role.getId());
            int count = statement.executeUpdate();
            if (count != 1)
                throw new PersistException("On update modify more then 1 record: " + count);
        } catch (SQLException e) {
            throw new PersistException(e);
        }
    }

    /**
     * @see RoleDao#delete(Role)
     */
    @Override
    public void delete(Role role) throws PersistException {
        String sql = "DELETE FROM role WHERE id = ?;";
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setLong(1, role.getId());
            int count = statement.executeUpdate();
            if (count != 1)
                throw new PersistException("On delete modify more then 1 record: " + count);
        } catch (SQLException e) {
            throw new PersistException(e);
        }

    }
}
