package com.egar.auction.server.dao;

import com.egar.auction.server.OrderKeysForBid;
import com.egar.auction.server.domain.Bid;
import com.egar.auction.server.domain.Lot;
import com.egar.auction.server.domain.User;

import java.util.List;

/**
 * BidDao interface for Bid entity.
 *
 * @author Lisitsyn R.
 */
public interface BidDao {

    /**
     * Persists bid in persistent data storage.
     *
     * @param bid bid.
     * @return same bid with set id.
     * @throws PersistException
     */
    Bid persist(Bid bid) throws PersistException;

    /**
     * Returns bid with specifies id.
     *
     * @param pk id.
     * @return bid with specifies id.
     * @throws PersistException
     */
    Bid getById(long pk) throws PersistException;

    /**
     * Update specified bid.
     *
     * @param bid for update.
     * @throws PersistException
     */
    void update(Bid bid) throws PersistException;

    /**
     * Delete specified bid
     *
     * @param bid for delete.
     * @throws PersistException
     */
    void delete(Bid bid) throws PersistException;

    /**
     * Returns List of the bids with specified Owner ordered by id.
     *
     * @param user   specified owner.
     * @param limit  for sql query.
     * @param offset for sql query.
     * @return Returns List of the bids.
     * @throws PersistException
     */
 /*   List<Bid> getBidsByOwnerWithOrderById(User user, long limit, long offset) throws PersistException;

    *//**
     * Returns List of the bids with specified Owner ordered by lot.
     *
     * @param user   specified owner.
     * @param limit  for sql query.
     * @param offset for sql query.
     * @return Returns List of the bids.
     * @throws PersistException
     *//*
    List<Bid> getBidsByOwnerWithOrderByLot(User user, long limit, long offset) throws PersistException;

    *//**
     * Returns List of the bids with specified lot ordered by id.
     *
     * @param lot    specified lot.
     * @param limit  for sql query.
     * @param offset for sql query.
     * @return Returns List of the bids.
     * @throws PersistException
     *//*
    List<Bid> getBidsByLotWithOrderById(Lot lot, long limit, long offset) throws PersistException;

    *//**
     * Returns List of the bids with specified lot ordered by owner.
     *
     * @param lot    specified lot.
     * @param limit  for sql query.
     * @param offset for sql query.
     * @return Returns List of the bids.
     * @throws PersistException
     *//*
    List<Bid> getBidsByLotWithOrderByOwner(Lot lot, long limit, long offset) throws PersistException;
*/

    /**
     * Returns List of the Bids with specified Lot and ordered by key.
     *
     * @param key    Order key.
     * @param lot    specified lot
     * @param limit  for sql query.
     * @param offset for sql query.
     * @return Returns List of the bids.
     * @throws PersistException
     */
    List<Bid> getBidsByLotWithOrderByKey(OrderKeysForBid key, Lot lot, long limit, long offset) throws PersistException;

    /**
     * Returns List of the Bids with specified owner and ordered by key.
     *
     * @param key    Order key.
     * @param user   specified owner.
     * @param limit  for sql query.
     * @param offset for sql query.
     * @return Returns List of the bids.
     * @throws PersistException
     */
    List<Bid> getBidsByOwnerWithOrderByKey(OrderKeysForBid key, User user, long limit, long offset) throws PersistException;

    /**
     * Returns number of the Bids with specified owner  from persistent data storage.
     *
     * @param owner owner of the bids.
     * @return bid number.
     */
    Long countBidsWithSpecifiedOwner(User owner);

    /**
     * Returns number of the Bids with specified lot  from persistent data storage.
     *
     * @param lot owner of the bids.
     * @return bid number.
     */
    Long countBidsWithSpecifiedLot(Lot lot);
}
