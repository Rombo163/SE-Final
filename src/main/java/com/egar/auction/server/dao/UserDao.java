package com.egar.auction.server.dao;

import com.egar.auction.server.OrderKeysForUser;
import com.egar.auction.server.domain.User;

import java.util.List;

/**
 * Lot DAO interface for Lot entity.
 *
 * @author Lisitsyn R.
 */
public interface UserDao {

    /**
     * Persists specified User in persistent data storage.
     *
     * @param user specified User.
     * @return user with set id.
     * @throws PersistException
     */
    User persist(User user) throws PersistException;

    /**
     * Returns User from persistent data storage by specified id.
     *
     * @param pk id.
     * @return User from persistent data storage by specified id.
     * @throws PersistException
     */
    User getById(long pk) throws PersistException;

    /**
     * Returns List of the Users names of which contains part of specified string.
     *
     * @param name   specified string.
     * @param limit  limit for sql query.
     * @param offset offset for sql query.
     * @return Returns List of the Users.
     * @throws PersistException
     */
    List<User> getByUserName(String name, long limit, long offset) throws PersistException;

    /**
     * Updates specified User in persistent data storage.
     *
     * @param user specified User.
     * @throws PersistException
     */
    void update(User user) throws PersistException;

    /**
     * Deletes specified User from persistent data storage.
     *
     * @param user specified User.
     * @throws PersistException
     */
    void delete(User user) throws PersistException;

    /**
     * Returns List of the Users.
     *
     * @param limit  limit for sql query.
     * @param offset offset for sql query.
     * @return List of the Users.
     * @throws PersistException
     */
    List<User> getUsers(long limit, long offset) throws PersistException;

    /**
     * Returns number of the Lots with specified owner from persistent data storage.
     *
     * @param user specified owner.
     * @return number of the Lots with specified owner from persistent data storage.
     */
    Long countLotsForUser(User user);

    /**
     * Returns number of the Bids with specified owner from persistent data storage.
     *
     * @param user specified owner.
     * @return number of the Bids with specified owner from persistent data storage.
     */
    Long countBidsForUser(User user);

    /**
     * Returns number of the Users from persistent data storage.
     *
     * @return number of the Users from persistent data storage.
     */
    Long countUsers();

    /**
     * Returns User by his credentials.
     *
     * @param userName name of the user.
     * @param password password of the user.
     * @return User by his credentials.
     * @throws PersistException
     */
    User getByCredentials(String userName, String password) throws PersistException;

    /**
     * Returns List of the Users sorted by one of the fields depending of the specified key.
     *
     * @param key    enum key.
     * @param limit  limit for sql query.
     * @param offset offset for sql query
     * @return List of the Users.
     * @throws PersistException
     */
    List<User> getUsersSortedByKey(OrderKeysForUser key, long limit, long offset) throws PersistException;

    /**
     * Returns number of the Users with specified name from persistent data storage.
     *
     * @return number of the Users with specified name from persistent data storage.
     */
    Long countUsersWithSpecifiedName(String name);

}
