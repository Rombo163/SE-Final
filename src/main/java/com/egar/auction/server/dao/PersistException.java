package com.egar.auction.server.dao;

/**
 * Common exception for all types of persistent data storage.
 *
 * @author Lisicyn R.
 */
public class PersistException extends Exception {
    /**
     * @see Exception#Exception()
     */
    public PersistException() {
    }

    /**
     * @see Exception#Exception(String)
     */
    public PersistException(String message) {
        super(message);
    }

    /**
     * @see Exception#Exception(String, Throwable)
     */
    public PersistException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * @see Exception#Exception(Throwable)
     */
    public PersistException(Throwable cause) {
        super(cause);
    }

    /**
     * @see Exception#Exception(String, Throwable, boolean, boolean)
     */
    public PersistException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}