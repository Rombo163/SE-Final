package com.egar.auction.server.dao;

import com.egar.auction.server.OrderKeysForLot;
import com.egar.auction.server.domain.Category;
import com.egar.auction.server.domain.Lot;
import com.egar.auction.server.domain.User;

import java.util.List;

/**
 * Lot DAO Interface for Lot entity.
 *
 * @author Lisitsyn R.
 */
public interface LotDao {

    /**
     * Persists specified Lot in persistent data storage.
     *
     * @param lot specified Lot.
     * @return lot.
     * @throws PersistException
     */
    Lot persist(Lot lot) throws PersistException;

    /**
     * Returns lot with specified id.
     *
     * @param pk id.
     * @return lot with specified id.
     * @throws PersistException
     */
    Lot getById(long pk) throws PersistException;

    /**
     * Updates specified lot in persistent data storage.
     *
     * @param lot specified lot.
     * @throws PersistException
     */
    void update(Lot lot) throws PersistException;

    /**
     * Deletes specified lot from persistent data storage.
     *
     * @param lot specified lot.
     * @throws PersistException
     */
    void delete(Lot lot) throws PersistException;

    /**
     * Returns List of the lots with specified owner and lotSold.
     *
     * @param lotSold lotSold.
     * @param userId  owner id.
     * @param limit   limit for sql query.
     * @param offset  offswt fro sql query.
     * @return List of the lots
     * @throws PersistException
     */
    List<Lot> getBySold(boolean lotSold, long userId, long limit, long offset) throws PersistException;

    /**
     * Returns List of the Lots with specified Category.
     *
     * @param category specified category.
     * @param limit    limit for sql query.
     * @param offset   offset for sql query.
     * @return Returns List of the Lots with specified Category.
     * @throws PersistException
     */
    List<Lot> getByCategory(Category category, long limit, long offset) throws PersistException;

    /**
     * Returns List of the Lots with specified owner.
     *
     * @param user   specified user.
     * @param limit  limit for sql query.
     * @param offset offset for sql query.
     * @return List of the Lots.
     * @throws PersistException
     */
    List<Lot> getByOwner(User user, long limit, long offset) throws PersistException;

    /**
     * Returns List of the Lots with Categories  names of which, contain key sting.
     *
     * @param description key String.
     * @param limit       limit for sql query.
     * @param offset      offset for sql query.
     * @return List of the Lots with Categories.
     * @throws PersistException
     */
    List<Lot> getByDescription(String description, long limit, long offset) throws PersistException;

    /**
     * Returns List of the Lots sorted by one of parameters depending of the key.
     *
     * @param key    key.
     * @param limit  limit for db query.
     * @param offset offset for db query
     * @return List of the Lots sorted by one of parameters depending of the key.
     * @throws PersistException
     */
    List<Lot> getSortedByKey(OrderKeysForLot key, long limit, long offset) throws PersistException;

    /**
     * Returns number of the Lots from persistent data storage.
     *
     * @return number of the Lots from persistent data storage.
     */
    Long countLots();

    /**
     * Returns number of the Lots with specified owner  from persistent data storage.
     *
     * @return number of the Lots with specified owner from persistent data storage.
     */
    Long countLotsWithSpecifiedOwner(User owner);

    /**
     * Returns number of the Lots with specified category  from persistent data storage.
     *
     * @return number of the Lots with specified category from persistent data storage.
     */
    Long countLotsWithSpecifiedCategory(Category category);

    /**
     * Returns number of the Lots with specified description  from persistent data storage.
     *
     * @return number of the Lots with specified description from persistent data storage.
     */
    Long countLotsWithSpecifiedDescription(String description);

}
