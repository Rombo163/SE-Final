package com.egar.auction.server.dao.jpa;

import com.egar.auction.server.dao.DeliveryServiceDao;
import com.egar.auction.server.dao.PersistException;
import com.egar.auction.server.domain.DeliveryService;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;

/**
 * JPA DeliveryService DAO.
 *
 * @author Lisitsyn.
 */
public class JPADeliveryServiceDAO implements DeliveryServiceDao {

    private EntityManagerFactory emf;

    /**
     * Constructs new JPADeliveryServiceDAO with specified EntityManager.
     *
     * @param emf EntityManagerFactory.
     */
    public JPADeliveryServiceDAO(EntityManagerFactory emf) {
        this.emf = emf;
    }

    /**
     * @see DeliveryServiceDao#persist(DeliveryService)
     */
    @Override
    public DeliveryService persist(DeliveryService service) throws PersistException {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        em.persist(service);
        em.getTransaction().commit();
        em.close();
        return service;
    }

    /**
     * @see DeliveryServiceDao#getById(long)
     */
    @Override
    public DeliveryService getById(long pk) throws PersistException {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        DeliveryService result = em.find(DeliveryService.class, pk);
        em.getTransaction().commit();
        em.close();
        return result;
    }

    /**
     * @see DeliveryServiceDao#update(DeliveryService)
     */
    @Override
    public void update(DeliveryService deliveryService) throws PersistException {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        em.merge(deliveryService);
        em.getTransaction().commit();
        em.close();
    }

    /**
     * @see DeliveryServiceDao#delete(DeliveryService)
     */
    @Override
    public void delete(DeliveryService deliveryService) throws PersistException {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        DeliveryService mergedDelivery = em.merge(deliveryService);
        em.remove(mergedDelivery);
        em.getTransaction().commit();
        em.close();
    }

    /**
     * @see DeliveryServiceDao#getAverageWeightPrice()
     */
    @Override
    public Double getAverageWeightPrice() throws PersistException {
        EntityManager em = emf.createEntityManager();
        Query q = em.createQuery("SELECT AVG(d.weightPrice) FROM DeliveryService d");
        Double result = (Double) q.getSingleResult();
        em.close();
        return result;
    }

    /**
     * @see DeliveryServiceDao#getAverageDistancePrice()
     */
    @Override
    public Double getAverageDistancePrice() throws PersistException {
        EntityManager em = emf.createEntityManager();
        Query q = em.createQuery("SELECT AVG(d.distancePrice) FROM DeliveryService d");
        Double result = (Double) q.getSingleResult();
        em.close();
        return result;
    }

    /**
     * @see DeliveryServiceDao#getAverageVolumePrice()
     */
    @Override
    public Double getAverageVolumePrice() throws PersistException {
        EntityManager em = emf.createEntityManager();
        Query q = em.createQuery("SELECT AVG(d.volumePrice) FROM DeliveryService d");
        Double result = (Double) q.getSingleResult();
        em.close();
        return result;
    }
}
