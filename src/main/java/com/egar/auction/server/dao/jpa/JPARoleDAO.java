package com.egar.auction.server.dao.jpa;

import com.egar.auction.server.dao.PersistException;
import com.egar.auction.server.dao.RoleDao;
import com.egar.auction.server.domain.Role;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 * JPA Role DAO.
 *
 * @author Lisitsyn.
 */
public class JPARoleDAO implements RoleDao {

    private EntityManagerFactory emf;

    /**
     * Constructs new JPARoleDAO with specified EntityManager.
     *
     * @param emf EntityManagerFactory to set.
     */
    public JPARoleDAO(EntityManagerFactory emf) {
        this.emf = emf;
    }

    /**
     * @see RoleDao#persist(Role)
     */
    @Override
    public Role persist(Role role) throws PersistException {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        em.persist(role);
        em.getTransaction().commit();
        em.close();
        return role;
    }

    /**
     * @see RoleDao#getById(long)
     */
    @Override
    public Role getById(long pk) throws PersistException {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        Role role = em.find(Role.class, pk);
        em.getTransaction().commit();
        em.close();
        return role;
    }

    /**
     * @see RoleDao#update(Role)
     */
    @Override
    public void update(Role role) throws PersistException {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        em.merge(role);
        em.getTransaction().commit();
        em.close();
    }

    /**
     * @see RoleDao#delete(Role)
     */
    @Override
    public void delete(Role role) throws PersistException {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        Role mergedRole = em.merge(role);
        em.remove(mergedRole);
        em.getTransaction().commit();
        em.close();
    }
}
