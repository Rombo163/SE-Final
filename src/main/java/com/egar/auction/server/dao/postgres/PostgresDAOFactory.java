package com.egar.auction.server.dao.postgres;

import org.postgresql.ds.PGPoolingDataSource;
import com.egar.auction.server.dao.*;

import javax.sql.DataSource;
import java.util.ResourceBundle;

/**
 * Postgres DAO Factory impl.
 *
 * @author Lisitsyn.
 */
public class PostgresDAOFactory extends DAOFactory {

    private static final String DS_NAME = "auctionDS";
    private DataSource dataSource;

    /**
     * Constructs new PostgresDAOFactory
     */
    public PostgresDAOFactory() {
        ResourceBundle props = ResourceBundle.getBundle("db");

        PGPoolingDataSource dataSource = new PGPoolingDataSource();
        dataSource.setDataSourceName(DS_NAME);
        dataSource.setServerName(props.getString("org.postgres.db.host"));
        dataSource.setPortNumber(Integer.valueOf(props.getString("org.postgres.db.port")));
        dataSource.setDatabaseName(props.getString("org.postgres.db.name"));
        dataSource.setUser(props.getString("org.postgres.db.user"));
        dataSource.setPassword(props.getString("org.postgres.db.pass"));
        dataSource.setInitialConnections(10);

        this.dataSource = dataSource;
    }

    /**
     * Returns user DAO.
     */
    @Override
    public UserDao getUserDAO() {
        return new PostgresUserDao(dataSource);
    }

    /**
     * Returns bid DAO.
     */
    @Override
    public BidDao getBidDAO() {
        return new PostgresBidDao(dataSource);
    }

    /**
     * Returns category DAO.
     */
    @Override
    public CategoryDao getCategoryDAO() {
        return new PostgresCategoryDao(dataSource);
    }

    /**
     * Returns delivery service DAO.
     */
    @Override
    public DeliveryServiceDao getDeliveryServiceDAO() {
        return new PostgresDeliveryServiceDao(dataSource);
    }

    /**
     * Returns lot DAO.
     */
    @Override
    public LotDao getLotDAO() {
        return new PostgresLotDao(dataSource);
    }

    /**
     * Returns role DAO.
     */
    @Override
    public RoleDao getRoleDAO() {
        return new PostgresRoleDao(dataSource);
    }
}
