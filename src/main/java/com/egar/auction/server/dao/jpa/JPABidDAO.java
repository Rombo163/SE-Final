package com.egar.auction.server.dao.jpa;

import com.egar.auction.server.OrderKeysForBid;
import com.egar.auction.server.dao.BidDao;
import com.egar.auction.server.dao.PersistException;
import com.egar.auction.server.domain.Bid;
import com.egar.auction.server.domain.Lot;
import com.egar.auction.server.domain.User;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

/**
 * JPA Bid DAO.
 *
 * @author Lisitsyn.
 */
public class JPABidDAO implements BidDao {

    private EntityManagerFactory emf;

    /**
     * Constructs new JPABidDAO  with specified EntityManager.
     *
     * @param emf EntityManager to set.
     */
    public JPABidDAO(EntityManagerFactory emf) {
        this.emf = emf;
    }

    /**
     * @see BidDao#persist(Bid)
     */
    @Override
    public Bid persist(Bid bid) throws PersistException {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        em.persist(bid);
        em.getTransaction().commit();
        em.close();
        return bid;
    }

    /**
     * @see BidDao#getById(long)
     */
    @Override
    public Bid getById(long pk) throws PersistException {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        Bid bid = em.find(Bid.class, pk);
        em.getTransaction().commit();
        em.close();
        return bid;
    }

    /**
     * @see BidDao#update(Bid)
     */
    @Override
    public void update(Bid bid) throws PersistException {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        em.merge(bid);
        em.getTransaction().commit();
        em.close();
    }

    /**
     * @see BidDao#delete(Bid)
     */
    @Override
    public void delete(Bid bid) throws PersistException {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        Bid mergedBid = em.merge(bid);
        em.remove(mergedBid);
        em.getTransaction().commit();
        em.close();
    }

    /**
     * @see BidDao#getBidsByLotWithOrderByKey(OrderKeysForBid, Lot, long, long)
     */
    @Override
    public List<Bid> getBidsByLotWithOrderByKey(OrderKeysForBid key, Lot lot, long limit, long offset) throws PersistException {
        EntityManager em = emf.createEntityManager();
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Bid> bidCriteriaQuery = cb.createQuery(Bid.class);
        Root<Bid> bidRoot = bidCriteriaQuery.from(Bid.class);
        bidCriteriaQuery.select(bidRoot);
        bidCriteriaQuery.where(cb.equal(bidRoot.get("lot"), lot));
        switch (key) {
            case ID:
                bidCriteriaQuery.orderBy(cb.asc(bidRoot.get("id")));
                break;
            case OWNER:
                bidCriteriaQuery.orderBy(cb.asc(bidRoot.get("owner")));
                break;
            case LOT:
                bidCriteriaQuery.orderBy(cb.asc(bidRoot.get("lot")));
                break;
            default:
                em.close();
                return null;
        }
        List<Bid> result = em.createQuery(bidCriteriaQuery).setFirstResult((int) offset).setMaxResults((int) limit).getResultList();
        em.close();
        return result;
    }

    /**
     * @see BidDao#getBidsByOwnerWithOrderByKey(OrderKeysForBid, User, long, long)
     */
    @Override
    public List<Bid> getBidsByOwnerWithOrderByKey(OrderKeysForBid key, User user, long limit, long offset) throws PersistException {
        EntityManager em = emf.createEntityManager();
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Bid> bidCriteriaQuery = cb.createQuery(Bid.class);
        Root<Bid> bidRoot = bidCriteriaQuery.from(Bid.class);
        bidCriteriaQuery.select(bidRoot);
        bidCriteriaQuery.where(cb.equal(bidRoot.get("owner"), user));
        switch (key) {
            case ID:
                bidCriteriaQuery.orderBy(cb.asc(bidRoot.get("id")));
                break;
            case OWNER:
                bidCriteriaQuery.orderBy(cb.asc(bidRoot.get("owner")));
                break;
            case LOT:
                bidCriteriaQuery.orderBy(cb.asc(bidRoot.get("lot")));
                break;
            default:
                em.close();
                return null;
        }
        List<Bid> result = em.createQuery(bidCriteriaQuery).setFirstResult((int) offset).setMaxResults((int) limit).getResultList();
        em.close();
        return result;
    }

    /**
     * @see BidDao#countBidsWithSpecifiedOwner(User)
     */
    @Override
    public Long countBidsWithSpecifiedOwner(User owner) {
        EntityManager em = emf.createEntityManager();
        Query q = em.createQuery("SELECT COUNT(l) FROM Bid b WHERE b.bid_owner := owner");
        q.setParameter("owner", owner);
        Long result = (Long) q.getSingleResult();
        em.close();
        return result;
    }

    /**
     * @see BidDao#countBidsWithSpecifiedLot(Lot)
     */
    @Override
    public Long countBidsWithSpecifiedLot(Lot lot) {
        EntityManager em = emf.createEntityManager();
        Query q = em.createQuery("SELECT COUNT(l) FROM Bid b WHERE b.lot := lot");
        q.setParameter("lot", lot);
        Long result = (Long) q.getSingleResult();
        em.close();
        return result;
    }
}
