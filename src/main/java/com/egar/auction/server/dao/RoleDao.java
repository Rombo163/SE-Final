package com.egar.auction.server.dao;


import com.egar.auction.server.domain.Role;

/**
 * Role DAO interface for Role entity.
 *
 * @author Lisicyn R.
 */
public interface RoleDao {
    /**
     * Persists specified role in persistent data storage.
     *
     * @param role specified role.
     * @return role.
     * @throws PersistException
     */
    Role persist(Role role) throws PersistException;

    /**
     * Returns role by specified id.
     *
     * @param pk id.
     * @return role.
     * @throws PersistException
     */
    Role getById(long pk) throws PersistException;

    /**
     * Updates specified role in persistent data storage.
     *
     * @param role specified role.
     * @throws PersistException
     */
    void update(Role role) throws PersistException;

    /**
     * Deletes Role from persistent data storage.
     *
     * @param role specified role.
     * @throws PersistException
     */
    void delete(Role role) throws PersistException;
}
