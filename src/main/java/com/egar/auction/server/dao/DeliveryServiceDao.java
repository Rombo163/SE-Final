package com.egar.auction.server.dao;


import com.egar.auction.server.domain.DeliveryService;

/**
 * DeliveryService DAO interface for DeliveryService entity.
 *
 * @author Lisicyn R.
 */
public interface DeliveryServiceDao {

    /**
     * Persists specified DeliveryService in persistent data storage.
     *
     * @param service specified DeliveryService.
     * @return DeliveryService after set it's id.
     * @throws PersistException
     */
    DeliveryService persist(DeliveryService service) throws PersistException;

    /**
     * Returns DeliveryService by specified id.
     *
     * @param pk id.
     * @return DeliveryService by specified id.
     * @throws PersistException
     */
    DeliveryService getById(long pk) throws PersistException;

    /**
     * Updates specified DeliveryService in persistent data storage.
     *
     * @param deliveryService specified DeliveryService.
     * @throws PersistException
     */
    void update(DeliveryService deliveryService) throws PersistException;

    /**
     * Updates specified DeliveryService from persistent data storage.
     *
     * @param deliveryService specified DeliveryService.
     * @throws PersistException
     */
    void delete(DeliveryService deliveryService) throws PersistException;

    /**
     * Returns average weight price over all DeliveryServices from persistent data storage.
     *
     * @throws PersistException
     * @return average weight price.
     */
    Double getAverageWeightPrice() throws PersistException;

    /**
     * Returns average distance price over all DeliveryServices from persistent data storage.
     *
     * @throws PersistException
     * @return average distance price.
     */
    Double getAverageDistancePrice() throws PersistException;

    /**
     * Returns average volume price over all DeliveryServices from persistent data storage.
     *
     * @throws PersistException
     * @return average volume price.
     */
    Double getAverageVolumePrice() throws PersistException;
}
