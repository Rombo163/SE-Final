package com.egar.auction.server.dao.postgres;

import com.egar.auction.server.dao.DeliveryServiceDao;
import com.egar.auction.server.dao.PersistException;
import com.egar.auction.server.domain.DeliveryService;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * DeliveryServiceDao implementation for Postgres SQL.
 *
 * @author Lisitsyn R.
 */
public class PostgresDeliveryServiceDao implements DeliveryServiceDao {
    private DataSource dataSource;

    /**
     * Constructs new DeliveryService with specified DataSource.
     *
     * @param dataSource specified DataSource.
     */
    public PostgresDeliveryServiceDao(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    /**
     * @see DeliveryServiceDao#persist(DeliveryService)
     */
    @Override
    public DeliveryService persist(DeliveryService service) throws PersistException {
        String sql = "INSERT INTO delivery (title, w_price, v_price, d_price, min_weight," +
                " max_weight, min_volume, max_volume, min_distance, max_distance) VALUES (?,?,?,?,?,?,?,?,?,?);";
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setString(1, service.getTitle());
            statement.setFloat(2, service.getWeightPrice());
            statement.setFloat(3, service.getVolumePrice());
            statement.setFloat(4, service.getDistancePrice());
            statement.setDouble(5, service.getMinWeight());
            statement.setDouble(6, service.getMaxWeight());
            statement.setDouble(7, service.getMinVolume());
            statement.setDouble(8, service.getMaxVolume());
            statement.setDouble(9, service.getMinDistance());
            statement.setDouble(10, service.getMaxDistance());
            int count = statement.executeUpdate();
            if (count != 1)
                throw new PersistException("On persist modify more then 1 record: " + count);
        } catch (SQLException e) {
            throw new PersistException(e);
        }
        sql = "SELECT CURRVAL(pg_get_serial_sequence('delivery','id')) AS last_insert_id;";
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement statement = connection.prepareStatement(sql);
            ResultSet resultSet = statement.executeQuery();
            resultSet.next();
            service.setId(resultSet.getLong(1));
        } catch (SQLException e) {
            throw new PersistException(e);
        }
        return service;
    }

    /**
     * @see DeliveryServiceDao#getById(long)
     */
    @Override
    public DeliveryService getById(long pk) throws PersistException {
        String sql = "SELECT * FROM delivery WHERE id = ?;";
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setLong(1, pk);
            ResultSet resultSet = statement.executeQuery();
            if (!resultSet.next())
                return null;
            else {
                //resultSet.next();
                DeliveryService service = new DeliveryService();
                parseResultSet(resultSet, service);
                return service;
            }
        } catch (SQLException e) {
            throw new PersistException(e);
        }
    }

    /**
     * @see DeliveryServiceDao#update(DeliveryService)
     */
    @Override
    public void update(DeliveryService service) throws PersistException {
        String sql = "UPDATE delivery SET name = ?, w_price = ?, v_price = ?, d_price = ?," +
                " min_weight = ?, max_weight = ?, min_volume = ?, max_volume = ?, min_distance = ?, max_distance = ?" +
                "  WHERE id = ?;";
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setString(1, service.getTitle());
            statement.setFloat(2, service.getWeightPrice());
            statement.setFloat(3, service.getVolumePrice());
            statement.setFloat(4, service.getDistancePrice());
            statement.setDouble(5, service.getMinWeight());
            statement.setDouble(6, service.getMaxWeight());
            statement.setDouble(7, service.getMinVolume());
            statement.setDouble(8, service.getMaxVolume());
            statement.setDouble(9, service.getMinDistance());
            statement.setDouble(10, service.getMaxDistance());
            statement.setLong(11, service.getId());
            int count = statement.executeUpdate();
            if (count != 1)
                throw new PersistException("On update modify more then one record: " + count);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * @see DeliveryServiceDao#delete(DeliveryService)
     */
    @Override
    public void delete(DeliveryService service) throws PersistException {
        String sql = "DELETE FROM delivery WHERE id = ?;";
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setLong(1, service.getId());
            int count = statement.executeUpdate();
            if (count != 1)
                throw new PersistException("On delete modify more then one record: " + count);
        } catch (SQLException e) {
            throw new PersistException(e);
        }

    }

    /**
     * @see DeliveryServiceDao#getAverageWeightPrice()
     */
    @Override
    public Double getAverageWeightPrice() throws PersistException {
        String sql = "SELECT AVG (w_price) FROM delivery;";
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stm = connection.prepareStatement(sql);
            ResultSet rs = stm.executeQuery();
            if (rs.next())
                return rs.getDouble("avg");
            else
                throw new PersistException("ResultSer is empty on getAverageWeightPrice().");
        } catch (SQLException e) {
            throw new PersistException(e);
        }
    }

    /**
     * @see DeliveryServiceDao#getAverageDistancePrice()
     */
    @Override
    public Double getAverageDistancePrice() throws PersistException {
        String sql = "SELECT AVG (d_price) FROM delivery;";
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stm = connection.prepareStatement(sql);
            ResultSet rs = stm.executeQuery();
            if (rs.next())
                return rs.getDouble("avg");
            else
                throw new PersistException("ResultSer is empty on getAverageDistancePrice().");
        } catch (SQLException e) {
            throw new PersistException(e);
        }
    }

    /**
     * @see DeliveryServiceDao#getAverageVolumePrice()
     */
    @Override
    public Double getAverageVolumePrice() throws PersistException {
        String sql = "SELECT AVG (v_price) FROM delivery;";
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stm = connection.prepareStatement(sql);
            ResultSet rs = stm.executeQuery();
            if (rs.next())
                return rs.getDouble("avg");
            else
                throw new PersistException("ResultSer is empty on getAverageVolumePrice().");
        } catch (SQLException e) {
            throw new PersistException(e);
        }
    }

    private void parseResultSet(ResultSet rs, DeliveryService service) throws SQLException {
        service.setId(rs.getLong("id"));
        service.setTitle(rs.getString("name"));
        service.setWeightPrice(rs.getFloat("w_price"));
        service.setVolumePrice(rs.getFloat("v_price"));
        service.setDistancePrice(rs.getFloat("d_price"));
        service.setMaxDistance(rs.getLong("max_distance"));
        service.setMinDistance(rs.getLong("min_distance"));
        service.setMaxVolume(rs.getLong("max_volume"));
        service.setMinVolume(rs.getLong("min_volume"));
        service.setMaxWeight(rs.getLong("max_weight"));
        service.setMinWeight(rs.getLong("min_weight"));
    }
}
