package com.egar.auction.server;

/**
 * Contains order key for bid queries.
 *
 * @author Lisitsyn.
 */
public enum OrderKeysForBid {
    ID, OWNER, LOT
}
