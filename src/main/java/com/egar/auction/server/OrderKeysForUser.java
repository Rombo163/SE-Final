package com.egar.auction.server;

/**
 * Contains order key words.
 *
 * @author Lisitsyn.
 */
public enum OrderKeysForUser {
    ID, NAME, ROLE
}
