package com.egar.auction.server;

/**
 * Outer class for server starting. It contains main static method.
 *
 * @author Lisitsyn.
 */
public class ServerEntryPoint {
    /**
     * Static method for starting server.
     *
     * @param args command prompt parameters.
     */
    public static void main(String[] args) {
        Server server = new Server();
        server.runServer();
    }
}
