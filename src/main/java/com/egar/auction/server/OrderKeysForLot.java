package com.egar.auction.server;

/**
 * Contains order key for lot queries.
 *
 * @author Lisitsyn.
 */
public enum OrderKeysForLot {
    ID, CATEGORY, DESCRIPTION, OWNER
}
