package com.egar.auction.server;

import com.egar.auction.server.controller.Controller;
import com.egar.auction.server.controller.exceptions.*;
import com.egar.auction.server.domain.Role;
import com.egar.auction.server.domain.User;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.util.*;

import static java.lang.Thread.sleep;

/**
 * Server class.
 *
 * @author Lisitsyn.
 */
public class Server {

    private User currentUser;
    private Controller controller;
    private boolean keepWorking;
    private HashMap<String, AbstractServerQueryExecutor> executors;
    private long serverId;

    /**
     * Entry method for Server Class. Invoke this method to run server.
     */
    public void runServer() {
        try {
            ResourceBundle props = ResourceBundle.getBundle("server");
            ServerSocket ss = new ServerSocket(Integer.valueOf(props.getString("egar.se.server.port")));
            ss.setSoTimeout(2000);
            System.out.println("Server started.");
            while (keepWorking) {
                try {
                    new AuctionServer(serverId, ss.accept());
                    serverId++;
                } catch (SocketTimeoutException e) {
                    //do nothing.
                    //We need this catch clause to have a chance to stop server.
                }
            }
            ss.close();
            controller.closeResources();
            System.out.println("Server stopped.");
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    /**
     * Constructs server class.
     */
    public Server() {
        keepWorking = true;
        this.controller = new Controller();
        this.executors = new HashMap<>();
        serverId = 1;
        Thread readerThread = new Thread(new Runnable() {

            @Override
            public void run() {
                BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
                try {
                    while (!(reader.readLine().equals("stop"))) {
                        sleep(1000);
                    }
                    keepWorking = false;
                } catch (InterruptedException | IOException e) {
                    e.printStackTrace();
                }
            }
        });
        readerThread.setDaemon(true);
        readerThread.start();
        initExecutors();
    }

    private class AuctionServer extends Thread {
        Socket s;
        long id;
        ObjectInputStream in;
        ObjectOutputStream out;

        /**
         * AConstructs new AuctionServer with specified id and socket.
         *
         * @param id server id.
         * @param ss socket.
         */
        public AuctionServer(long id, Socket ss) {
            this.s = ss;
            this.id = id;

            setDaemon(true);
            setPriority(NORM_PRIORITY);
            start();
        }

        /**
         * Entry method for new server thread.
         */
        public void run() {
            try {
                System.out.println("AuctionServer#" + id + ", started.");
                boolean keepWaitingForAQuery = true;
                List<Object> query;
                Object o;

                while (keepWaitingForAQuery) {
                    try {
                        in = new ObjectInputStream((s.getInputStream()));
                        out = new ObjectOutputStream((s.getOutputStream()));
                        System.out.println("AuctionServer#" + id + ", waiting for a query...");
                        o = in.readObject();
                        query = (List<Object>) o;
                        String command = (String) query.get(0);
                        System.out.println("AuctionServer#" + id + ": - Got a Query with name: \"" + command + "\", trying to execute...");
                        if (command.equals("authentication")) {
                            sendAuthenticationResponse(query, out);
                        } else if (command.equals("deAuthentication")) {
                            deAuthentication();
                            sendResponseToClient(true, out);
                        } else if (command.equals("stopServerThread")) {
                            keepWaitingForAQuery = false;
                            sendResponseToClient(true, out);
                            s.close();
                            break;
                        } else {
                            AbstractServerQueryExecutor executor = executors.get(command);
                            if (executor == null) {
                                System.out.println("AuctionServer#" + id + ": - WrongServerQueryStructureException, sending response to client...");
                                sendResponseToClient(new WrongServerQueryStructureException("Unknown com.egar.auction.server command: " + command), out);
                            } else {
                                try {
                                    sendResponseToClient(executor.executeServerQuery(query, currentUser, controller), out);
                                } catch (AuctionException e) {
                                    System.out.println("AuctionServer#" + id + ": - AuctionException, sending response to client...");
                                    sendResponseToClient(e, out);
                                }
                            }
                        }
                    } catch (ClassCastException e) {
                        System.out.println("AuctionServer#" + id + ": - ClassCastException:" + e.getMessage() + ", sending response to client...");
                        sendResponseToClient(new WrongServerQueryStructureException(e), out);
                    } catch (ClassNotFoundException e) {
                        e.printStackTrace();
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            System.out.println("AuctionServer#" + id + ", stopped.");
        }

        private void sendAuthenticationResponse(List<Object> query, ObjectOutputStream out) {
            User user = authentication((String) query.get(1), (String) query.get(2));
            List<Object> response = new ArrayList<>();
            if (user != null) {
                response.add(true);
                response.add(user);
                sendResponseToClient(response, out);
            } else {
                response.add(false);
                sendResponseToClient(response, out);
            }
        }

        private void deAuthentication() {
            setCurrentUser(null);
        }

        private User authentication(String userName, String password) {
            User user = controller.getUserByHisCredentials(userName, password);
            if (user != null) {
                setCurrentUser(user);
                return user;
            } else
                return null;
        }

        private void sendResponseToClient(Object o, ObjectOutputStream out) {
            try {
                out.writeObject(o);
                out.flush();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }

    private void setCurrentUser(User currentUser) {
        this.currentUser = currentUser;
    }

    private void initExecutors() {
        executors.put("getCategories", new AbstractServerQueryExecutor(null) {
            @Override
            public Object executeServerQuery(List<Object> objects, User user, Controller controller) throws AuctionException {
                if (checkUserRole(currentUser))
                    return controller.getCategories((long) objects.get(1), (long) objects.get(2));
                else
                    throw new PermissionNotGrantedException("Can not execute this server query for current user access level.");
            }
        });
        executors.put("getLotsSortedByKey", new AbstractServerQueryExecutor(null) {
            @Override
            public Object executeServerQuery(List<Object> objects, User user, Controller controller) throws PermissionNotGrantedException, CategoryNotFoundException, UserWithSuchIdNotFoundException, LotWithSuchIdNotFoundException, IncorrectQueryKeyException {
                if (checkUserRole(currentUser)) {
                    return controller.getLotsSortedByKey((OrderKeysForLot) objects.get(1), (long) objects.get(2), (long) objects.get(3));
                } else
                    throw new PermissionNotGrantedException("Can not execute this server query for current user access level.");

            }
        });
        executors.put("getAllLotsInCategory", new AbstractServerQueryExecutor(null) {
            @Override
            public Object executeServerQuery(List<Object> objects, User user, Controller controller) throws PermissionNotGrantedException, CategoryNotFoundException {
                if (checkUserRole(currentUser))
                    return controller.getAllLotsInCategory((String) objects.get(1), (long) objects.get(2), (long) objects.get(3));
                else
                    throw new PermissionNotGrantedException("Can not execute this server query for current user access level.");
            }
        });
        executors.put("getLotsByOwner", new AbstractServerQueryExecutor(null) {
            @Override
            public Object executeServerQuery(List<Object> objects, User user, Controller controller) throws PermissionNotGrantedException, CategoryNotFoundException, UserWithSuchIdNotFoundException {
                if (checkUserRole(currentUser))
                    return controller.getLotsByOwner((Long) objects.get(1), (long) objects.get(2), (long) objects.get(3));
                else
                    throw new PermissionNotGrantedException("Can not execute server query for current user access level.");
            }
        });
        executors.put("getLotsByDescription", new AbstractServerQueryExecutor(null) {
            @Override
            public Object executeServerQuery(List<Object> objects, User user, Controller controller) throws PermissionNotGrantedException, CategoryNotFoundException, UserWithSuchIdNotFoundException, LotWithSuchIdNotFoundException {
                if (checkUserRole(currentUser)) {
                    return controller.getLotsByDescription((String) objects.get(1), (long) objects.get(2), (long) objects.get(3));
                } else
                    throw new PermissionNotGrantedException("Can not execute this server query for current user access level.");

            }
        });
        executors.put("getLotById", new AbstractServerQueryExecutor(null) {
            @Override
            public Object executeServerQuery(List<Object> objects, User user, Controller controller) throws PermissionNotGrantedException, CategoryNotFoundException, UserWithSuchIdNotFoundException, LotWithSuchIdNotFoundException {
                if (checkUserRole(currentUser)) {
                    return controller.getLotById((long) objects.get(1));
                } else
                    throw new PermissionNotGrantedException("Can not execute this server query for current user access level.");
            }
        });
        executors.put("getBidsByOwnerSortedByKey", new AbstractServerQueryExecutor(new ArrayList<>(Arrays.asList(new Role("RegUser")))) {
            @Override
            public Object executeServerQuery(List<Object> objects, User user, Controller controller) throws PermissionNotGrantedException, CategoryNotFoundException, UserWithSuchIdNotFoundException, LotWithSuchIdNotFoundException {
                if (checkUserRole(currentUser)) {
                    return controller.getBidsByOwnerSortedByKey(currentUser.getId(), (OrderKeysForBid) objects.get(1), (Long) objects.get(2), (Long) objects.get(3));
                } else
                    throw new PermissionNotGrantedException("Can not execute this server query for current user access level.");
            }
        });
        executors.put("getBidsByLotSortedByKey", new AbstractServerQueryExecutor(new ArrayList<>(Arrays.asList(new Role("RegUser")))) {
            @Override
            public Object executeServerQuery(List<Object> objects, User user, Controller controller) throws PermissionNotGrantedException, CategoryNotFoundException, UserWithSuchIdNotFoundException, LotWithSuchIdNotFoundException, IncorrectQueryKeyException {
                if (checkUserRole(currentUser)) {
                    return controller.getBidsByLotSortedByKey((long) objects.get(1), (OrderKeysForBid) objects.get(2), (Long) objects.get(3), (Long) objects.get(4));
                } else
                    throw new PermissionNotGrantedException("Can not execute this server query for current user access level.");
            }
        });
        executors.put("getDeliveryCostForLot", new AbstractServerQueryExecutor(new ArrayList<>(Arrays.asList(new Role("RegUser")))) {
            @Override
            public Object executeServerQuery(List<Object> objects, User user, Controller controller) throws PermissionNotGrantedException, CategoryNotFoundException, UserWithSuchIdNotFoundException, LotWithSuchIdNotFoundException, IncorrectQueryKeyException, LotIsNotSoldException {
                if (checkUserRole(currentUser)) {
                    return controller.getDeliveryCostForLot((long) objects.get(1));
                } else
                    throw new PermissionNotGrantedException("Can not execute this server query for current user access level.");

            }
        });
        executors.put("getLotsBySold", new AbstractServerQueryExecutor(new ArrayList<>(Arrays.asList(new Role("RegUser")))) {
            @Override
            public Object executeServerQuery(List<Object> objects, User user, Controller controller) throws PermissionNotGrantedException, CategoryNotFoundException, UserWithSuchIdNotFoundException, LotWithSuchIdNotFoundException, IncorrectQueryKeyException, LotIsNotSoldException {
                if (checkUserRole(currentUser)) {
                    return controller.getLotsBySold((long) objects.get(1), (long) objects.get(2), (long) objects.get(3));
                } else
                    throw new PermissionNotGrantedException("Can not execute this server query for current user access level.");

            }
        });
        executors.put("getUsers", new AbstractServerQueryExecutor(new ArrayList<>(Arrays.asList(new Role("Admin")))) {
            @Override
            public Object executeServerQuery(List<Object> objects, User user, Controller controller) throws PermissionNotGrantedException, CategoryNotFoundException, UserWithSuchIdNotFoundException, LotWithSuchIdNotFoundException, IncorrectQueryKeyException, LotIsNotSoldException {
                if (checkUserRole(currentUser)) {
                    return controller.getUsers((long) objects.get(1), (long) objects.get(2));
                } else
                    throw new PermissionNotGrantedException("Can not execute this server query for current user access level.");
            }
        });
        executors.put("getNumberOfLotsForUser", new AbstractServerQueryExecutor(new ArrayList<>(Arrays.asList(new Role("Admin")))) {
            @Override
            public Object executeServerQuery(List<Object> objects, User user, Controller controller) throws PermissionNotGrantedException, CategoryNotFoundException, UserWithSuchIdNotFoundException, LotWithSuchIdNotFoundException, IncorrectQueryKeyException, LotIsNotSoldException {
                if (checkUserRole(currentUser)) {
                    return controller.getNumberOfLotsForUser((long) objects.get(1));
                } else
                    throw new PermissionNotGrantedException("Can not execute this server query for current user access level.");
            }
        });
        executors.put("getNumberOfBidsForUser", new AbstractServerQueryExecutor(new ArrayList<>(Arrays.asList(new Role("Admin")))) {
            @Override
            public Object executeServerQuery(List<Object> objects, User user, Controller controller) throws PermissionNotGrantedException, CategoryNotFoundException, UserWithSuchIdNotFoundException, LotWithSuchIdNotFoundException, IncorrectQueryKeyException, LotIsNotSoldException {
                if (checkUserRole(currentUser)) {
                    return controller.getNumberOfBidsForUser((long) objects.get(1));
                } else
                    throw new PermissionNotGrantedException("Can not execute this server query for current user access level.");
            }
        });
        executors.put("getUserById", new AbstractServerQueryExecutor(new ArrayList<>(Arrays.asList(new Role("Admin")))) {
            @Override
            public Object executeServerQuery(List<Object> objects, User user, Controller controller) throws PermissionNotGrantedException, CategoryNotFoundException, UserWithSuchIdNotFoundException, LotWithSuchIdNotFoundException, IncorrectQueryKeyException, LotIsNotSoldException {
                if (checkUserRole(currentUser)) {
                    return controller.getUserById((long) objects.get(1));
                } else
                    throw new PermissionNotGrantedException("Can not execute this server query for current user access level.");
            }
        });
        executors.put("getUserById", new AbstractServerQueryExecutor(new ArrayList<>(Arrays.asList(new Role("Admin")))) {
            @Override
            public Object executeServerQuery(List<Object> objects, User user, Controller controller) throws PermissionNotGrantedException, CategoryNotFoundException, UserWithSuchIdNotFoundException, LotWithSuchIdNotFoundException, IncorrectQueryKeyException, LotIsNotSoldException {
                if (checkUserRole(currentUser)) {
                    return controller.getUserById((long) objects.get(1));
                } else
                    throw new PermissionNotGrantedException("Can not execute this server query for current user access level.");
            }
        });
        executors.put("getUsersByName", new AbstractServerQueryExecutor(new ArrayList<>(Arrays.asList(new Role("Admin")))) {
            @Override
            public Object executeServerQuery(List<Object> objects, User user, Controller controller) throws PermissionNotGrantedException, CategoryNotFoundException, UserWithSuchIdNotFoundException, LotWithSuchIdNotFoundException, IncorrectQueryKeyException, LotIsNotSoldException {
                if (checkUserRole(currentUser)) {
                    return controller.getUsersByName((String) objects.get(1), (long) objects.get(2), (long) objects.get(3));
                } else
                    throw new PermissionNotGrantedException("Can not execute this server query for current user access level.");
            }
        });
        executors.put("getUsersSortedByKey", new AbstractServerQueryExecutor(new ArrayList<>(Arrays.asList(new Role("Admin")))) {
            @Override
            public Object executeServerQuery(List<Object> objects, User user, Controller controller) throws PermissionNotGrantedException, CategoryNotFoundException, UserWithSuchIdNotFoundException, LotWithSuchIdNotFoundException, IncorrectQueryKeyException, LotIsNotSoldException {
                if (checkUserRole(currentUser)) {
                    return controller.getUsersSortedByKey((OrderKeysForUser) objects.get(1), (long) objects.get(2), (long) objects.get(3));
                } else
                    throw new PermissionNotGrantedException("Can not execute this server query for current user access level.");
            }
        });
    }
}
