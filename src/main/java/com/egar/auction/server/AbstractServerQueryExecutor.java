package com.egar.auction.server;

import com.egar.auction.server.controller.Controller;
import com.egar.auction.server.controller.exceptions.*;
import com.egar.auction.server.domain.Role;
import com.egar.auction.server.domain.User;

import java.util.List;

/**
 * AbstractServerQueryExecutor class.
 * Abstract class, ancestor of the anonymous classes that are using for server query processing.
 *
 * @author Lisitsyn R.
 */
public abstract class AbstractServerQueryExecutor {
    private List<Role> authorizedRoles;

    /**
     * AbstractServerQueryExecutor constructor.
     *
     * @param roles List of authorized roles.
     */
    public AbstractServerQueryExecutor(List<Role> roles) {
        this.authorizedRoles = roles;
    }

    /**
     * Server's query processing method. Returns Object for transfer to client.
     * It can throws any exception of the controller's methods.
     *
     * @param objects    List of the Objects.
     * @param user       current user for rights checking.
     * @param controller controller class.
     * @return object response for client.
     * @throws PermissionNotGrantedException
     * @throws CategoryNotFoundException
     * @throws UserWithSuchIdNotFoundException
     * @throws LotWithSuchIdNotFoundException
     * @throws IncorrectQueryKeyException
     * @throws LotIsNotSoldException
     */
    public abstract Object executeServerQuery(List<Object> objects, User user, Controller controller)
            throws AuctionException /*PermissionNotGrantedException, CategoryNotFoundException, UserWithSuchIdNotFoundException,
            LotWithSuchIdNotFoundException, IncorrectQueryKeyException, LotIsNotSoldException*/;

    protected boolean checkUserRole(User user) {
        if (authorizedRoles == null)
            return true;
        else if (user == null)
            return false;
        else {
            for (Role role : authorizedRoles) {
                if (user.getRole().equals(role))
                    return true;
            }
        }
        return false;
    }

}
