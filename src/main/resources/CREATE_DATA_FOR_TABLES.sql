﻿INSERT INTO auction_user (id,name,password,role,latitude,longitude) VALUES (1,'Vasya','Vasya',2,111111.00,111111.00);
INSERT INTO auction_user (id,name,password,role,latitude,longitude) VALUES (2,'Petya','Petya',2,222222.00,222222.00);
INSERT INTO auction_user (id,name,password,role,latitude,longitude) VALUES (3,'Kolyan','Kolyan',2,333333.00,333333.00);
INSERT INTO auction_user (id,name,password,role,latitude,longitude) VALUES (4,'Sanya','Sanya',2,444444.00,444444.00);
INSERT INTO auction_user (id,name,password,role,latitude,longitude) VALUES (5,'Fedya','Fedya',2,555555.00,555555.00);
INSERT INTO auction_user (id,name,password,role,latitude,longitude) VALUES (6,'Admin','Admin',1,0.00,0.00);

INSERT INTO category (name) VALUES ('kitties');
INSERT INTO category (name) VALUES ('puppies');
INSERT INTO category (name) VALUES ('hamsters');
INSERT INTO category (name) VALUES ('books');
INSERT INTO category (name) VALUES ('drugs');
INSERT INTO category (name) VALUES ('guns');
INSERT INTO category (name) VALUES ('computers');
INSERT INTO category (name) VALUES ('animals');
INSERT INTO category (name) VALUES ('movies');
INSERT INTO category (name) VALUES ('birds');
INSERT INTO category (name) VALUES ('tools');
INSERT INTO category (name) VALUES ('cars');
INSERT INTO category (name) VALUES ('bikes');
INSERT INTO category (name) VALUES ('electronics');
INSERT INTO category (name) VALUES ('furniture');
INSERT INTO category (name) VALUES ('music');
INSERT INTO category (name) VALUES ('guitars');
INSERT INTO category (name) VALUES ('parts for car');
INSERT INTO category (name) VALUES ('blabla');
INSERT INTO category (name) VALUES ('blablablab');
INSERT INTO category (name) VALUES ('blablablabl');
INSERT INTO category (name) VALUES ('blablablabla');

INSERT INTO lot (id,price,category,description,lot_owner,quantity,height,width,depth,weight,delivery,buyer,lotsold) 
	VALUES (1,100,1,'red',1,1,1,1,1,1,1,2,true);
INSERT INTO lot (id,price,category,description,lot_owner,quantity,height,width,depth,weight,delivery,buyer,lotsold) 
	VALUES (2,200,2,'blue',2,1,1,1,1,1,2,3,false);
INSERT INTO lot (id,price,category,description,lot_owner,quantity,height,width,depth,weight,delivery,buyer,lotsold) 
	VALUES (3,300,3,'purple',3,1,1,1,1,1,3,4,false);
INSERT INTO lot (id,price,category,description,lot_owner,quantity,height,width,depth,weight,delivery,buyer,lotsold) 
	VALUES (4,400,1,'orange',4,1,1,1,1,1,1,5,false);
INSERT INTO lot (id,price,category,description,lot_owner,quantity,height,width,depth,weight,delivery,buyer,lotsold) 
	VALUES (5,500,2,'green',5,1,1,1,1,1,2,1,false);
INSERT INTO lot (id,price,category,description,lot_owner,quantity,height,width,depth,weight,delivery,buyer,lotsold) 
	VALUES (6,5000,2,'white',1,1,1,1,1,1,3,2,false);
INSERT INTO lot (id,price,category,description,lot_owner,quantity,height,width,depth,weight,delivery,buyer,lotsold) 
	VALUES (7,5900,1,'deep purple',3,1,1,1,1,1,1,2,true);
INSERT INTO lot (id,price,category,description,lot_owner,quantity,height,width,depth,weight,delivery,buyer,lotsold) 
	VALUES (8,90,3,'gray',1,1,1,1,1,1,2,2,false);
INSERT INTO lot (id,price,category,description,lot_owner,quantity,height,width,depth,weight,delivery,buyer,lotsold) 
	VALUES (9,400,1,'magenta',2,1,1,1,1,1,0,1,true);
INSERT INTO lot (id,price,category,description,lot_owner,quantity,height,width,depth,weight,delivery,buyer,lotsold) 
	VALUES (10,3500,2,'brown',2,1,1,1,1,1,1,1,false);
INSERT INTO lot (id,price,category,description,lot_owner,quantity,height,width,depth,weight,delivery,buyer,lotsold) 
	VALUES (11,700,1,'black',3,1,1,1,1,1,2,1,true);
INSERT INTO lot (id,price,category,description,lot_owner,quantity,height,width,depth,weight,delivery,buyer,lotsold) 
	VALUES (12,800,3,'super black',2,1,1,1,1,1,3,1,true);



INSERT INTO bid (id,bid_owner,lot,value) VALUES (1,2,1,1000);
INSERT INTO bid (id,bid_owner,lot,value) VALUES (2,3,2,2000);
INSERT INTO bid (id,bid_owner,lot,value) VALUES (3,1,3,4000);
INSERT INTO bid (id,bid_owner,lot,value) VALUES (4,3,2,4100);
INSERT INTO bid (id,bid_owner,lot,value) VALUES (5,3,2,4200);
INSERT INTO bid (id,bid_owner,lot,value) VALUES (6,3,2,4300);
INSERT INTO bid (id,bid_owner,lot,value) VALUES (7,3,2,4400);
INSERT INTO bid (id,bid_owner,lot,value) VALUES (8,3,2,4500);
INSERT INTO bid (id,bid_owner,lot,value) VALUES (9,1,3,4600);
INSERT INTO bid (id,bid_owner,lot,value) VALUES (10,1,3,4700);
INSERT INTO bid (id,bid_owner,lot,value) VALUES (11,1,2,4800);
INSERT INTO bid (id,bid_owner,lot,value) VALUES (12,1,3,4900);
INSERT INTO bid (id,bid_owner,lot,value) VALUES (13,2,1,5000);
INSERT INTO bid (id,bid_owner,lot,value) VALUES (14,2,1,5100);
INSERT INTO bid (id,bid_owner,lot,value) VALUES (15,2,1,5300);
INSERT INTO bid (id,bid_owner,lot,value) VALUES (16,2,1,5300);


INSERT INTO role (id,name) VALUES (1,'Admin');
INSERT INTO role (id,name) VALUES (2,'RegUser');

INSERT INTO delivery (id,name,w_price,v_price,d_price,min_weight,max_weight,min_volume,max_volume,min_distance,max_distance)
	VALUES (1,'DHL',0.00001,0.00001,0.00001,0,10000,0,10,0,24000000);
INSERT INTO delivery (id,name,w_price,v_price,d_price,min_weight,max_weight,min_volume,max_volume,min_distance,max_distance)
	VALUES (2,'LSD',0.00002,0.00002,0.00002,0,20000,0,20,0,24000000);
INSERT INTO delivery (id,name,w_price,v_price,d_price,min_weight,max_weight,min_volume,max_volume,min_distance,max_distance)
	VALUES (3,'DDT',0.00003,0.00003,0.00003,0,15000,0,16,0,24000000);