<!DOCTYPE html PUBLIC>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
	<head>
		<title>Auction.</title>
		<link rel="stylesheet" href = "<%=request.getContextPath()%>/style.css"/>
		<c:set var="pageSize" scope="application" value="20"/>
	<head/>
	<body>
		<div id="container">
			<div id="header">
				<div id="hint" style="color:#8e8e8e; font-size:18px;" >
					<center>
						login/password</br>
						For Admin: Admin/Admin</br>
						For RegUser: Vasya/Vasya					
					</center>
				</div>
			</div>
			<div id="navigation">

			</div>
			<div id="menu">
				<jsp:include page = "/content/jspelements/authorization.jsp" flush = "true"/>			
				<jsp:include page = "/content/jspelements/menu.jsp" flush="true"/>		
			</div>
			<div id="content">

			</div>
			<div id="clear">

			</div>
			<div id="footer">

			</div>
		</div>
	</body>
<html/>