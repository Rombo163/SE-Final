<!DOCTYPE html PUBLIC>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
	<head>
		<title>Auction.</title>
		<link rel="stylesheet" href = "<%=request.getContextPath()%>/style.css"/>
	<head/>
	<body>
		<div id="container">
			<div id="header">

			</div>
			<div id="navigation">

			</div>
			<div id="menu">			
				<jsp:include page = "jspelements/authorization.jsp" flush = "true"/>			
				<jsp:include page = "jspelements/menu.jsp" flush="true"/>			
			</div>
			<div id="content">			
				<c:if test="${categories != null}">
					<div id="contentTitle">
						<p align="center" style="font-size:18px; color:black">Categories</p>
					</div>
					<div id="categories" style="font-size:10px " align="center">
						<table border="1" cellpadding="1" cellspacing="1" width="400">
							<tr>
								<th>CategoryName</th>
								<th>NumberOfLots</th>
							</tr>
							<c:forEach var="category" items="${categories}">
								<tr>
									<c:if test="${numberOfLots.get(category.getId())!=0}">
										<td style="padding-left: 50px;" align="left"><a href="<%=request.getContextPath()%>/auction/content/FindLots?pageSize=${pageSize}&currentPage=1&keyParameterType=category&keyValue=${category.getId()}"><c:out value="${category.getCategoryName()}"/></a>	</td>
									</c:if>
									<c:if test="${numberOfLots.get(category.getId())==0}">
										<td style="padding-left: 50px;" align="left">${category.getCategoryName()}</td>
									</c:if>
									<td align="center">${numberOfLots.get(category.getId())}</td>
								</tr>
							</c:forEach>
						</table>
						<table>
							<tr>
								<c:if test="${currentPage>1}">
									<td><a href="<%=request.getContextPath()%>/auction/content/ShowCategories?pageSize=${pageSize}&currentPage=${currentPage-1}"> &lt&ltPrev. </a></td>
								</c:if>
								<td>${currentPage} / ${pageNumber}</td>
								<c:if test="${currentPage<pageNumber}">
									<td><a href="<%=request.getContextPath()%>/auction/content/ShowCategories?pageSize=${pageSize}&currentPage=${currentPage+1}"> Next.&gt&gt</a></td>
								</c:if>
							</tr>
						</table>						
					</div>		
				</c:if>	
			</div>
			<div id="clear">

			</div>
			<div id="footer">

			</div>
		</div>
	</body>
<html/>