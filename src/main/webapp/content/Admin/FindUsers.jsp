<!DOCTYPE html PUBLIC>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
	<head>
		<title>Auction.</title>
		<link rel="stylesheet" href = "<%=request.getContextPath()%>/style.css"/>
	<head/>
	<body>
		<div id="container">
			<div id="header">
		
			</div>
			<div id="navigation">

			</div>
			<div id="menu">
				<jsp:include page = "../jspelements/authorization.jsp" flush = "true"/>			
				<jsp:include page = "../jspelements/menu.jsp" flush="true"/>		
			</div>
			<div id="content">
				<c:if test="${foundedUsers != null}">
					<div id="contentTitle">
						<p align="center" style="font-size:18px; color:black">User's stat.</p>
					</div>
					<div id="lots" style="font-size:10px " align="center">
						<table border="1" cellpadding="1" cellspacing="1" width="600">
							<tr>
								<th>UserId</th>
								<th>Name</th>
								<th>Role</th>
								<th>Bids number</th>
								<th>Lots number</th>
							</tr>
							<c:forEach var="user" items="${foundedUsers}">
								<tr>
									<td align="center">${user.getId()}</td>
									<td align="center">${user.getUserName()}</td>
									<td align="center">${user.getRole().getRoleName()}</td>
									<td align="center"><a href="<%=request.getContextPath()%>/auction/content/RegUser/FindBids?pageSize=${pageSize}&currentPage=1&keyParameterType=owner&keyValue=${user.getId()}">${bidsNumbersForUsers.get(user.getId())}</a></td>
									<td align="center"><a href="<%=request.getContextPath()%>/auction/content/FindLots?pageSize=${pageSize}&currentPage=1&keyParameterType=owner&keyValue=${user.getId()}">${lotsNumbersForUsers.get(user.getId())}</a></td>
								</tr>
							</c:forEach>
						</table>
						<table>
							<tr>
								<c:if test="${currentPage>1}">
									<td><a href="<%=request.getContextPath()%>/auction/content/Admin/FindUsers?pageSize=${pageSize}&currentPage=${currentPage-1}"> &lt&ltPrev. </a></td>
								</c:if>
									<td>${currentPage} / ${pageNumber}</td>
								<c:if test="${currentPage<pageNumber}">
									<td><a href="<%=request.getContextPath()%>/auction/content/Admin/FindUsers?pageSize=${pageSize}&currentPage=${currentPage+1}"> Next.&gt&gt</a></td>
								</c:if>
							</tr>
						</table>		
					</div>
				</c:if>	
			</div>
			<div id="clear">

			</div>
			<div id="footer">

			</div>
		</div>
	</body>
<html/>