<!DOCTYPE html PUBLIC>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
	<head>
		<title>Auction.</title>
		<link rel="stylesheet" href = "<%=request.getContextPath()%>/style.css"/>

	<head/>
	<body>
		<div id="container">
			<div id="header">
	
			</div>
			<div id="navigation">

			</div>
			<div id="menu">
				<jsp:include page = "../jspelements/authorization.jsp" flush = "true"/>			
				<jsp:include page = "../jspelements/menu.jsp" flush="true"/>			
			</div>
			<div id="content">
				<c:if test="${users != null}">
					<div id="contentTitle">
						<p align="center" style="font-size:18px; color:black">User's stat.</p>
					</div>
					<div id="lots" style="font-size:10px " align="center">
						<table border="1" cellpadding="1" cellspacing="1" width="600">
							<tr>
								<th><a href="<%=request.getContextPath()%>/auction/content/Admin/ShowUsers?order=id&pageSize=${pageSize}&currentPage=${currentPage}">UserId</a></th>
								<th><a href="<%=request.getContextPath()%>/auction/content/Admin/ShowUsers?order=name&pageSize=${pageSize}&currentPage=${currentPage}">Name</a></th>
								<th><a href="<%=request.getContextPath()%>/auction/content/Admin/ShowUsers?order=role&pageSize=${pageSize}&currentPage=${currentPage}">Role</a></th>
								<th>Bids number</th>
								<th>Lots number</th>
							</tr>
							<c:forEach var="user" items="${users}">
								<tr>
									<td align="center">${user.getId()}</td>
									<td align="center">${user.getUserName()}</td>
									<td align="center">${user.getRole().getRoleName()}</td>
									<c:if test="${bidsNumbersForUsers.get(user.getId()) != 0}">
										<td align="center"><a href="<%=request.getContextPath()%>/auction/content/RegUser/FindBids?pageSize=${pageSize}&currentPage=1&keyParameterType=owner&keyValue=${user.getId()}">${bidsNumbersForUsers.get(user.getId())}</a></td>
									</c:if>
									<c:if test="${bidsNumbersForUsers.get(user.getId()) == 0}">
										<td align="center">${bidsNumbersForUsers.get(user.getId())}</td>
									</c:if>
									<c:if test="${lotsNumbersForUsers.get(user.getId()) != 0}">
										<td align="center"><a href="<%=request.getContextPath()%>/auction/content/FindLots?pageSize=${pageSize}&currentPage=1&keyParameterType=owner&keyValue=${user.getId()}">${lotsNumbersForUsers.get(user.getId())}</a></td>
									</c:if>
									<c:if test="${lotsNumbersForUsers.get(user.getId()) == 0}">
										<td align="center">${lotsNumbersForUsers.get(user.getId())}</td>
									</c:if>
								</tr>
							</c:forEach>
						</table>
						<table>
							<tr>
								<c:if test="${currentPage>1}">
									<td><a href="<%=request.getContextPath()%>/auction/content/Admin/ShowUsers?order=${order}&pageSize=${pageSize}&currentPage=${currentPage-1}"> &lt&ltPrev. </a></td>
								</c:if>
									<td>${currentPage} / ${pageNumber}</td>
								<c:if test="${currentPage<pageNumber}">
									<td><a href="<%=request.getContextPath()%>/auction/content/Admin/ShowUsers?order=${order}&pageSize=${pageSize}&currentPage=${currentPage+1}"> Next.&gt&gt</a></td>
								</c:if>
							</tr>
						</table>		
					</div>
				</c:if>	
			</div>
			<div id="clear">

			</div>
			<div id="footer">

			</div>
		</div>
	</body>
<html/>