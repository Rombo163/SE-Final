<!DOCTYPE html PUBLIC>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
	<head>
		<title>Auction.</title>
	<head/>
	<body>
		<div style="margin-top:24px">
			<a style="color:white; font-size:18px; margin-left:24px" href="<%=request.getContextPath()%>/auction/content/ShowCategories?pageSize=${pageSize}&currentPage=1">-Show categories.</a><br/>
			<a style="color:white; font-size:18px; margin-left:24px" href="<%=request.getContextPath()%>/auction/content/ShowLots?order=id&pageSize=${pageSize}&currentPage=1">-Show lots.</a><br/>
			<a style="color:white; font-size:18px; margin-left:24px" rel="" href="#win">-Find lot.</a></br>
			<c:if test = "${user!=null}">
				<c:if test = "${user.getRole().getRoleName() == 'Admin'}">
					<a style="color:white; font-size:18px; margin-left:24px" href="<%=request.getContextPath()%>/auction/content/Admin/ShowUsers?order=id&pageSize=${pageSize}&currentPage=1">-Show user's stat.</a></br>
					<a style="color:white; font-size:18px; margin-left:24px" href="#win1">-Find user.</a></br>
				</c:if>
				<c:if test = "${user.getRole().getRoleName() == 'RegUser'}">
					<a style="color:white; font-size:18px; margin-left:24px" href="<%=request.getContextPath()%>/auction/content/FindLots?pageSize=${pageSize}&currentPage=1&keyParameterType=owner&keyValue=${user.getId()}">-Show my lots.</a></br>
					<a style="color:white; font-size:18px; margin-left:24px" href="<%=request.getContextPath()%>/auction/content/RegUser/FindBids?pageSize=${pageSize}&currentPage=1&keyParameterType=owner&keyValue=${user.getId()}">-Show my bids.</a></br>
				</c:if>
			</c:if>		
			<a href="#x" class="overlay" id="win"></a>
				<div class="popup" align="left">
					<form action="<%=request.getContextPath()%>/auction/content/FindLots?pageSize=${pageSize}&currentPage=1" method="post">
						<center>
						<p>Input id or select another parameter.</p>
						<select name="keyParameterType">
							<option value="id">id</option>
							<option value="owner">owner id</option>
							<option value="description">description</option>
						</select>							
						 <input type="text" name="keyValue" placeholder ="input key value"/>
						<p> <input type="submit" value="Search"/></p>		
						</center>						
					</form>
					<a class="close"title="Close" href="#close"></a>
				</div>					
				<a href="#x" class="overlay" id="win1"></a>
				<div class="popup" align="left">
					<form action="<%=request.getContextPath()%>/auction/content/Admin/FindUsers?pageSize=${pageSize}&currentPage=1" method="post">
						<center>
						<p>Input id or select another parameter.</p>
						<select name="keyParameterType">
							<option value="id">id</option>
							<option value="owner">name</option>
						</select>							
						 <input type="text" name="keyValue" placeholder ="input key value"/>
						<p> <input type="submit" value="Search"/></p>		
						</center>						
					</form>
					<a class="close"title="Close" href="#close"></a>
				</div>	
		</div>
	</body>
<html/>