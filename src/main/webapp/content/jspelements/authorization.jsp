<!DOCTYPE html PUBLIC>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
	<head>
		<title>Auction.</title>
	<head/>
	<body>
		<div id="authr">
			<c:if test="${user == null}">
				<fieldset style="border:3px#9ACD32 solid; font-size:18">
					<legend style="color:white">Authorization.</legend>
					<div id ="auth" class = "block2" align="left">
						<form action="<%=request.getContextPath()%>/auction/authorization" method="post">
							<p> <input type="text" name="login" placeholder ="Login."/></p>
							<p> <input type="password" name="pass" placeholder = "Password."/></p>
							<p> <input type="submit" value="Sign in."/></p>
						</form>
					</div>
				</fieldset>
			</c:if>
			<c:if test="${user != null}">
				<fieldset style="border:3px#9ACD32 solid; font-size:18">
					<legend style="color:white">Welcome!</legend>
					<center>
						<p>You have authorized as</p>
						<c:out value="${user.getRole().getRoleName()}"/>
						<div id ="auth" class = "block2" align="left">
							<form action="<%=request.getContextPath()%>/auction/deauthorization" method="post">
							<center>
								<p> <input type="submit" value="Logout."/></p>
							</center>
							</form>
						</div>
					</center>
				</fieldset>			
			</c:if>
		</div>	
	</body>
<html/>