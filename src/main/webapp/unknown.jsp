<!DOCTYPE html PUBLIC>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
	<head>
		<title>Unknown.</title>	
	<head/>
	<body>
		<center>
			<h1>You were redirect to this page by one of the possible reasons:
			the server received an unknown command, </br>
			or an incorrect parameter type,</br>
			or you have no rights to view the page you have tried to get </h1></br>
			<a href = "<%=request.getContextPath()%>/auction">to main page</a>
		</center>
	</body>
<html/>